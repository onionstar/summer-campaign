﻿using BrakesPromotions.Manager.Services;
using System;
using Umbraco.Core;
using Umbraco.Core.Logging;

namespace BrakesPromotions.Manager.Models.ViewModels
{
    public class ExportMember
    {
        private static GiftTierService giftTierService = new GiftTierService();
        private static GiftService giftService = new GiftService();
        private static BrandService brandService = new BrandService();

        public string Urn { get; set; }
        public string ClaimCode { get; set; }
        public string ClaimLevel { get; set; }
        public string PrizeClaimed { get; set; }
        public DateTime? PrizeClaimedDate { get; set; }
        public decimal SpendBalance { get; set; }
        public string NectarCustomer { get; set; }
        public string NectarCardNumber { get; set; }
        public string Brand { get; set; }
        public string BusinessName { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public DateTime? DateRegistered { get; set; }
        public DateTime? LastLoggedIn { get; set; }

        public bool? GdprSalesContact { get; set; }
        public bool? SalesEmail { get; set; }
        public bool? SalesTelephone { get; set; }
        public bool? SalesMobile { get; set; }
        public bool? GdprOffers { get; set; }
        public bool? OffersEmail { get; set; }
        public bool? OffersTelephone { get; set; }
        public bool? OffersMobile { get; set; }
        public bool? OffersPost { get; set; }

        public ExportMember(Rdbms.Member member)
        {
            if (member != null)
            {
                Urn = member.Urn;
                ClaimCode = member.ClaimCode;
                if (member.ClaimedGiftTierGuid.HasValue && member.ClaimedGiftTierGuid.Value != Guid.Empty)
                {
                    ClaimLevel = giftTierService.GetByGiftTierGuid(member.ClaimedGiftTierGuid.Value).Name;
                    PrizeClaimed = giftService.GetByGiftGuid(member.ClaimedGiftGuid.Value).Name;
                    PrizeClaimedDate = member.ClaimedDate.Value;
                }
                SpendBalance = member.SpendBalance;

                NectarCustomer = member.NectarCustomer.ToString();
                NectarCardNumber = member.NectarCardNumber;
                Brand = brandService.GetByGuid(member.BrandGuid).Name;

                BusinessName = member.BusinessName;
                FirstName = member.FirstName;
                LastName = member.LastName;
                Address1 = member.Address1;
                Address2 = member.Address2;
                Address3 = member.Address3;
                Town = member.Town;
                County = member.County;
                Postcode = member.Postcode;
                Telephone = member.Telephone;
                Mobile = member.Mobile;
                Email = member.Email;

                GdprSalesContact = member.GdprSalesContact;
                SalesEmail = member.SalesEmail;
                SalesTelephone = member.SalesTelephone;
                SalesMobile = member.SalesMobile;
                GdprOffers = member.GdprOffers;
                OffersEmail = member.OffersEmail;
                OffersTelephone = member.OffersTelephone;
                OffersMobile = member.OffersMobile;
                OffersPost = member.OffersPost;

                if (member.UmbracoMemberGuid.HasValue && member.UmbracoMemberGuid != Guid.Empty)
                {
                    var cmsMember = ApplicationContext.Current.Services.MemberService.GetByKey(member.UmbracoMemberGuid.Value);
                    if (cmsMember != null)
                    {
                        DateRegistered = cmsMember.CreateDate;
                        LastLoggedIn = cmsMember.LastLoginDate;
                    }
                    else
                    {
                        LogHelper.Error<ExportMember>($"Problem with exporting member details. Claim code: {ClaimCode}", null);
                    }
                }
            }
        }
    }
}
