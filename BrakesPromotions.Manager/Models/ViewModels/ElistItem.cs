﻿using Brakes.Elist.Core.Logic;

namespace BrakesPromotions.Manager.Models.ViewModels
{
    public class ElistItem
    {
        public int ProductListProductId { get; set; }
        public int Code { get; set; }
        public string ShortDescription { get; set; }
        public string SecondaryTitle { get; set; }
        public string Prefix { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string PackSize { get; set; }
        public string MinimumPackSize { get; set; }
        public string ApproxSizeCountPtn { get; set; }
        public string PriceEach { get; set; }
        public string ListPrice { get; set; }
        public string Category { get; set; }
        public string CategoryHierarchy1 { get; set; }
        public string CategoryHierarchy2 { get; set; }
        public string CategoryHierarchy3 { get; set; }
        public string CategoryUrl
        {
            get
            {
                return ProductList.MakeUrlSegment(Category);
            }
        }
        public string CategoryHierarchy1Url
        {
            get
            {
                return ProductList.MakeUrlSegment(CategoryHierarchy1);
            }
        }
        public string CategoryHierarchy2Url
        {
            get
            {
                return ProductList.MakeUrlSegment(CategoryHierarchy2);
            }
        }
        public string CategoryHierarchy3Url
        {
            get
            {
                return ProductList.MakeUrlSegment(CategoryHierarchy3);
            }
        }
        public string ItemUrlSegment
        {
            get
            {
                return ProductList.MakeUrlSegment(Name);
            }
        }
    }
}
