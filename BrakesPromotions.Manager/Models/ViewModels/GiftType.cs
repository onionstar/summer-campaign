﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrakesPromotions.Manager.Models.ViewModels
{
    public class GiftType
    {
        public Guid TypeGuid { get; set; }
        public string Name { get; set; }
        public string Suffix { get; set; }
        public string Prefix { get; set; }
        public int Position { get; set; }
        public bool HasMedia { get; set; }
    }
}
