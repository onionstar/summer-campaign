﻿namespace BrakesPromotions.Manager.Models.ViewModels
{
    // Namespaces
    using System;
    using System.Collections.Generic;

    public class Campaign
    {
        public Guid CampaignGuid { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public int MaxGifts { get; set; }

        public bool IsLive { get; set; }

        public DateTime CampaignStartDate { get; set; }

        public DateTime CampaignEndDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime? ClaimStartDate { get; set; }

        public DateTime? ClaimEndDate { get; set; }

        public ICollection<GiftType> AvailableGifts { get; set; }

        public ICollection<CampaignGiftType> AllocatedGiftTypes { get; set; }
    }
}
