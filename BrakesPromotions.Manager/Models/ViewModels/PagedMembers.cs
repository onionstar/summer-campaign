﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrakesPromotions.Manager.Models.ViewModels
{
    public class PagedMembers
    {

        public PagedMembers(long totalItems, long pageNumber, long pageSize)
        {
            TotalItems = totalItems;
            PageNumber = pageNumber;
            PageSize = pageSize;

            if (pageSize > 0)
            {
                TotalPages = (long)Math.Ceiling(totalItems / (Decimal)pageSize);
            }
            else
            {
                TotalPages = 1;
            }
        }

        public IList<Member> Items { get; set; }
        public long PageNumber { get; private set; }
        public long PageSize { get; private set; }
        public long TotalPages { get; private set; }
        public long TotalItems { get; private set; }

        public int GetSkipSize()
        {
            if (PageNumber > 0 && PageSize > 0)
            {
                return Convert.ToInt32((PageNumber - 1) * PageSize);
            }
            return 0;
        }
    }
}
