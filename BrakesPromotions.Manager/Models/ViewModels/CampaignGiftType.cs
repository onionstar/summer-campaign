﻿using System;

namespace BrakesPromotions.Manager.Models.ViewModels
{
    public class CampaignGiftType
    {
        public Guid TypeGuid { get; set; }
        public Guid CampaignGuid { get; set; }
    }
}
