﻿using System;
using System.Collections.Generic;

namespace BrakesPromotions.Manager.Models.ViewModels
{
    public class GiftTier
    {
        public Guid GiftTierGuid { get; set; }
        public Guid CampaignGuid { get; set; }
        public string Name { get; set; }
        public string ClaimCode { get; set; }
        public string Colour { get; set; }
        public int Position { get; set; }
        public IList<Gift> Gifts { get; set; }
    }
}
