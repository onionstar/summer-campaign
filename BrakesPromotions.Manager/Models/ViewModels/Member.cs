﻿using System;

namespace BrakesPromotions.Manager.Models.ViewModels
{
    public class Member
    {
        public Guid MemberGuid { get; set; }
        public Guid? UmbracoMemberGuid { get; set; }
        public bool HasRegistered => UmbracoMemberGuid.HasValue && UmbracoMemberGuid != Guid.Empty;
        public Guid BrandGuid { get; set; }
        public string TelesalesNumber { get; set; }
        public Guid CampaignGuid { get; set; }
        public Guid? TypeGuid { get; set; }
        public Guid? ClaimedGiftTierGuid { get; set; }
        public Guid? ClaimedGiftGuid { get; set; }
        public bool HasClaimed => ClaimedGiftTierGuid.HasValue && ClaimedGiftTierGuid != Guid.Empty;
        public string Urn { get; set; }
        public string ClaimCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string NectarCardNumber { get; set; }
        public string Type { get; set; }
        public decimal SpendBalance { get; set; }
        public bool NectarCustomer { get; set; }
        public int? NectarPointsBalance { get; set; }
        public int? NectarBonusPoints { get; set; }
        public int? NectarBonusTarget { get; set; }
        public double? NectarBonusMinimum { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public DateTime? ClaimedDate { get; set; }
        public int? TradeDiscount { get; set; }
        public int? Tokens { get; set; }
        public string BusinessName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string TradingState { get; set; }
        public bool? GdprSalesContact { get; set; }
        public bool? SalesEmail { get; set; }
        public bool? SalesTelephone { get; set; }
        public bool? SalesMobile { get; set; }
        public bool? GdprOffers { get; set; }
        public bool? OffersEmail { get; set; }
        public bool? OffersTelephone { get; set; }
        public bool? OffersMobile { get; set; }
        public bool? OffersPost { get; set; }
    }
}
