﻿using System;

namespace BrakesPromotions.Manager.Models.ViewModels
{
    public class Gift
    {
        public Guid GiftGuid { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public Guid MediaId { get; set; }
        public bool HasMedia { get; set; }
    }
}
