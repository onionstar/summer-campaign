﻿using System;

namespace BrakesPromotions.Manager.Models
{
    public class ExportResponse
    {
        public bool Success { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
