﻿using System;

namespace BrakesPromotions.Manager.Models
{
    public class ImportResponse
    {
        public bool Success { get; set; }
        public TimeSpan Duration { get; set; }
        public int AccountCount { get; set; }
        public int PromoCount { get; set; }
        public int TargetCount { get; set; }
    }
}
