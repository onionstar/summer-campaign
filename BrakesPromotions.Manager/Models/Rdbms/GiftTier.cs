﻿namespace BrakesPromotions.Manager.Models.Rdbms
{
    // Namespaces
    using System;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    [TableName("vpmGiftTier")]
    [PrimaryKey("GiftTierGuid", autoIncrement = false)]
    [ExplicitColumns]
    public class GiftTier
    {
        [Column("GiftTierGuid")]
        [PrimaryKeyColumn(AutoIncrement = false)]
        [Index(IndexTypes.UniqueNonClustered)]
        public Guid GiftTierGuid { get; set; }

        [Column("CampaignGuid")]
        [ForeignKey(typeof(Campaign))]
        public Guid CampaignGuid { get; set; }

        [Column("Name")]
        [Length(100)]
        public string Name { get; set; }

        [Column("ClaimCode")]
        [Length(100)]
        public string ClaimCode { get; set; }

        [Column("Colour")]
        public string Colour { get; set; }

        [Column("Position")]
        public int Position { get; set; }

        [ResultColumn]
        public Campaign Campaign { get; set; }
    }
}
