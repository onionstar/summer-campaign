﻿namespace BrakesPromotions.Manager.Models.Rdbms
{
    // Namespaces
    using System;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    [TableName("vpmGiftType")]
    [PrimaryKey("TypeGuid", autoIncrement = false)]
    [ExplicitColumns]
    public class GiftType
    {
        [Column("TypeGuid")]
        [PrimaryKeyColumn(AutoIncrement = false)]
        [Index(IndexTypes.UniqueNonClustered)]
        public Guid TypeGuid { get; set; }

        [Column("Name")]
        [Length(100)]
        public string Name { get; set; }

        [Column("Suffix")]
        [Length(100)]
        [NullSetting]
        public string Suffix { get; set; }

        [Column("Prefix")]
        [Length(100)]
        [NullSetting]
        public string Prefix { get; set; }

        [Column("Position")]
        public int Position { get; set; }

        [Column("HasMedia")]
        [NullSetting]
        public bool HasMedia { get; set; }
    }
}
