﻿namespace BrakesPromotions.Manager.Models.Rdbms
{
    // Namespaces
    using System;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    [TableName("vpmCampaign")]
    [PrimaryKey("CampaignGuid", autoIncrement = false)]
    [ExplicitColumns]
    public class Campaign
    {
        [Column("CampaignGuid")]
        [PrimaryKeyColumn(AutoIncrement = false)]
        public Guid CampaignGuid { get; set; }

        [Column("Name")]
        [Length(100)]
        public string Name { get; set; }

        [Column("Email")]
        [Length(100)]
        public string Email { get; set; }

        [Column("MaxGifts")]
        public int MaxGifts { get; set; }

        [Column("IsLive")]
        public bool IsLive { get; set; }

        [Column("CampaignStartDate")]
        [NullSetting]
        public DateTime? CampaignStartDate { get; set; }

        [Column("CampaignEndDate")]
        [NullSetting]
        public DateTime? CampaignEndDate { get; set; }

        [Column("CreatedDate")]
        public DateTime CreatedDate { get; set; }

        [Column("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }

        [Column("ClaimStartDate")]
        [NullSetting]
        public DateTime? ClaimStart { get; set; }

        [Column("ClaimEndDate")]
        [NullSetting]
        public DateTime? ClaimEnd { get; set; }
    }
}
