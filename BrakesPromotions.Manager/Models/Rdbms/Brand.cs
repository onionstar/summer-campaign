﻿namespace BrakesPromotions.Manager.Models.Rdbms
{
    // Namespaces
    using System;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    [TableName("vpmBrand")]
    [PrimaryKey("BrandGuid", autoIncrement = false)]
    [ExplicitColumns]
    public class Brand
    {
        [Column("BrandGuid")]
        [PrimaryKeyColumn(AutoIncrement = false)]
        [Index(IndexTypes.UniqueNonClustered)]
        public Guid BrandGuid { get; set; }

        [Column("Name")]
        [Length(100)]
        public string Name { get; set; }
    }
}
