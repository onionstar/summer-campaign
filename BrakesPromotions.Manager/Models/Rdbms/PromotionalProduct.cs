﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrakesPromotions.Manager.Models.Rdbms
{
    // Namespaces
    using System;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    [TableName("vpmPromoProduct")]
    [PrimaryKey("PromoGuid", autoIncrement = false)]
    [ExplicitColumns]
    public class PromotionalProduct
    {
        [Column("PromoGuid")]
        [PrimaryKeyColumn(AutoIncrement = false)]
        [Index(IndexTypes.UniqueNonClustered)]
        public Guid PromoGuid { get; set; }

        [Column("MemberGuid")]
        [ForeignKey(typeof(Member))]
        [NullSetting]
        public Guid MemberGuid { get; set; }

        [Column("BrakesProductId")]
        public int BrakesProductId { get; set; }

        [Column("PromoPrice")]
        public string PromoPrice { get; set; }

        [Column("Position")]
        public int Position { get; set; }
    }
}
