﻿namespace BrakesPromotions.Manager.Models.Rdbms
{
    // Namespaces
    using System;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    [TableName("vpmTarget")]
    [PrimaryKey("TargetGuid", autoIncrement = false)]
    [ExplicitColumns]
    public class Target
    {
        [Column("TargetGuid")]
        [PrimaryKeyColumn(AutoIncrement = false)]
        [Index(IndexTypes.UniqueNonClustered)]
        public Guid TargetGuid { get; set; }

        [Column("MemberGuid")]
        [ForeignKey(typeof(Member))]
        public Guid MemberGuid { get; set; }

        [Column("GiftTierGuid")]
        [ForeignKey(typeof(GiftTier))]
        public Guid GiftTierGuid { get; set; }

        [Column("TargetValue")]
        public int TargetValue { get; set; }
    }
}
