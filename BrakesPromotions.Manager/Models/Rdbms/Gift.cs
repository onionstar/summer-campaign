﻿namespace BrakesPromotions.Manager.Models.Rdbms
{
    // Namespaces
    using System;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    [TableName("vpmGift")]
    [PrimaryKey("GiftGuid", autoIncrement = false)]
    [ExplicitColumns]
    public class Gift
    {
        [Column("GiftGuid")]
        [PrimaryKeyColumn(AutoIncrement = false)]
        [Index(IndexTypes.UniqueNonClustered)]
        public Guid GiftGuid { get; set; }

        [Column("GiftTierGuid")]
        [ForeignKey(typeof(GiftTier))]
        public Guid GiftTierGuid { get; set; }

        [Column("TypeGuid")]
        [ForeignKey(typeof(GiftType))]
        public Guid TypeGuid { get; set; }

        [Column("Name")]
        [Length(100)]
        public string Name { get; set; }

        [Column("MediaId")]
        [NullSetting]
        public Guid MediaId { get; set; }

        [ResultColumn]
        public GiftTier GiftTier { get; set; }

        [ResultColumn]
        public GiftType GiftType { get; set; }
    }
}
