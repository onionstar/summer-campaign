﻿namespace BrakesPromotions.Manager.Models.Rdbms
{
    // Namespaces
    using System;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    [TableName("vpmMember")]
    [PrimaryKey("MemberGuid", autoIncrement = false)]
    [ExplicitColumns]
    public class Member
    {
        [Column("MemberGuid")]
        [PrimaryKeyColumn(AutoIncrement = false)]
        [Index(IndexTypes.UniqueNonClustered)]
        public Guid MemberGuid { get; set; }

        [Column("UmbracoMemberGuid")]
        [NullSetting]
        public Guid? UmbracoMemberGuid { get; set; }

        [Column("BrandGuid")]
        [ForeignKey(typeof(Brand))]
        public Guid BrandGuid { get; set; }

        [Column("TelesalesNumber")]
        public string TelesalesNumber { get; set; }

        [Column("CampaignGuid")]
        [ForeignKey(typeof(Campaign))]
        public Guid CampaignGuid { get; set; }

        [Column("TypeGuid")]
        [NullSetting]
        public Guid? TypeGuid { get; set; }

        [Column("ClaimedGiftTierGuid")]
        [NullSetting]
        public Guid? ClaimedGiftTierGuid { get; set; }

        [Column("ClaimedGiftGuid")]
        [NullSetting]
        public Guid? ClaimedGiftGuid { get; set; }

        //Registration & Personal info

        [Column("URN")]
        [Length(20)]
        public string Urn { get; set; }

        [Column("ClaimCode")]
        [Length(20)]
        public string ClaimCode { get; set; }

        [Column("FirstName")]
        [Length(50)]
        public string FirstName { get; set; }

        [Column("LastName")]
        [Length(50)]
        public string LastName { get; set; }

        [Column("Email")]
        [Length(200)]
        [NullSetting]
        public string Email { get; set; }

        [Column("NectarCardNumber")]
        [Length(50)]
        [NullSetting]
        public string NectarCardNumber { get; set; }

        [Column("Type")]
        [Length(50)]
        [NullSetting]
        public string Type { get; set; }

        [Column("SpendBalance")]
        public decimal SpendBalance { get; set; }

        // Bonus Nectar Data

        [Column("NectarCustomer")]
        public bool NectarCustomer { get; set; }

        [Column("NectarPointsBalance")]
        [NullSetting]
        public int? NectarPointsBalance { get; set; }

        [Column("NectarBonusPoints")]
        [NullSetting]
        public int? NectarBonusPoints { get; set; }

        [Column("NectarBonusTarget")]
        [NullSetting]
        public int? NectarBonusTarget { get; set; }

        [Column("NectarBonusDeliveryCount")]
        [NullSetting]
        public int? NectarBonusDeliveryCount { get; set; }

        [Column("NectarBonusMinimum")]
        [NullSetting]
        public double? NectarBonusMinimum { get; set; }

        [Column("Created")]
        public DateTime Created { get; set; }

        [Column("Updated")]
        public DateTime Updated { get; set; }

        [Column("ClaimedDate")]
        [NullSetting]
        public DateTime? ClaimedDate { get; set; }

        [Column("TradeDiscount")]
        [NullSetting]
        public int? TradeDiscount { get; set; }

        [Column("Tokens")]
        [NullSetting]
        public int? Tokens { get; set; }

        [Column("BusinessName")]
        [Length(100)]
        public string BusinessName { get; set; }

        [Column("Address1")]
        [Length(100)]
        [NullSetting]
        public string Address1 { get; set; }

        [Column("Address2")]
        [Length(100)]
        [NullSetting]
        public string Address2 { get; set; }

        [Column("Address3")]
        [Length(100)]
        [NullSetting]
        public string Address3 { get; set; }

        [Column("Town")]
        [Length(100)]
        [NullSetting]
        public string Town { get; set; }

        [Column("County")]
        [Length(100)]
        [NullSetting]
        public string County { get; set; }

        [Column("Postcode")]
        [Length(10)]
        [NullSetting]
        public string Postcode { get; set; }

        [Column("Telephone")]
        [Length(20)]
        public string Telephone { get; set; }

        [Column("Mobile")]
        [Length(20)]
        [NullSetting]
        public string Mobile { get; set; }

        [Column("TradingState")]
        [Length(100)]
        [NullSetting]
        public string TradingState { get; set; }

        // GDPR fields

        [Column("GdprSalesContact")]
        [NullSetting]
        public bool? GdprSalesContact { get; set; }

        [Column("SalesEmail")]
        [NullSetting]
        public bool? SalesEmail { get; set; }

        [Column("SalesTelephone")]
        [NullSetting]
        public bool? SalesTelephone { get; set; }

        [Column("SalesMobile")]
        [NullSetting]
        public bool? SalesMobile { get; set; }


        [Column("GdprOffers")]
        [NullSetting]
        public bool? GdprOffers { get; set; }

        [Column("OffersEmail")]
        [NullSetting]
        public bool? OffersEmail { get; set; }

        [Column("OffersTelephone")]
        [NullSetting]
        public bool? OffersTelephone { get; set; }

        [Column("OffersMobile")]
        [NullSetting]
        public bool? OffersMobile { get; set; }

        [Column("OffersPost")]
        [NullSetting]
        public bool? OffersPost { get; set; }


        [Column("AgreeTerms")]
        [NullSetting]
        public bool? AgreeTerms { get; set; }

        [Column("AgreePrivacyPolicy")]
        [NullSetting]
        public bool? AgreePrivacyPolicy { get; set; }

        // Additional claim form fields 

        [Column("IsBusinessOwner")]
        [NullSetting]
        public bool? IsBusinessOwner { get; set; }

        [Column("HasPermission")]
        [NullSetting]
        public bool? HasPermission { get; set; }

        [Column("BusinessOwnerName")]
        [Length(100)]
        [NullSetting]
        public string BusinessOwnerName { get; set; }
    }
}
