﻿namespace BrakesPromotions.Manager.Models.Rdbms
{
    // Namespaces
    using System;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    [TableName("vpmEventTrack")]
    [PrimaryKey("EventGuid", autoIncrement = false)]
    [ExplicitColumns]
    public class EventTrack
    {
        [Column("EventGuid")]
        [PrimaryKeyColumn(AutoIncrement = false)]
        [Index(IndexTypes.UniqueNonClustered)]
        public Guid EventGuid { get; set; }

        [Column("MemberGuid")]
        [NullSetting]
        public Guid MemberGuid { get; set; }

        [Column("CampaignGuid")]
        [ForeignKey(typeof(Campaign))]
        [NullSetting]
        public Guid CampaignGuid { get; set; }

        [Column("MediaId")]
        [NullSetting]
        public Guid? MediaId { get; set; }

        [Column("Event")]
        [Length(200)]
        public string Event { get; set; }

        [Column("EventDate")]
        public DateTime EventDate { get; set; }
    }
}
