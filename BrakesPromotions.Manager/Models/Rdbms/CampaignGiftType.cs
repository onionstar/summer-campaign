﻿namespace BrakesPromotions.Manager.Models.Rdbms
{
    // Namespaces
    using System;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    [TableName("vpmCampaignGiftType")]
    [ExplicitColumns]
    public class CampaignGiftType
    {
        [Column("TypeGuid")]
        [ForeignKey(typeof(GiftType))]
        public Guid TypeGuid { get; set; }

        [Column("CampaignGuid")]
        [ForeignKey(typeof(Campaign))]
        public Guid CampaignGuid { get; set; }
    }
}
