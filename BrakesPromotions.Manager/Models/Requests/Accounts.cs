﻿using System;
using Umbraco.Core.Persistence.DatabaseModelDefinitions;

namespace BrakesPromotions.Manager.Models.Requests
{
    public class Accounts
    {
        public Guid CampaignGuid { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public Direction OrderDirection { get; set; }
        public string Filter { get; set; }
        public Filters Filters { get; set; }
    }

    public class Filters
    {
        public string StatusFilter { get; set; }
        public BrandFilter BrandFilter { get; set; }
    }

    public class BrandFilter
    {
        public string Name { get; set; }
        public Guid BrandGuid { get; set; }
    }
}
