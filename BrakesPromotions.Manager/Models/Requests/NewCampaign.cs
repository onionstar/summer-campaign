﻿using System;

namespace BrakesPromotions.Manager.Models.Requests
{
    public class NewCampaign
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
