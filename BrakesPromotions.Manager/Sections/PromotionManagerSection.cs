﻿namespace BrakesPromotions.Manager.Sections
{
    // Namespaces
    using Umbraco.Core;
    using Umbraco.Core.Models;

    public class PromotionsManagerSection : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbraco, ApplicationContext context)
        {

            Section section = context.Services.SectionService.GetByAlias(PromotionsManager.SectionAlias);
            if (section != null) return;

            context.Services.SectionService.MakeNew(PromotionsManager.SectionName, PromotionsManager.SectionAlias, PromotionsManager.SectionIcon);

        }
    }
}
