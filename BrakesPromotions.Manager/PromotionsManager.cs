﻿namespace BrakesPromotions.Manager
{
    public class PromotionsManager
    {
        public const string VersionKey = @"1.0.0";

        public const string SectionAlias = @"promotionsmanager";
        public const string SectionName = @"Promotions Manager";
        public const string SectionIcon = @"icon-gift";

        public const string TreeAlias = @"promotions";
        public const string PluginName = @"PromotionsManager";
        public const string PluginTitle = @"Promotions Manager";

        public const string MenuActionsPath = @"/App_Plugins/promotionsmanager/backoffice/promotionsmanager/dialogs/";
    }
}
