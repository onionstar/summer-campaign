﻿namespace BrakesPromotions.Manager.Trees
{
    using Actions;
    using BrakesPromotions.Manager.Helpers;
    using BrakesPromotions.Manager.Services;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http.Formatting;
    using Umbraco.Core;
    using Umbraco.Web.Models.Trees;
    using Umbraco.Web.Mvc;
    using Umbraco.Web.Trees;

    using CoreConstants = Umbraco.Core.Constants;

    [PluginController(PromotionsManager.PluginName)]
    [Tree(PromotionsManager.SectionAlias, PromotionsManager.TreeAlias, PromotionsManager.PluginName, "icon-folder", "icon-folder-open", true, sortOrder: 0)]
    public sealed class PromotionsManagerTreeController : TreeController
    {
        private static CampaignService campaignService = new CampaignService();
        private static GiftTierService giftTierService = new GiftTierService();

        protected override TreeNode CreateRootNode(FormDataCollection queryStrings)
        {
            var root = base.CreateRootNode(queryStrings);

            //this will load in a custom UI instead of the dashboard for the root node
            root.RoutePath = string.Format("{0}/{1}/{2}/{3}", PromotionsManager.SectionAlias, PromotionsManager.TreeAlias, "overview", "0");
            root.Icon = "icon-gift";

            root.HasChildren = false;
            return root;
        }

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            var menu = new MenuItemCollection();
            var rootId = CoreConstants.System.Root.ToInvariantString();
            var formatUrl = PromotionsManager.SectionAlias + "/" + PromotionsManager.TreeAlias + "/{0}";

            if (id.InvariantEquals(rootId))
            {
                menu.Items.Add<ImportCampaignAction>("New Campaign", false).NavigateToRoute(string.Format(formatUrl, "createCampaign"));
            }

            else if (id.Contains("campaign_"))
            {
                var additionalAdditional = new Dictionary<string, object>()
                {
                    { "Id", id.Replace("campaign_", "") }
                };
                menu.Items.Add<ImportCampaignAction>(
                    "New Gift Tier", false, additionalData: additionalAdditional).LaunchDialogView(PromotionsManager.MenuActionsPath + "new.giftTier.html", "New Gift Tier");
            }

            else switch (id)
                {
                    case "campaigns":
                        menu.Items.Add<ImportCampaignAction>(
                            "Update Campaign", false).LaunchDialogView(PromotionsManager.MenuActionsPath + "update.campaign.html", "Update campaign data");
                        break;
                    case "import_campaign":
                        menu.Items.Add<ImportCampaignAction>(
                            "New Campaign", false).LaunchDialogView(PromotionsManager.MenuActionsPath + "import.campaign.html", "Import new campaign data");
                        break;
                    case "import_bonus":
                        menu.Items.Add<ImportCampaignAction>(
                            "New Campaign Bonus", false).LaunchDialogView(PromotionsManager.MenuActionsPath + "import.bonus.html", "Import new bonus data");
                        break;
                    case "import_update":
                        menu.Items.Add<ImportCampaignAction>(
                            "Update Campaign", false).LaunchDialogView(PromotionsManager.MenuActionsPath + "update.campaign.html", "Update campaign data");
                        break;
                    case "export":
                        menu.Items.Add<ImportCampaignAction>(
                            "Export Campaign", false).LaunchDialogView(PromotionsManager.MenuActionsPath + "export.campaign.html", "Export campaign data");
                        break;
                    default:
                        return null;
                }

            return menu;
        }

        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            var entityId = GuidHelper.GetGuid(id);
            var nodes = new TreeNodeCollection();
            var rootId = CoreConstants.System.Root.ToInvariantString();

            var isAdmin = ApplicationContext.Services.UserService.GetUserById(umbraco.helper.GetCurrentUmbracoUser().Id).Groups.FirstOrDefault(x => x.Alias.Equals("admin")) != null;

            if (id.InvariantEquals(rootId))
            {
                var formatUrl = PromotionsManager.SectionAlias + "/" + PromotionsManager.TreeAlias + "/{0}";

                var item = CreateTreeNode("campaigns",
                    id,
                    queryStrings,
                    "Campaigns",
                    "icon-folder",
                    true,
                    string.Format(formatUrl, "overview"));
                nodes.Add(item);

                item = CreateTreeNode("accounts",
                    id,
                    queryStrings,
                    "Acounts",
                    "icon-folder",
                    true,
                    string.Format(formatUrl, "AccountsManager", "overview"));
                nodes.Add(item);

                item = CreateTreeNode("import",
                    id, queryStrings,
                    "Import",
                    "icon-folder",
                    true,
                    string.Format(formatUrl, "AccountsManager", "import"));
                nodes.Add(item);

                item = CreateTreeNode("export",
                    id,
                    queryStrings,
                    "Export",
                    "icon-folder",
                    false,
                    string.Format(formatUrl, "AccountsManager", "export"));
                nodes.Add(item);

                return nodes;
            }

            if (id == "campaigns")
            {
                var AllCampaigns = campaignService.GetAll();
                foreach (var campaign in AllCampaigns)
                {
                    var CampaignGiftTiers = giftTierService.GetByCampaignGuid(campaign.CampaignGuid);
                    var giftTierCount = CampaignGiftTiers != null && CampaignGiftTiers.Any() ? CampaignGiftTiers.Count() : 0;
                    var campaignGiftCount = campaign.MaxGifts;

                    if (giftTierCount < campaignGiftCount)
                    {
                        var item = CreateTreeNode("campaign_" + campaign.CampaignGuid.ToString(), "campaigns", queryStrings, campaign.Name, "icon-document", true, "PromotionsManager/PromotionsManager/editCampaign/" + campaign.CampaignGuid.ToString());
                        nodes.Add(item);
                    }
                    else
                    {
                        var item = CreateTreeNode("campaign_" + campaign.CampaignGuid.ToString(), "campaigns", queryStrings, campaign.Name, "icon-document", true, "PromotionsManager/PromotionsManager/editCampaign/" + campaign.CampaignGuid.ToString());
                        item.MenuUrl = null;
                        nodes.Add(item);
                    }
                }
                return nodes;
            }

            if (id == "members")
            {
                //var AllCampaigns = CampaignRepository.GetAll();
                //foreach (var campaign in AllCampaigns)
                //{
                //    var item = CreateTreeNode("members_" + campaign.CampaignGuid.ToString(), "members", queryStrings, campaign.Name, "icon-document", false, "PromotionManager/PromotionManager/campaignMembers/" + campaign.CampaignGuid.ToString());
                //    item.MenuUrl = null;
                //    nodes.Add(item);
                //}
                return nodes;
            }

            if (id == "import")
            {
                //if (canImportNew)
                //{
                //    var item = CreateTreeNode("import_campaign", id, queryStrings, "Import New Campaign", "icon-document", false, "PromotionManager");
                //    nodes.Add(item);

                //    item = CreateTreeNode("import_update", id, queryStrings, "Update Campaign", "icon-document", false, "PromotionManager");
                //    nodes.Add(item);

                //    item = CreateTreeNode("import_bonus", id, queryStrings, "New Campaign Bonus", "icon-document", false, "PromotionManager");
                //    nodes.Add(item);
                //}
                //else
                //{
                //    var item = CreateTreeNode("import_update", id, queryStrings, "Update Campaign", "icon-document", false, "PromotionManager");
                //    nodes.Add(item);
                //}

                return nodes;
            }

            if (id == "export")
            {
                return nodes;
            }

            if (campaignService.GetAll().Any())
            {
                var campaigns = campaignService.GetAll();
                foreach (var campaign in campaigns.Where(x => id == "campaign_" + x.CampaignGuid.ToString() && x.MaxGifts > 0))
                {
                    var CampaignGiftTiers = giftTierService.GetByCampaignGuid(campaign.CampaignGuid);

                    foreach (var giftTier in CampaignGiftTiers)
                    {
                        var item = CreateTreeNode("giftTier_" + giftTier.GiftTierGuid.ToString(), "campaign_" + campaign, queryStrings, giftTier.Name, "icon-document", false, "PromotionsManager/PromotionsManager/editGiftTier/" + giftTier.GiftTierGuid.ToString());
                        item.MenuUrl = null;
                        nodes.Add(item);
                    }
                }

                return nodes;
            }

            throw new NotSupportedException();
        }
    }
}
