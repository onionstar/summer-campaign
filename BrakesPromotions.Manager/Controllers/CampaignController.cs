﻿namespace BrakesPromotions.Manager.Controllers
{
    // Namespaces
    using Manager.Actions;
    using Manager.Helpers;
    using Manager.Services;
    using Models;
    using Models.Requests;
    using Models.ViewModels;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web;
    using System.Web.Http;
    using Umbraco.Core.IO;
    using Umbraco.Web.Editors;
    using Umbraco.Web.WebApi;

    public class CampaignController : UmbracoAuthorizedJsonController
    {
        private static bool TestMode => bool.Parse(ConfigurationManager.AppSettings["Visarc.Testmode"]);
        private static CampaignService campaignService = new CampaignService();
        private static GiftTypeService giftTypeService = new GiftTypeService();
        private static AllocationsService allocationService = new AllocationsService();
        private static GiftTierService giftTierService = new GiftTierService();
        private static GiftService giftService = new GiftService();
        private static MemberService memberservice = new MemberService();
        private static BrandService brandService = new BrandService();

        [HttpGet]
        public IList<Campaign> GetAllCampaigns() => campaignService.GetAll();

        [HttpGet]
        public IList<Campaign> GetActiveCampaigns() => campaignService.GetAllActive();

        [HttpGet]
        public IList<Campaign> GetInactiveCampaigns() => campaignService.GetAllInactive();

        [HttpGet]
        public Campaign GetCampaign(Guid CampaignGuid)
        {
            var campaign = campaignService.GetByGuid(CampaignGuid);

            AutoMapper.Mapper.CreateMap<Models.Rdbms.GiftType, GiftType>();
            campaign.AvailableGifts = AutoMapper.Mapper.Map<IList<Models.Rdbms.GiftType>, IList<GiftType>>(giftTypeService.GetAll());
            campaign.AllocatedGiftTypes = allocationService.GetAllAllocatedToCampaign(campaign.CampaignGuid);

            return campaign;
        }

        [HttpGet]
        public Campaign EnableCampaign(Guid CampaignGuid) => campaignService.EnableCampaign(CampaignGuid);

        [HttpGet]
        public Campaign DisableCampaign(Guid CampaignGuid) => campaignService.DisableCampaign(CampaignGuid);

        [HttpPost]
        public NewCampaign SetNewCampaign(NewCampaign request)
        {
            //Services.MemberGroupService.Save(new MemberGroup { Name = request.Name, CreateDate = DateTime.UtcNow });
            return campaignService.SetNewCampaign(request);
        }

        [HttpPost]
        public Campaign UpdateCampaign(Campaign campaign)
        {
            var currentCampaign = campaignService.GetByGuid(campaign.CampaignGuid);

            campaign.CreatedDate = currentCampaign.CreatedDate;

            campaign.UpdatedDate = DateTime.UtcNow;
            campaignService.Update(campaign);

            foreach (var allocation in allocationService.GetAllAllocatedToCampaign(campaign.CampaignGuid))
            {
                if (!campaign.AllocatedGiftTypes.Any(x => x.TypeGuid == allocation.TypeGuid))
                {
                    allocationService.DeleteByGuid(campaign.CampaignGuid, allocation.TypeGuid);
                }
                else
                {
                    campaign.AllocatedGiftTypes.Remove(campaign.AllocatedGiftTypes.FirstOrDefault(x => x.TypeGuid == allocation.TypeGuid));
                }
            }
            foreach (var newAllocation in campaign.AllocatedGiftTypes)
            {
                allocationService.Insert(newAllocation);
            }

            return campaign;
        }

        [HttpPost]
        public ImportResponse ImportCampaignAccounts()
        {
            var myContext = Request.TryGetHttpContext();
            if (myContext.Success)
            {
                HttpPostedFileBase myFile = myContext.Result.Request.Files["file"];
                Guid CampaignGuid = GuidHelper.GetGuid(myContext.Result.Request.Form["campaignGuid"]);
                string Password = myContext.Result.Request.Form["password"];

                if (myFile == null)
                {
                    throw new HttpException("invalid file");
                }
                return new AccountImporter().ImportAccounts(myFile, CampaignGuid, Password);
            }
            return new ImportResponse { Success = false };
        }

        [HttpPost]
        public ImportResponse UpdateCampaignAccounts()
        {
            var myContext = Request.TryGetHttpContext();
            if (myContext.Success)
            {
                HttpPostedFileBase myFile = myContext.Result.Request.Files["file"];
                Guid CampaignGuid = GuidHelper.GetGuid(myContext.Result.Request.Form["campaignGuid"]);
                string Password = myContext.Result.Request.Form["password"];
                bool IncludePromos = bool.Parse(myContext.Result.Request.Form["includePromo"]);
                bool IncludeTargets = bool.Parse(myContext.Result.Request.Form["includeTargets"]);


                if (myFile == null)
                {
                    throw new HttpException("invalid file");
                }
                return new AccountUpdater().UpdateAccounts(myFile, CampaignGuid, Password, doPromotions: IncludePromos, doTargets: IncludeTargets);
            }
            return new ImportResponse { Success = false };
        }

        [HttpPost]
        public HttpResponseMessage ExportCampaignAccounts()
        {
            var myContext = Request.TryGetHttpContext();
            if (myContext.Success)
            {
                Guid CampaignGuid = GuidHelper.GetGuid(myContext.Result.Request.Form["campaignGuid"]);
                string Password = myContext.Result.Request.Form["password"];

                var accountExporter = new AccountExporter();
                var path = accountExporter.GetExcelSheet(CampaignGuid, Password);

                FileStream fileStream = new FileStream(IOHelper.MapPath(path), FileMode.Open);

                return new HttpResponseMessage() { Content = new StreamContent(fileStream) };
            }

            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        // Gift Management
        [HttpGet]
        public List<GiftTier> GetGiftTiers(Guid CampaignGuid)
        {
            var Tiers = new List<GiftTier>();

            var giftTierModel = giftTierService.GetByCampaignGuid(CampaignGuid);

            if (giftTierModel != null)
            {
                foreach (var giftTier in giftTierModel)
                {
                    Tiers.Add(new GiftTier
                    {
                        Name = giftTier.Name,
                        GiftTierGuid = giftTier.GiftTierGuid,
                        Gifts = GetGifts(giftTier.GiftTierGuid),
                        CampaignGuid = CampaignGuid,
                        ClaimCode = giftTier.ClaimCode,
                        Colour = $"#{giftTier.Colour}",
                        Position = giftTier.Position
                    });
                }
            }

            return Tiers;
        }

        [HttpPost]
        public GiftTier UpdateGiftTier(GiftTier giftTier)
        {
            var currentGiftTier = giftTierService.GetByGiftTierGuid(giftTier.GiftTierGuid);

            currentGiftTier.ClaimCode = giftTier.ClaimCode;
            currentGiftTier.Colour = giftTier.Colour.Replace("#", "");
            currentGiftTier.Name = giftTier.Name;

            giftTierService.Update(currentGiftTier);

            foreach (var gift in giftTier.Gifts)
            {
                var currentGift = giftService.GetByGiftGuid(gift.GiftGuid);
                currentGift.Name = gift.Value;
                currentGift.MediaId = gift.MediaId;

                giftService.Update(currentGift);
            }

            return giftTier;
        }

        private List<Gift> GetGifts(Guid GiftTierGuid)
        {
            var Gifts = new List<Gift>();

            var giftModel = giftService.GetByGiftTierGuid(GiftTierGuid);

            if (giftModel != null)
            {
                foreach (var gift in giftModel)
                {
                    var hasMedia = giftTypeService.GetByTypeGuid(gift.TypeGuid).HasMedia;
                    Gifts.Add(new Gift
                    {
                        GiftGuid = gift.GiftGuid,
                        Name = string.Format(giftService.FullGiftName(gift.GiftGuid, gift.TypeGuid).Replace("&pound;", "£"), "Brakes / Woodward"),
                        Value = gift.Name,
                        MediaId = gift.MediaId,
                        HasMedia = hasMedia
                    });
                }
            }

            return Gifts;
        }

        // Account Management
        [HttpPost]
        public PagedMembers GetAccounts(Accounts request)
        {
            return memberservice.GetPagedByCampaignGuid(request);
        }

        [HttpGet]
        public IList<Models.Rdbms.Brand> GetBrands()
        {
            return brandService.GetAll();
        }

        public Member UpdateAccount(Member account)
        {
            var currentAccount = memberservice.GetByClaimCode(account.ClaimCode, account.CampaignGuid);

            currentAccount.BusinessName = account.BusinessName;
            currentAccount.Address1 = account.Address1;
            currentAccount.Address2 = account.Address2;
            currentAccount.Address3 = account.Address3;
            currentAccount.Town = account.Town;
            currentAccount.County = account.County;
            currentAccount.Postcode = account.Postcode;
            currentAccount.Telephone = account.Telephone;
            currentAccount.NectarCardNumber = account.NectarCardNumber;
            currentAccount.NectarBonusTarget = account.NectarBonusTarget;
            currentAccount.NectarBonusMinimum = account.NectarBonusMinimum;
            currentAccount.TradingState = account.TradingState;
            currentAccount.TradeDiscount = account.TradeDiscount;
            currentAccount.Tokens = account.Tokens;
            currentAccount.NectarPointsBalance = account.NectarPointsBalance;
            currentAccount.SpendBalance = account.SpendBalance;

            currentAccount.Updated = DateTime.UtcNow;

            memberservice.Update(currentAccount, Guid.Empty);

            AutoMapper.Mapper.CreateMap<Models.Rdbms.Member, Member>();
            return AutoMapper.Mapper.Map<Models.Rdbms.Member, Member>(currentAccount);

        }
    }
}
