﻿using System;

namespace BrakesPromotions.Manager.Controllers
{
    //Namespaces
    using Models.ViewModels;
    using System.Linq;
    using Umbraco.Core.Persistence;
    public class ElistController
    {
        public ElistItem GetProductFromId(string productCode)
        {
            var cacheKey = $"Product{productCode}";
            var data = (ElistItem)System.Web.HttpRuntime.Cache[cacheKey];

            if (data == null)
            {
                using (var dataContext = new Database("elist"))
                {
                    var query =
                        new Sql().Select("Name, Code, Prefix, ShortDescription, SecondaryTitle, PriceEach, PackSize, Category, CategoryHierarchy1, CategoryHierarchy2, CategoryHierarchy3, ListPrice, MinimumPackSize, ApproxSizeCountPtn, ProductId")
                                 .From("Product")
                                 .Where("Code = @0 AND ShowOnWeb = 1 AND (StatusId = 1 OR StatusId = 4) AND IsTempProduct = 0", productCode);

                    data = dataContext.Fetch<ElistItem>(query).FirstOrDefault();

                    System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        data,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.High,
                        null
                        );
                }
            }

            return data;
        }
    }
}
