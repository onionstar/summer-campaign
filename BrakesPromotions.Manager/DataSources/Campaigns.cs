﻿namespace BrakesPromotions.Manager.DataSources
{
    // Namespaces
    using nuPickers.Shared.DotNetDataSource;
    using Services;
    using System.Collections.Generic;
    using System.Linq;

    public class Campaigns : IDotNetDataSource
    {
        private static CampaignService campaignService = new CampaignService();
        public IEnumerable<KeyValuePair<string, string>> GetEditorDataItems(int contextId)
        {
            List<KeyValuePair<string, string>> returnData = new List<KeyValuePair<string, string>>();
            foreach (var campaign in campaignService.GetAll())
            {
                returnData.Add(new KeyValuePair<string, string>(campaign.CampaignGuid.ToString(), campaign.Name));
            }
            return returnData.Distinct();
        }
    }
}
