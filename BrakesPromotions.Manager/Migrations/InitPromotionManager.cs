﻿namespace BrakesPromotions.Manager.Migrations
{
    // Namespaces
    using Models.Rdbms;
    using Umbraco.Core;
    using Umbraco.Core.Logging;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.Migrations;
    using Umbraco.Core.Persistence.SqlSyntax;

    [Migration("1.0.0", 1, "PromotionsManager")]
    public class InitPromotionManager : MigrationBase
    {
        private readonly UmbracoDatabase _database = ApplicationContext.Current.DatabaseContext.Database;
        private readonly DatabaseSchemaHelper _schemaHelper;

        public InitPromotionManager(ISqlSyntaxProvider sqlSyntax, ILogger logger) : base(sqlSyntax, logger)
        {
            _schemaHelper = new DatabaseSchemaHelper(_database, logger, sqlSyntax);
        }

        public override void Down()
        {
            Logger.Info<InitPromotionManager>("1.0.0: Running Migration Down");


            if (_schemaHelper.TableExist("vpmTarget"))
            {
                _schemaHelper.DropTable<Target>();
            }
            if (_schemaHelper.TableExist("vpmEventTrack"))
            {
                _schemaHelper.DropTable<EventTrack>();
            }
            if (_schemaHelper.TableExist("vpmPromoProduct"))
            {
                _schemaHelper.DropTable<PromotionalProduct>();
            }
            if (_schemaHelper.TableExist("vpmMember"))
            {
                _schemaHelper.DropTable<Member>();
            }
            if (_schemaHelper.TableExist("vpmBrand"))
            {
                _schemaHelper.DropTable<Brand>();
            }
            if (_schemaHelper.TableExist("vpmCampaignGiftType"))
            {
                _schemaHelper.DropTable<CampaignGiftType>();
            }
            if (_schemaHelper.TableExist("vpmGift"))
            {
                _schemaHelper.DropTable<Gift>();
            }
            if (_schemaHelper.TableExist("vpmGiftTier"))
            {
                _schemaHelper.DropTable<GiftTier>();
            }
            if (_schemaHelper.TableExist("vpmGiftType"))
            {
                _schemaHelper.DropTable<GiftType>();
            }
            if (_schemaHelper.TableExist("vpmCampaign"))
            {
                _schemaHelper.DropTable<Campaign>();
            }
        }

        public override void Up()
        {
            Logger.Info<InitPromotionManager>("1.0.0: Running Migration Up");

            if (!_schemaHelper.TableExist("vpmCampaign"))
            {
                _schemaHelper.CreateTable<Campaign>(false);
            }
            if (!_schemaHelper.TableExist("vpmGiftType"))
            {
                _schemaHelper.CreateTable<GiftType>(false);
            }
            if (!_schemaHelper.TableExist("vpmGiftTier"))
            {
                _schemaHelper.CreateTable<GiftTier>(false);
            }
            if (!_schemaHelper.TableExist("vpmGift"))
            {
                _schemaHelper.CreateTable<Gift>(false);
            }
            if (!_schemaHelper.TableExist("vpmCampaignGiftType"))
            {
                _schemaHelper.CreateTable<CampaignGiftType>(false);
            }
            if (!_schemaHelper.TableExist("vpmBrand"))
            {
                _schemaHelper.CreateTable<Brand>(false);
            }
            if (!_schemaHelper.TableExist("vpmMember"))
            {
                _schemaHelper.CreateTable<Member>(false);
            }
            if (!_schemaHelper.TableExist("vpmPromoProduct"))
            {
                _schemaHelper.CreateTable<PromotionalProduct>(false);
            }
            if (!_schemaHelper.TableExist("vpmEventTrack"))
            {
                _schemaHelper.CreateTable<EventTrack>(false);
            }
            if (!_schemaHelper.TableExist("vpmTarget"))
            {
                _schemaHelper.CreateTable<Target>(false);
            }
        }

    }
}
