﻿namespace BrakesPromotions.Manager.Migrations
{
    // Namespaces
    using Models.Rdbms;
    using Umbraco.Core;
    using Umbraco.Core.Logging;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.Migrations;
    using Umbraco.Core.Persistence.SqlSyntax;

    [Migration("1.1.0", 1, "PromotionsManager")]
    public class ClaimFormUpdate : MigrationBase
    {
        private readonly UmbracoDatabase _database = ApplicationContext.Current.DatabaseContext.Database;
        private readonly DatabaseSchemaHelper _schemaHelper;

        public ClaimFormUpdate(ISqlSyntaxProvider sqlSyntax, ILogger logger) : base(sqlSyntax, logger)
        {
            _schemaHelper = new DatabaseSchemaHelper(_database, logger, sqlSyntax);
        }

        public override void Down()
        {
            Logger.Info<ClaimFormUpdate>("1.1.0: Running Migration Down");

            if (_schemaHelper.TableExist("vpmMember"))
            {
                Delete.Column("IsBusinessOwner").FromTable("vpmMember");
                Delete.Column("HasPermission").FromTable("vpmMember");
                Delete.Column("BusinessOwnerName").FromTable("vpmMember");
            }
        }

        public override void Up()
        {
            Logger.Info<ClaimFormUpdate>("1.1.0: Running Migration Up");

            if (!_schemaHelper.TableExist("vpmMember"))
            {
                _schemaHelper.CreateTable<Member>(false);
            }
            else
            {
                Alter.Table("vpmMember").AddColumn("IsBusinessOwner").AsBoolean().Nullable();
                Alter.Table("vpmMember").AddColumn("HasPermission").AsBoolean().Nullable();
                Alter.Table("vpmMember").AddColumn("BusinessOwnerName").AsString(100).Nullable();
            }

        }
    }
}
