﻿using BrakesPromotions.Manager.Models.Rdbms;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;

namespace BrakesPromotions.Manager.Services
{
    public class TargetService
    {
        public TargetService() { }

        public IList<Target> GetAll(Guid MemberGuid)
        {
            var cacheKey = $"CachedTargets{MemberGuid}";
            var targets = (List<Target>)System.Web.HttpRuntime.Cache[cacheKey];

            if (targets == null || !targets.Any())
            {
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    targets = _db.Fetch<Target>("SELECT vpmTarget.* FROM vpmTarget INNER JOIN vpmGiftTier ON vpmTarget.GiftTierGuid = vpmGiftTier.GiftTierGuid WHERE vpmTarget.MemberGuid = @0 ORDER BY vpmGiftTier.Position ASC", MemberGuid);
                    System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        targets,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.High,
                        null
                        );
                }
            }

            return targets;
        }

        public void Insert(Target target, bool bulkAction = false)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Insert(target);
            }
            if (!bulkAction)
            {
                var cacheKey = $"CachedTarget{target.MemberGuid}{target.GiftTierGuid}";
                ClearCache(cacheKey);
            }
        }

        public void Update(Target target, bool bulkAction = false)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Update(target);
            }
            if (!bulkAction)
            {
                var cacheKey = $"CachedTarget{target.MemberGuid}{target.GiftTierGuid}";
                ClearCache(cacheKey);
            }
        }

        public void ClearCache(string cacheKey = "CachedTarget")
        {
            if (cacheKey != "CachedTarget")
            {
                System.Web.HttpRuntime.Cache.Remove(cacheKey);
            }
            else
            {
                foreach (System.Collections.DictionaryEntry cachedMember in System.Web.HttpRuntime.Cache)
                {
                    if (cachedMember.Key.ToString().StartsWith(cacheKey))
                    {
                        System.Web.HttpRuntime.Cache.Remove((string)cachedMember.Key);
                    }
                }
            }
        }
    }
}
