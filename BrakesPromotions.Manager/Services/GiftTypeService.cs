﻿namespace BrakesPromotions.Manager.Services
{
    // Namespaces
    using Models.Rdbms;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Umbraco.Core;

    public class GiftTypeService
    {
        public GiftTypeService() { }

        public IList<GiftType> GetAll()
        {
            var cacheKey = "CachedGiftTypes";
            var giftTypes = (List<GiftType>)System.Web.HttpRuntime.Cache[cacheKey];

            if (giftTypes == null || !giftTypes.Any())
            {
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    giftTypes = _db.Fetch<GiftType>("SELECT * FROM vpmGiftType");
                }
                System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        giftTypes,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.High,
                        null
                        );
            }
            return giftTypes;
        }

        public GiftType GetByTypeGuid(Guid GiftTypeGuid)
        {
            return GetAll().FirstOrDefault(x => x.TypeGuid == GiftTypeGuid);
        }

        private void ClearCache()
        {
            var cacheKey = "CachedGiftTypes";
            System.Web.HttpRuntime.Cache.Remove(cacheKey);
        }
    }
}
