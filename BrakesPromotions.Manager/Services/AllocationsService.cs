﻿namespace BrakesPromotions.Manager.Services
{
    // Namespaces
    using AutoMapper;
    using Models.ViewModels;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Umbraco.Core;

    public class AllocationsService
    {
        public AllocationsService() { }

        internal IList<CampaignGiftType> GetAllAllocated()
        {
            var cacheKey = $"CachedAllocatedGiftTypes";
            var allocatedGiftTypes = (List<CampaignGiftType>)System.Web.HttpRuntime.Cache[cacheKey];

            if (allocatedGiftTypes == null || !allocatedGiftTypes.Any())
            {
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    allocatedGiftTypes = _db.Fetch<CampaignGiftType>("SELECT * FROM vpmCampaignGiftType");
                }
                System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        allocatedGiftTypes,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.High,
                        null
                        );
            }
            return allocatedGiftTypes;
        }

        internal IList<CampaignGiftType> GetAllAllocatedToCampaign(Guid CampaignGuid) => GetAllAllocated().Where(x => x.CampaignGuid == CampaignGuid).ToList();

        internal void Update(CampaignGiftType allocation)
        {
            Mapper.CreateMap<CampaignGiftType, Models.Rdbms.CampaignGiftType>();
            var allocationModel = Mapper.Map<CampaignGiftType, Models.Rdbms.CampaignGiftType>(allocation);
            Update(allocationModel);

        }

        internal void Insert(CampaignGiftType allocation)
        {
            Mapper.CreateMap<CampaignGiftType, Models.Rdbms.CampaignGiftType>();
            var allocationModel = Mapper.Map<CampaignGiftType, Models.Rdbms.CampaignGiftType>(allocation);
            Insert(allocationModel);
        }

        private void Insert(Models.Rdbms.CampaignGiftType allocation)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Insert(allocation);
            }
            ClearCache();
        }

        private void Update(Models.Rdbms.CampaignGiftType allocation)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Update(allocation);
            }
            ClearCache();
        }

        internal void DeleteByGuid(Guid CampaignGuid, Guid TypeGuid)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Execute("DELETE FROM vpmCampaignGiftType WHERE CampaignGuid = @0 AND TypeGuid = @1", CampaignGuid, TypeGuid);
            }
            ClearCache();
        }

        private void ClearCache()
        {
            var cacheKey = "CachedAllocatedGiftTypes";
            System.Web.HttpRuntime.Cache.Remove(cacheKey);
        }
    }
}
