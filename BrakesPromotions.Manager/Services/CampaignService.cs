﻿namespace BrakesPromotions.Manager.Services
{
    // Namespaces
    using AutoMapper;
    using Models.Requests;
    using Models.ViewModels;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Umbraco.Core;
    using Umbraco.Core.Logging;
    using Umbraco.Core.Persistence;

    public class CampaignService
    {
        public CampaignService() { }

        public IList<Campaign> GetAll()
        {
            var cacheKey = "CachedCampaigns";
            var campaigns = (List<Campaign>)System.Web.HttpRuntime.Cache[cacheKey];

            if (campaigns == null || !campaigns.Any())
            {
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    campaigns = _db.Fetch<Campaign>("SELECT * FROM vpmCampaign");
                }
                System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        campaigns,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.High,
                        null
                        );
            }
            return campaigns;
        }

        internal NewCampaign SetNewCampaign(NewCampaign request)
        {
            var newCampaign = new Campaign
            {
                Name = request.Name,
                Email = request.Email,
                CampaignStartDate = DateTime.Parse(request.StartDate),
                CampaignEndDate = DateTime.Parse(request.EndDate),
                CampaignGuid = Guid.NewGuid(),
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow
            };
            Mapper.CreateMap<Campaign, Models.Rdbms.Campaign>();
            try
            {
                var CampaignModel = Mapper.Map<Campaign, Models.Rdbms.Campaign>(newCampaign);
                Insert(CampaignModel);
                request.Id = CampaignModel.CampaignGuid;
                return request;
            }
            catch (Exception ex)
            {
                LogHelper.Error<CampaignService>("Error creating new campaign", ex);
                return request;
            }

        }

        internal IList<Campaign> GetAllActive() => GetAll().Where(x => x.IsLive).ToList();

        internal IList<Campaign> GetAllInactive() => GetAll().Where(x => !x.IsLive).ToList();

        public Campaign GetByGuid(Guid CampaignGuid) => GetAll().Where(x => x.CampaignGuid == CampaignGuid).FirstOrDefault();

        internal Campaign GetByName(string CampaignName) => GetAll().Where(x => x.Name == CampaignName).FirstOrDefault();

        internal Campaign EnableCampaign(Guid CampaignGuid)
        {
            var campaign = GetByGuid(CampaignGuid);
            campaign.UpdatedDate = DateTime.UtcNow;
            campaign.IsLive = true;
            Update(campaign);
            return campaign;
        }

        internal Campaign DisableCampaign(Guid CampaignGuid)
        {
            var campaign = GetByGuid(CampaignGuid);
            campaign.UpdatedDate = DateTime.UtcNow;
            campaign.IsLive = false;
            Update(campaign);
            return campaign;
        }

        internal void Update(Campaign campaign)
        {
            Mapper.CreateMap<Campaign, Models.Rdbms.Campaign>();
            var campaignModel = Mapper.Map<Campaign, Models.Rdbms.Campaign>(campaign);
            Update(campaignModel);
        }

        private void Insert(Models.Rdbms.Campaign campaign)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Insert(campaign);
            }
            ClearCampaignCache();
        }

        private void Update(Models.Rdbms.Campaign campaign)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Update(campaign);
            }
            ClearCampaignCache();
        }

        private void DeleteByGuid(Guid CampaignGuid)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Execute("DELETE FROM vpmCampaign WHERE CampaignGuid = @0", CampaignGuid);
            }
            ClearCampaignCache();
        }

        private void ClearCampaignCache()
        {
            var cacheKey = "CachedCampaigns";
            System.Web.HttpRuntime.Cache.Remove(cacheKey);
        }
    }
}
