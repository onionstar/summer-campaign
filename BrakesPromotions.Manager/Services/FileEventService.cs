﻿namespace BrakesPromotions.Manager.Services
{
    // Namespaces
    using Models.Rdbms;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Umbraco.Core;

    public class FileEventService
    {
        public IList<EventTrack> GetAll(Guid CampaignGuid)
        {
            var cacheKey = "CachedEvents";
            var events = (List<EventTrack>)System.Web.HttpRuntime.Cache[cacheKey];

            if (events == null || !events.Any())
            {
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    events = _db.Fetch<EventTrack>("SELECT * FROM vpmEventTrack WHERE CampaignGuid = @0", CampaignGuid);
                    System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        events,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.High,
                        null
                        );
                }
            }

            return events;
        }

        public void Insert(EventTrack campaignEvent)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Insert(campaignEvent);
            }
        }

        public void ClearCache(string cacheKey)
        {
            System.Web.HttpRuntime.Cache.Remove(cacheKey);
        }
    }
}
