﻿namespace BrakesPromotions.Manager.Services
{
    using AutoMapper;
    // Namespaces
    using Models.Rdbms;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Umbraco.Core;
    using Umbraco.Core.Persistence;

    public class GiftTierService
    {

        public GiftTierService() { }

        public IList<GiftTier> GetAll()
        {
            var cacheKey = "CachedGiftTiers";
            var giftTiers = (List<GiftTier>)System.Web.HttpRuntime.Cache[cacheKey];

            if (giftTiers == null || !giftTiers.Any())
            {
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    giftTiers = _db.Fetch<GiftTier>("SELECT * FROM vpmGiftTier ORDER BY Position");
                }
                System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        giftTiers,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.High,
                        null
                        );
            }
            return giftTiers;
        }

        public IList<GiftTier> GetByCampaignGuid(Guid CampaignGuid) => GetAll().Where(x => x.CampaignGuid == CampaignGuid).ToList();

        public GiftTier GetByGiftTierGuid(Guid GiftTierGuid) => GetAll().FirstOrDefault(x => x.GiftTierGuid == GiftTierGuid);

        internal void Update(GiftTier giftTier)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Update(giftTier);
            }
            ClearGiftTierCache();
        }

        private void ClearGiftTierCache()
        {
            var cacheKey = "CachedGiftTiers";
            System.Web.HttpRuntime.Cache.Remove(cacheKey);
        }

    }
}
