﻿namespace BrakesPromotions.Manager.Services
{
    // Namespaces
    using Models.Rdbms;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Umbraco.Core;

    public class BrandService
    {
        public BrandService() { }

        public IList<Brand> GetAll()
        {
            var cacheKey = "CachedBrands";
            var brands = (List<Brand>)System.Web.HttpRuntime.Cache[cacheKey];

            if (brands == null || !brands.Any())
            {
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    brands = _db.Fetch<Brand>("SELECT * FROM vpmBrand");
                    System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        brands,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.High,
                        null
                        );
                }
            }

            return brands;
        }

        public Brand GetByGuid(Guid BrandGuid) => GetAll().Where(x => x.BrandGuid == BrandGuid).FirstOrDefault();

        public Brand GetByName(string BrandName) => GetAll().Where(x => x.Name == BrandName).FirstOrDefault();
    }
}
