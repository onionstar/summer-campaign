﻿namespace BrakesPromotions.Manager.Services
{
    // Namespaces
    using Models.Rdbms;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Umbraco.Core;
    using Umbraco.Core.Persistence;

    public class GiftService
    {
        public GiftService() { }

        public IList<Gift> GetAll()
        {
            var cacheKey = "CachedGifts";
            var gifts = (List<Gift>)System.Web.HttpRuntime.Cache[cacheKey];

            if (gifts == null || !gifts.Any())
            {
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    gifts = _db.Fetch<Gift>("SELECT vpmGift.* FROM vpmGift INNER JOIN vpmGiftType ON vpmGift.TypeGuid = vpmGiftType.TypeGuid ORDER BY vpmGift.GiftTierGuid, vpmGiftType.Position");
                }
                System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        gifts,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.High,
                        null
                        );
            }
            return gifts;
        }

        public IList<Gift> GetByGiftTierGuid(Guid GiftTierGuid) => GetAll().Where(x => x.GiftTierGuid == GiftTierGuid).ToList();

        public Gift GetByGiftGuid(Guid GiftGuid) => GetAll().FirstOrDefault(x => x.GiftGuid == GiftGuid);

        public string FullGiftName(Guid GiftGuid, Guid GiftTypeGuid)
        {
            var cacheKey = $"CachedGiftName_{GiftGuid}";
            var giftName = (string)System.Web.HttpRuntime.Cache[cacheKey];

            if (string.IsNullOrWhiteSpace(giftName))
            {
                var Gift = GetAll().Where(x => x.GiftGuid == GiftGuid).FirstOrDefault();
                var GiftType = new GiftType();
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    GiftType = _db.Fetch<GiftType>("SELECT * FROM vpmGiftType WHERE TypeGuid = @0", GiftTypeGuid).FirstOrDefault();
                }

                bool nameIsNumber = false;
                nameIsNumber = int.TryParse(Gift.Name, out int nameAsNumber);

                var name = nameIsNumber ? nameAsNumber.ToString("N0") : Gift.Name;

                giftName = $"{GiftType.Prefix}{name} {GiftType.Suffix}".Trim();
                System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        giftName,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.High,
                        null
                        );
            }

            return giftName;
        }

        internal void Update(Gift gift)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Update(gift);
            }
            ClearGiftCache();
        }

        private void ClearGiftCache()
        {
            var cacheKey = "CachedGifts";
            System.Web.HttpRuntime.Cache.Remove(cacheKey);
        }
    }
}
