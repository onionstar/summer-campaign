﻿namespace BrakesPromotions.Manager.Services
{
    // Namespaces
    using AutoMapper;
    using Helpers;
    using Models.Rdbms;
    using Models.Requests;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using Umbraco.Core;
    using Umbraco.Core.Persistence;

    public class MemberService
    {

        private static bool TestMode => bool.Parse(ConfigurationManager.AppSettings["Visarc.Testmode"]);

        public Member GetByClaimCode(string ClaimCode, Guid CampaignGuid)
        {
            var cacheKey = $"CachedMember{ClaimCode}{CampaignGuid}";
            var member = (Member)System.Web.HttpRuntime.Cache[cacheKey];

            if (member == null)
            {
                member = GetByCampaignGuid(CampaignGuid).FirstOrDefault(x => x.ClaimCode == ClaimCode);
                if (member == null) return member;
                System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        member,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.Normal,
                        null
                        );
            }

            return member;
        }

        public IList<Member> GetByCampaignGuid(Guid CampaignGuid)
        {
            var cacheKey = $"CachedMember{CampaignGuid}";
            var members = (List<Member>)System.Web.HttpRuntime.Cache[cacheKey];

            if (members == null)
            {
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    members = _db.Fetch<Member>("SELECT * FROM vpmMember WHERE CampaignGuid = @0", CampaignGuid).ToList();
                }
                System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        members,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.Normal,
                        null
                        );
            }

            return members;
        }

        public Models.ViewModels.PagedMembers GetPagedByCampaignGuid(Accounts request)
        {
            IList<Member> members = GetByCampaignGuid(request.CampaignGuid);

            var pageIndex = request.PageNumber > 0 ? request.PageNumber - 1 : 0;
            request.PageNumber = request.PageNumber > 0 ? request.PageNumber : 1;
            request.PageSize = request.PageSize > 0 ? request.PageSize : 24;

            var skip = pageIndex > 0 ? (pageIndex * request.PageSize) : 0;

            var totalItems = members.Count;

            if (!string.IsNullOrWhiteSpace(request.Filter))
            {
                members = members.Where(x =>
                (!string.IsNullOrWhiteSpace(x.BusinessName) && x.BusinessName.ToLower().Contains(request.Filter.ToLower())) ||
                (!string.IsNullOrWhiteSpace(x.ClaimCode) && x.ClaimCode.ToLower().Contains(request.Filter.ToLower())) ||
                (!string.IsNullOrWhiteSpace(x.Email) && x.Email.ToLower().Contains(request.Filter.ToLower())) ||
                (!string.IsNullOrWhiteSpace(x.FirstName) && x.FirstName.ToLower().Contains(request.Filter.ToLower())) ||
                (!string.IsNullOrWhiteSpace(x.Mobile) && x.Mobile.ToLower().Contains(request.Filter.ToLower())) ||
                (!string.IsNullOrWhiteSpace(x.Telephone) && x.Telephone.ToLower().Contains(request.Filter.ToLower())) ||
                (!string.IsNullOrWhiteSpace(x.Town) && x.Town.ToLower().Contains(request.Filter.ToLower())) ||
                (!string.IsNullOrWhiteSpace(x.Urn) && x.Urn.ToLower().Contains(request.Filter.ToLower()))
                ).ToList();
            }

            if (request.Filters.BrandFilter != null && request.Filters.BrandFilter.BrandGuid != Guid.Empty)
            {
                members = members.Where(x => x.BrandGuid == request.Filters.BrandFilter.BrandGuid).ToList();
            }

            if (!string.IsNullOrWhiteSpace(request.Filters.StatusFilter))
            {
                switch (request.Filters.StatusFilter.ToLower())
                {
                    case "inactive":
                        members = members.Where(x => !x.UmbracoMemberGuid.HasValue || x.UmbracoMemberGuid == Guid.Empty).ToList();
                        break;
                    case "active":
                        members = members.Where(x => x.UmbracoMemberGuid.HasValue && x.UmbracoMemberGuid != Guid.Empty).ToList();
                        break;
                }
            }

            if (!string.IsNullOrWhiteSpace(request.OrderBy))
            {
                if (request.OrderDirection == Umbraco.Core.Persistence.DatabaseModelDefinitions.Direction.Descending)
                {
                    members = (from r in members
                               orderby (request.OrderBy)
                               descending
                               select r).ToList();
                }
                else
                {
                    members = (from r in members
                               orderby (request.OrderBy)
                               ascending
                               select r).ToList();
                }
            }
            else
            {
                members = members.OrderBy(x => x.ClaimCode).ToList();
            }

            if (!TestMode)
            {
                members = members.Where(x => !x.ClaimCode.Contains("ARC") && !x.Urn.Contains("VIS")).ToList();
            }

            Mapper.CreateMap<Member, Models.ViewModels.Member>();



            return new Models.ViewModels.PagedMembers(totalItems, request.PageNumber, request.PageSize)
            {
                Items = Mapper.Map<List<Member>, List<Models.ViewModels.Member>>(members.Skip(skip).Take(request.PageSize).ToList())
            };
        }

        public List<Member> GetByUmbracoMember(Guid UmbracoMemberGuid, Guid CampaignGuid)
        {
            var cacheKey = $"CachedMember{UmbracoMemberGuid}{CampaignGuid}";
            var member = (List<Member>)System.Web.HttpRuntime.Cache[cacheKey];

            if (member == null)
            {
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    member = _db.Fetch<Member>("SELECT * FROM vpmMember WHERE UmbracoMemberGuid = @0 AND CampaignGuid = @1", UmbracoMemberGuid, CampaignGuid).ToList();
                }
                System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        member,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.Normal,
                        null
                        );
            }

            return member;
        }

        public Member GetByMemberGuid(Guid MemberGuid)
        {
            var cacheKey = $"CachedMember{MemberGuid}";
            var member = (Member)System.Web.HttpRuntime.Cache[cacheKey];

            if (member == null)
            {
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    member = _db.Fetch<Member>("SELECT * FROM vpmMember WHERE MemberGuid = @0", MemberGuid).FirstOrDefault();
                }
                if (member == null) return member;
                System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        member,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.Normal,
                        null
                        );
            }

            return member;
        }

        public void Insert(Member member, bool bulkAction = false)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Insert(member);
            }
            if (!bulkAction)
            {
                var cacheKey = $"CachedMember{member.ClaimCode}{member.CampaignGuid}";
                ClearMemberCache(cacheKey);
            }
        }

        public void Update(Member Member, Guid UmbracoMemberGuid, bool BulkAction = false)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Update(Member);
            }
            if (!BulkAction)
            {
                var cacheKey = $"CachedMember{Member.ClaimCode}{Member.CampaignGuid}";
                ClearMemberCache(cacheKey);
                cacheKey = $"CachedMember{Member.MemberGuid}{Member.CampaignGuid}";
                ClearMemberCache(cacheKey);
                cacheKey = $"CachedMember{Member.CampaignGuid}";
                ClearMemberCache(cacheKey);
                cacheKey = $"CachedMember{Member.MemberGuid}";
                ClearMemberCache(cacheKey);
                if (UmbracoMemberGuid != Guid.Empty)
                {
                    cacheKey = $"CachedMember{UmbracoMemberGuid}{Member.CampaignGuid}";
                    ClearMemberCache(cacheKey);
                    cacheKey = $"CachedMember{UmbracoMemberGuid}";
                    ClearMemberCache(cacheKey);
                }
            }
        }

        public void DeleteByGuid(Guid CampaignGuid)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Execute("DELETE FROM vpmMember WHERE MemberGuid = @0", CampaignGuid);
            }
            ClearMemberCache();
        }

        public void ClearMemberCache(string cacheKey = "CachedMember")
        {
            if (cacheKey != "CachedMember")
            {
                System.Web.HttpRuntime.Cache.Remove(cacheKey);
            }
            else
            {
                foreach (System.Collections.DictionaryEntry cachedMember in System.Web.HttpRuntime.Cache)
                {
                    if (cachedMember.Key.ToString().StartsWith(cacheKey))
                    {
                        System.Web.HttpRuntime.Cache.Remove((string)cachedMember.Key);
                    }
                }
            }
        }
    }
}
