﻿namespace BrakesPromotions.Manager.Services
{
    // Namespaces
    using Models.Rdbms;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Umbraco.Core;

    public class PromotionsService
    {
        public PromotionsService() { }

        public IList<PromotionalProduct> GetAll(Guid MemberGuid)
        {
            var cacheKey = $"CachedPromotions{MemberGuid}";
            var promotions = (List<PromotionalProduct>)System.Web.HttpRuntime.Cache[cacheKey];

            if (promotions == null || !promotions.Any())
            {
                using (var _db = ApplicationContext.Current.DatabaseContext.Database)
                {
                    promotions = _db.Fetch<PromotionalProduct>("SELECT * FROM vpmPromoProduct WHERE MemberGuid = @0 ORDER BY Position", MemberGuid);
                    System.Web.HttpContext.Current.Cache.Add(cacheKey,
                        promotions,
                        null,
                        DateTime.Now.AddDays(1),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.High,
                        null
                        );
                }
            }

            return promotions;
        }

        public void Insert(PromotionalProduct promo, bool bulkAction = false)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Insert(promo);
            }
            if (!bulkAction)
            {
                var cacheKey = $"CachedPromotion{promo.MemberGuid}{promo.PromoGuid}";
                ClearCache(cacheKey);
            }
        }

        public void Update(PromotionalProduct promo, bool bulkAction = false)
        {
            using (var _db = ApplicationContext.Current.DatabaseContext.Database)
            {
                _db.Update(promo);
            }
            if (!bulkAction)
            {
                var cacheKey = $"CachedPromotion{promo.MemberGuid}{promo.PromoGuid}";
                ClearCache(cacheKey);
            }
        }

        public void ClearCache(string cacheKey = "CachedPromotion")
        {
            if (cacheKey != "CachedPromotion")
            {
                System.Web.HttpRuntime.Cache.Remove(cacheKey);
            }
            else
            {
                foreach (System.Collections.DictionaryEntry cachedMember in System.Web.HttpRuntime.Cache)
                {
                    if (cachedMember.Key.ToString().StartsWith(cacheKey))
                    {
                        System.Web.HttpRuntime.Cache.Remove((string)cachedMember.Key);
                    }
                }
            }
        }

    }
}
