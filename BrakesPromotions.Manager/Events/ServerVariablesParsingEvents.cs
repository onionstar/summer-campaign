﻿namespace BrakesPromotions.Manager.Events
{
    // Namespaces
    using Controllers;
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Umbraco.Core;
    using Umbraco.Core.Logging;
    using Umbraco.Web;
    using Umbraco.Web.UI.JavaScript;

    class ServerVariablesParsingEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            base.ApplicationStarted(umbracoApplication, applicationContext);
            LogHelper.Info<ServerVariablesParsingEvents>("Initializing PromotionsManager ServerVariablesParsingEvents");
            ServerVariablesParser.Parsing += ServerVariablesParserParsing;

        }

        private static void ServerVariablesParserParsing(object sender, Dictionary<string, object> e)
        {
            if (HttpContext.Current == null) throw new InvalidOperationException("HttpContext is null");

            if (e.ContainsKey("promotionsManagerUrls")) return;

            var promotionsManagerUrls = new Dictionary<string, object>();

            var url = new UrlHelper(new RequestContext(new HttpContextWrapper(HttpContext.Current), new RouteData()));

            promotionsManagerUrls.Add(
                "campaignApiControllerBaseUrl",
                url.GetUmbracoApiServiceBaseUrl<CampaignController>(
                    controller => controller.GetAllCampaigns()));

            e.Add("promotionsManagerUrls", promotionsManagerUrls);
        }
    }
}
