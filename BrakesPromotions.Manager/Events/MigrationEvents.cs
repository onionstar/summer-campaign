﻿namespace BrakesPromotions.Manager.Events
{
    // Namespaces
    using Semver;
    using System;
    using System.Linq;
    using Umbraco.Core;
    using Umbraco.Core.Logging;
    using Umbraco.Core.Persistence.Migrations;

    public class MigrationEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            HandleTableMigration(new SemVersion(1, 1, 0), "PromotionsManager");
        }

        private static void HandleTableMigration(SemVersion targetVersion, string productName = "")
        {
            if (string.IsNullOrWhiteSpace(productName)) return;
            var currentVersion = new SemVersion(0);

            // get all migrations for 'productName' already executed
            var migrations = ApplicationContext.Current.Services.MigrationEntryService.GetAll(productName);

            // get the latest migration for 'productName' executed
            var latestMigration = migrations.OrderByDescending(x => x.Version).FirstOrDefault();

            if (latestMigration != null)
                currentVersion = latestMigration.Version;

            if (targetVersion == currentVersion)
                return;

            var migrationsRunner = new MigrationRunner(
              ApplicationContext.Current.Services.MigrationEntryService,
              ApplicationContext.Current.ProfilingLogger.Logger,
              currentVersion,
              targetVersion,
              productName);

            try
            {
                migrationsRunner.Execute(ApplicationContext.Current.DatabaseContext.Database);
            }
            catch (Exception e)
            {
                LogHelper.Error<MigrationEvents>("Error running Promotions table migration", e);
            }
        }
    }
}
