﻿namespace BrakesPromotions.Manager.Actions
{
    // Namespaces
    using ExcelDataReader;
    using Models;
    using Services;
    using System;
    using System.Data;
    using System.Diagnostics;
    using System.Globalization;
    using System.Web;

    public class AccountUpdater
    {
        private static MemberService memberService = new MemberService();
        private static BrandService brandService = new BrandService();
        private static GiftTierService giftTierService = new GiftTierService();
        private static TargetService targetService = new TargetService();
        private static PromotionsService promotionsService = new PromotionsService();

        public AccountUpdater() { }

        public ImportResponse UpdateAccounts(HttpPostedFileBase myFile, Guid campaignGuid, string password = "", bool doPromotions = false, bool doTargets = false)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            var response = new ImportResponse
            {
                Success = false,
                AccountCount = 0,
                PromoCount = 0,
                TargetCount = 0
            };

            if (myFile == null || myFile.ContentLength == 0)
            {
                watch.Stop();
                response.Duration = watch.Elapsed;
                return response;
            }

            using (var excelReader = ExcelReaderFactory.CreateReader(myFile.InputStream, new ExcelReaderConfiguration()
            {
                Password = password
            }))
            {
                var excelDataSet = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    UseColumnDataType = true,
                    ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true,

                    }
                });
                DataTable excelDataTable = new DataTable();
                excelDataTable = excelDataSet.Tables[0];

                foreach (DataRow row in excelDataTable.Rows)
                {
                    var claimCode = row["Claim Code"].ToString();

                    var memberAccount = memberService.GetByClaimCode(claimCode, campaignGuid);
                    var targets = targetService.GetAll(memberAccount.MemberGuid);
                    var promotions = promotionsService.GetAll(memberAccount.MemberGuid);

                    // only update existing accounts
                    if (memberAccount != null)
                    {
                        memberAccount.SpendBalance = Convert.ToDecimal((double)row["Spend balance"]);
                        var isNectar = row["Nectar customer"].ToString();
                        memberAccount.NectarCustomer = Convert.ToBoolean(isNectar);
                        memberAccount.NectarBonusDeliveryCount = Convert.ToInt32((double)row["Deliveries"]);

                        var updatedTargets = false;
                        var updatedPromotions = false;

                        var targetReached = 0;

                        switch (row["Trading state"].ToString().ToLower())
                        {
                            case "not trading":
                                memberAccount.TradingState = "It’s time to heat up";
                                break;
                            case "below forecast":
                                memberAccount.TradingState = "THE temperatures ARE rising";
                                break;
                            case "above forecast":
                                memberAccount.TradingState = "Keep Sizzling";
                                break;
                        }

                        // targets

                        var t = 0;
                        var ti = 0;

                        foreach (var target in targets)
                        {
                            if (doTargets)
                            {
                                var newTarget = Convert.ToInt32((double)row[$"Target {ti + 1}"]);

                                if (newTarget != target.TargetValue)
                                {
                                    target.TargetValue = newTarget;
                                    targetService.Update(target, true);
                                    updatedTargets = true;
                                }
                                response.TargetCount++;
                                ti++;
                            }

                            if (memberAccount.SpendBalance > target.TargetValue)
                            {
                                targetReached = (t + 1);
                            }
                        }

                        // promotions
                        if (doPromotions)
                        {
                            var p = 0;

                            foreach (var promotion in promotions)
                            {
                                var newPromoCode = Convert.ToInt32(row[$"Promo product { p + 1}"].ToString());
                                var newPromoPrice = "";

                                var promoPrice = row[$"Promo product { p + 1} price"].ToString().Replace("£", "");

                                if (double.TryParse(promoPrice, out double price))
                                {
                                    newPromoPrice = price.ToString("C", new CultureInfo("en-GB"));
                                }
                                else
                                {
                                    newPromoPrice = promoPrice;
                                }

                                if (newPromoCode != promotion.BrakesProductId && newPromoPrice != promotion.PromoPrice)
                                {
                                    promotion.BrakesProductId = newPromoCode;
                                    promotion.PromoPrice = newPromoPrice;

                                    promotionsService.Update(promotion, true);

                                    updatedPromotions = true;
                                }
                                response.PromoCount++;

                                p++;
                            }

                            if (updatedPromotions)
                            {
                                promotionsService.ClearCache($"CachedPromotion{memberAccount.MemberGuid}");
                            }
                        }

                        if (row["Trading state"].ToString().ToLower() == "reached target")
                        {
                            switch (targetReached)
                            {
                                case 1:
                                    memberAccount.TradingState = "you’re smoking";
                                    break;
                                case 2:
                                    memberAccount.TradingState = "WOW, KEEP GOING!";
                                    break;
                                case 3:
                                    memberAccount.TradingState = "Almost there!";
                                    break;
                                case 4:
                                    memberAccount.TradingState = "Smashed it, you can claim any gift!";
                                    break;
                            };


                        }

                        memberService.Update(memberAccount, Guid.Empty, true);
                        response.AccountCount++;
                    }
                }

                if (response.AccountCount > 0)
                    memberService.ClearMemberCache();

                if (doTargets)
                {
                    targetService.ClearCache();
                }
            }


            watch.Stop();
            response.Duration = watch.Elapsed;

            if (response.AccountCount > 0) response.Success = true;
            return response;
        }
    }
}
