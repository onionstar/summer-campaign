﻿namespace BrakesPromotions.Manager.Actions
{
    // Namespaces
    using ExcelDataReader;
    using Models;
    using Services;
    using System;
    using System.Data;
    using System.Diagnostics;
    using System.Globalization;
    using System.Web;

    public class AccountImporter
    {
        private static MemberService memberService = new MemberService();
        private static BrandService brandService = new BrandService();
        private static GiftTierService giftTierService = new GiftTierService();
        private static TargetService targetService = new TargetService();
        private static PromotionsService promotionsService = new PromotionsService();

        public AccountImporter() { }

        public ImportResponse ImportAccounts(HttpPostedFileBase myFile, Guid campaignGuid, string password = "")
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            var response = new ImportResponse
            {
                Success = false,
                AccountCount = 0,
                PromoCount = 0,
                TargetCount = 0
            };
            if (myFile == null || myFile.ContentLength == 0)
            {
                watch.Stop();
                response.Duration = watch.Elapsed;
                return response;
            }

            using (var excelReader = ExcelReaderFactory.CreateReader(myFile.InputStream, new ExcelReaderConfiguration()
            {
                Password = password
            }))
            {
                var excelDataSet = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    UseColumnDataType = true,
                    ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true,

                    }
                });
                DataTable excelDataTable = new DataTable();
                excelDataTable = excelDataSet.Tables[0];

                foreach (DataRow row in excelDataTable.Rows)
                {
                    var claimCode = row["Claim Code"].ToString();
                    var memberAccount = memberService.GetByClaimCode(claimCode, campaignGuid);

                    // only import new accounts, duplicate / old accounts are skipped
                    if (memberAccount == null)
                    {
                        memberAccount = new Models.Rdbms.Member
                        {
                            ClaimCode = claimCode,
                            MemberGuid = Guid.NewGuid(),
                            CampaignGuid = campaignGuid,
                            Created = DateTime.UtcNow,
                            Updated = DateTime.UtcNow,
                            SpendBalance = 0,
                            BrandGuid = brandService.GetByName(row["Brand"].ToString()).BrandGuid,
                            TelesalesNumber = row["Telesales number"].ToString(),
                            Urn = row["URN"].ToString(),
                            NectarCustomer = (bool)row["Nectar customer"],
                            NectarBonusPoints = Convert.ToInt32((double)row["Delivery Bonus Points Value"]),
                            NectarBonusTarget = Convert.ToInt32((double)row["Delivery Target"]),
                            NectarBonusMinimum = (double)row["Delivery Minimum Value"],
                            BusinessName = row["Business name"].ToString(),
                            Address1 = row["Address line 1"].ToString(),
                            Address2 = row["Address line 2"].ToString(),
                            Address3 = row["Address line 3"].ToString(),
                            Town = row["Town"].ToString(),
                            County = row["County"].ToString(),
                            Postcode = row["Postcode"].ToString(),
                            Telephone = row["Telephone"].ToString(),
                            TradingState = "It’s time to heat up"
                        };

                        memberService.Insert(memberAccount, true);

                        // targets
                        var T1Position = row.Table.Columns["Target 1"].Ordinal;
                        var TLastPosition = row.Table.Columns["Promo product 1"].Ordinal - 1;

                        var giftTiers = giftTierService.GetByCampaignGuid(campaignGuid);

                        for (var i = T1Position; i <= TLastPosition; i++)
                        {
                            var giftTier = giftTiers[i - T1Position];
                            Models.Rdbms.Target target = new Models.Rdbms.Target
                            {
                                MemberGuid = memberAccount.MemberGuid,
                                TargetValue = Convert.ToInt32((double)row[$"Target {giftTier.Position + 1}"]),
                                GiftTierGuid = giftTier.GiftTierGuid,
                                TargetGuid = Guid.NewGuid()
                            };
                            targetService.Insert(target, true);
                            response.TargetCount++;
                        }


                        targetService.ClearCache($"CachedTarget{memberAccount.MemberGuid}");

                        // promotions
                        var P1Position = row.Table.Columns["Promo product 1"].Ordinal;
                        var PLastPosition = row.Table.Columns["Telesales number"].Ordinal - 1;
                        var PPosition = 0;

                        for (int i = P1Position; i < (PLastPosition); i += 2)
                        {
                            Models.Rdbms.PromotionalProduct promo = new Models.Rdbms.PromotionalProduct
                            {
                                PromoGuid = Guid.NewGuid(),
                                MemberGuid = memberAccount.MemberGuid,
                                BrakesProductId = Convert.ToInt32((double)row[$"Promo product { PPosition + 1}"]),
                                Position = PPosition
                            };

                            var promoPrice = row[$"Promo product { PPosition + 1} price"].ToString();

                            if (double.TryParse(promoPrice, out double price))
                            {
                                promo.PromoPrice = price.ToString("C", new CultureInfo("en-GB"));
                            }
                            else
                            {
                                promo.PromoPrice = promoPrice;
                            }

                            promotionsService.Insert(promo);
                            PPosition++;
                            response.PromoCount++;
                        }

                        promotionsService.ClearCache($"CachedPromotion{memberAccount.MemberGuid}");

                        response.AccountCount++;
                        memberService.ClearMemberCache($"CachedMember{memberAccount.ClaimCode}{memberAccount.CampaignGuid}");
                    }
                }
            }
            watch.Stop();
            response.Duration = watch.Elapsed;

            if (response.AccountCount > 0) response.Success = true;
            return response;
        }
    }
}
