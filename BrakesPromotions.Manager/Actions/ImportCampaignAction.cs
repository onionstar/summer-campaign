﻿namespace BrakesPromotions.Manager.Actions
{
    // Namespaces
    using umbraco.interfaces;

    public class ImportCampaignAction : IAction
    {
        private static readonly ImportCampaignAction LocalInstance = new ImportCampaignAction();

        private ImportCampaignAction()
        {
        }

        public static ImportCampaignAction Instance => LocalInstance;

        public string Alias => "importcampaign";

        public bool CanBePermissionAssigned => false;

        public string Icon => "add";

        public string JsFunctionName => string.Empty;

        public string JsSource => string.Empty;

        public char Letter => '1';

        public bool ShowInNotifier => true;
    }
}
