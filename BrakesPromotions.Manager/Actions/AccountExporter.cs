﻿namespace BrakesPromotions.Manager.Actions
{
    // Namespaces
    using Models.ViewModels;
    using OfficeOpenXml;
    using Services;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Umbraco.Core.IO;

    public class AccountExporter
    {
        private static MemberService memberService = new MemberService();
        private static CampaignService campaignService = new CampaignService();

        public AccountExporter() { }

        public string GetExcelSheet(Guid campaignGuid, string password)
        {
            var campaign = campaignService.GetByGuid(campaignGuid);
            var excelData = GetExportData(campaignGuid);

            var uploadPath = "~\\app_data\\TEMP\\FileUploads";
            var path = Path.Combine(uploadPath, $"{campaign.Name.Replace(" ", "")}.xlsx");

            using (var package = new ExcelPackage())
            {
                var workbook = package.Workbook;
                var worksheet = workbook.Worksheets.Add($"{campaign.Name} Data");

                worksheet.Cells["A1"].LoadFromCollection(excelData, true, OfficeOpenXml.Table.TableStyles.Medium1);
                worksheet.Column(22).Style.Numberformat.Format = "dd/mm/yyyy hh:mm";
                worksheet.Column(23).Style.Numberformat.Format = "dd/mm/yyyy hh:mm";

                if (!string.IsNullOrWhiteSpace(password))
                {
                    package.SaveAs(new FileInfo(IOHelper.MapPath(path)), password);
                }
                else
                {
                    package.SaveAs(new FileInfo(IOHelper.MapPath(path)));
                }
                return path;
            }
        }

        private IList<ExportMember> GetExportData(Guid CampaignGuid)
        {
            var members = memberService.GetByCampaignGuid(CampaignGuid);

            List<ExportMember> exportData = new List<ExportMember>();

            foreach (var member in members)
            {
                exportData.Add(new ExportMember(member));
            }


            return exportData;
        }
    }
}
