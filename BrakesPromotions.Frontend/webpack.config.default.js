const config = require("config");
const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WebpackPostcleanupPlugin = require("./tasks/webpack.postclean.js");
const AssetsPlugin = require("assets-webpack-plugin");

const buildPath = path.resolve(__dirname, config.get("cms") ? config.get("buildPath") : "build");
const srcPath = path.resolve(__dirname, "src");

module.exports = {
  context: srcPath,

  entry: {
    // JS
    "scripts/site": "./scripts/site",
    "scripts/vendor": "./scripts/vendor",
    
    // CSS
    "css/master": "./styles/master"
  },

  output: {
    filename: "[name].[chunkhash].js",
    path: buildPath,
    publicPath: "/",
  },

  resolve: {
    extensions: [
      ".js", ".json", ".jsx", ".scss", ".css", ".njk", ".nunjucks", ".html"
    ],
    modules: ["node_modules", srcPath + "/scripts", srcPath],
    alias: {
      "debug.addIndicators": "scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js",
    }
  },

  plugins: [
    new ExtractTextPlugin("[name].[chunkhash].css"),
    new WebpackPostcleanupPlugin({ paths: "css/*.js" }),
    new webpack.HashedModuleIdsPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      chunks: ["scripts/site", "scripts/vendor"],
      name: "scripts/vendor"
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: "scripts/manifest"
    }),
    new AssetsPlugin({path: path.join(buildPath, "config")})
  ]
  .concat(config.get("cms") ? [] : [
    new HtmlWebpackPlugin({
      template: "./templates/index.njk",
      inject: false
    })
  ]),

  module: {
    rules: [

      {
        test: /\.(js|jsx)$/,
        include: srcPath,
        exclude: /(node_modules|bower_components)/,
        use: [
          "cache-loader",
          {
            loader: "babel-loader",
            options: {
              babelrc: false,
              presets: [
                ["es2015", {"modules": false}],
                "stage-2"
              ],
              plugins: [
                "transform-runtime",
                "transform-class-properties",
                "transform-object-rest-spread"
              ],
              cacheDirectory: true
            }
          }
        ]
      },

      {
        test: /\.(scss|css)$/,
        loader: ExtractTextPlugin.extract({
          use: [
            "css-loader",
            "postcss-loader",
            {
              loader: "sass-loader",
              options: {
                includePaths: [
                  "node_modules"
                ]
              }
            }
          ],
          fallback: "style-loader",
          publicPath: "/"
        })
      },

      {
        test: /\.(njk|nunjucks)$/,
        loader: "nunjucks-isomorphic-loader",
        query: {
          root: [path.resolve(srcPath, "templates")]
        }
      }

    ]
  }

}
