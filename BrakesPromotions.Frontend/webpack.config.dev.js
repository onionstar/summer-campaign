const config = require('config');
const path = require('path');

const buildPath = path.resolve(__dirname, config.get('buildPath'));

module.exports = function(defaults) {
  // Dev server
  defaults.devServer = {
    contentBase: config.get('cms') ? false : buildPath,
    publicPath: '/',
    historyApiFallback: true,
    host: '0.0.0.0'
  };

  // Proxy dev server to CMS URL
  if ( config.get('cms') ) {
    defaults.devServer.proxy = {
      '/': config.get('url').includes('http') ? config.get('url') : `http://${config.get('url')}`,
    }
  }

  return defaults;
}
