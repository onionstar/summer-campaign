export function stop(el) {
  el.pause();
  el.currentTime = 0;
}
