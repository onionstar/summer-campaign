export const cookies = () => (
  Object.assign(
    {},
    ...document.cookie
      .split(';')
      .map(x => x.trim().split('='))
      .map(x => ({ [x[0]] : x[1] }))
  )
)

export const get = (name, def) => {
  return cookies()[name] || def;
}

export const set = (name, value, days) => {
  let c = `${name}=${value};`;
  if ( days ) {
    let d = new Date();
    d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000));
    c += `expires=${d.toUTCString()};`;
  }
  c += 'path=/;'
  document.cookie = c;
}

export const clear = (name) => {
  document.cookie = `${name}=;expires=${new Date(0)};path=/;`;
}