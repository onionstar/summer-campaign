export function getIcon(name, classname, size) {
  return `<svg xmlns="http://www.w3.org/2000/svg" class="${classname}" width="${size}" height="${size}"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#${name}"></use></svg>`
}
