export function ensureStartsWith(str, start) {
  return !str || str[0] === start ? str : start + str;
}

export function ensureStartsWithout(str, start) {
  return !!str && str[0] === start ? str.slice(1) : str;
}

export function ensureEndsWith(str, end) {
  return str[str.length-1] === end ? str : str + end;
}
