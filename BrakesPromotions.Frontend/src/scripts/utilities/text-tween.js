import anime from 'animejs'

import {$} from './selector';

const o = '<span style="white-space: nowrap">'; // Open Outer
const c = '</span>'; // Close

export function splitText(el) {
  if ( el ) {
    el.innerHTML = o + el.textContent.split(' ').map(x => (x.replace(/(.)/g, "<span>$1</span>"))).join(`${c} ${o}`) + c;
  }
}

export const textTween = (tween, split = true) => {
  let targets = tween.targets;

  if ( ! targets ) {
    throw new Error("Textweens require an element");
  }

  if ( split )
    splitText(targets);

  targets = Array.isArray(targets) ? targets : $('span span', targets);

  return anime({
    ...tween,
    targets: targets,
    delay: (el, i) => i * (tween.delay || 100),
  });
}

export default textTween
