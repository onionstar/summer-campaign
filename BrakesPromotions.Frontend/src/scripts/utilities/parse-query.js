export function parseQuery(str) {
  return str.substr(1).split('&')
    .filter(x => !!x)
    .map(x => x.split('='))
    .reduce((prev, curr) => Object.assign(prev, { [curr[0].toLowerCase()] : curr[1] }), {})
}

export function stringifyQuery(obj) {
  return Object
    .keys(obj)
    .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`)
    .join('&')
}
