export function diff(a, b) {
  return Math.abs(a - b);
}
export function absDiff(a, b) {
  return diff(Math.abs(a), Math.abs(b));
}
