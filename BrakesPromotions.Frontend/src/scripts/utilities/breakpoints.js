import {breakpoints} from '../settings';

export function parseBreakpoints(str) {
  let min = 0;
  let max = Infinity;

  if ( str ) {
    let points = str.split(/\s/);

    if ( points.length > 0 ) {
      if ( ! (min = breakpoints[points[0]]) ) {
        throw error(points[0])
      }
    }

    if ( points.length > 1 ) {
      if ( ! ( max = breakpoints[points[1]] ) ) {
        throw error(points[1])
      }
    }
  }

  return { min, max };
}

export function conditionsMet(bp) {
  let w = window.innerWidth;
  return w >= bp.min && w <= bp.max;
}

function error(p) {
  return new Error("Breakpoint: '" + p + "' is not defined");
}
