/**
 * Create a custom event
 * @param {string} type The event to create
 * @param {*} detail The data to send with the event
 */
export function event(type, params = {}) {
  params = params.detail || params.bubbles || params.cancellable
    ? params
    : {detail: params};
  return new CustomEvent(type, params);
}
