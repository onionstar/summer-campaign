export function getOffset(el) {
  let offset = el.offsetTop;
  while ( el != document.body ) {
    el = el.parentNode;
    if ( getComputedStyle(el).position == "static" ) continue;
    offset += el.offsetTop;
  }
  return offset;
}
