function allowScroll(e) {
  e.stopPropagation();
}

function prevent(e) {
  e.preventDefault();
}

let scrollPosition = 0;

export function toggleBodyScrolling(enabled) {
  if ( enabled === false ) {
    scrollPosition = document.body.scrollTop
    document.documentElement.classList.add("has-modal");
  } else {
    document.documentElement.classList.remove("has-modal");
    document.body.scrollTop = scrollPosition;
  }
}
