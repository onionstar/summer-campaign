export function isIe() {
  if ( '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style )  {
    document.documentElement.classList.add('ie-11')
  }

  if ( 'behavior' in document.documentElement.style && '-ms-user-select' in document.documentElement.style ) {
    document.documentElement.classList.add('ie-10')
  }
}
