export const getWallopClasses = (base) => ({
  baseClass: base,
  buttonPreviousClass: `${base}-buttonPrevious`,
  buttonNextClass: `${base}-buttonNext`,
  itemClass: `${base}-item`,
  currentItemClass: `is-active`,
  showPreviousClass: `${base}-item--showPrevious`,
  showNextClass: `${base}-item--showNext`,
  hidePreviousClass: `${base}-item--hidePrevious`,
  hideNextClass: `${base}-item--hideNext`
})
