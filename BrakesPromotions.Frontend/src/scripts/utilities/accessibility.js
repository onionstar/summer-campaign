export const focusableElements = "input,select,button,textarea,[tabindex]:not([tabindex=\"-1\"]),[contenteditable]";

export function getFocusableEl(parent) {
  return parent.querySelector(focusableElements) 
    || parent.querySelector("[tabindex=-1]");
}