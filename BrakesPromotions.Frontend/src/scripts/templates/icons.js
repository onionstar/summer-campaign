export const iconPause = "M 15,15 L 15,85 L 30,85 L 30,50 L 70,50 L 70,85 L 85,85 L 85,15 L 70,15 L 70,50 L 30,50 L 30,15 Z";
export const iconPlay = "M 15,15 L 15,85 L 30,77.5 L 42.5,70.875 L 58,63 L 70,57.5 L 85,50 L 85,50 L 70.25,42.5 L 58,36.5 L 42,28.5 L 30,22.5 Z";
