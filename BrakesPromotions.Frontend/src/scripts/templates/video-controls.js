import {iconPlay} from './icons'

export const controls = (model) => (
`<div class="Controls">
  <button type="button" class="Controls-button" data-js="controls-play">
    <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" class="Controls-icon" role="presentation" style="vertical-align: text-bottom;">
      <path d="${iconPlay}" fill="currentColor" stroke="none" data-js="controls-playIcon"></path>
    </svg>
  </button>
  <div class="Controls-track" data-js="controls-track">
    <div class="Controls-progress" data-js="controls-progress"></div>
  </div>
  <button type="button" class="Controls-button" data-js="controls-mute">
    <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" class="Controls-icon" style="vertical-align: text-bottom;">
      <path d="m4.053 10-0.106 4 4.053 2 4 4v-16l-4 4z"/>
      <path d="m18.1 5.159-0.832 0.832c1.751 1.434 2.867 3.613 2.867 6.059 0 2.39-1.067 4.524-2.75 5.96l0.8308 0.8308c1.897-1.652 3.099-4.082 3.099-6.79 0-2.763-1.251-5.236-3.216-6.889z" data-js="mute-icon"/>
      <path d="m15.96 7.291-0.7433 0.7433c1.24 0.9011 2.048 2.359 2.048 4.016 0 1.6-0.7544 3.015-1.924 3.922l0.7427 0.7427c1.358-1.103 2.229-2.784 2.229-4.665 0-1.937-0.9252-3.658-2.352-4.758z" data-js="mute-icon"/>
      <path d="m13.8 9.45-0.8037 0.8037c0.7214 0.2772 1.229 0.9717 1.229 1.795 0 0.7646-0.4388 1.418-1.079 1.729l0.7878 0.7878c0.8236-0.5374 1.371-1.466 1.371-2.517 0-1.108-0.6081-2.078-1.506-2.599z" data-js="mute-icon"/>
    </svg>
  </button>
</div>`
)
