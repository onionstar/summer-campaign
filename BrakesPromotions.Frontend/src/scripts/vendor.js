// Polyfills
import 'core-js/es6/promise'
import 'core-js/fn/promise'
import 'core-js/fn/string/includes'
import 'core-js/fn/object/assign'
import 'core-js/fn/object/keys'
import 'core-js/fn/array/from'
import 'core-js/fn/array/find'
import 'core-js/fn/array/find-index'
import 'core-js/fn/array/includes'
import 'core-js/fn/array/for-each'
import 'whatwg-fetch'
import 'objectFitPolyfill/src/objectFitPolyfill'

// Vendor
import 'animejs'
import 'hammerjs'
import 'validate.js'

import 'lodash/throttle'
