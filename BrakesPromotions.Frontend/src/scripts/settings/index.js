import {getWallopClasses} from '../utilities/settings-utils'

export const breakpoints = {
  tablet: 660,
  desktop: 1076,
  desktopHd: 1680
}

export const carouselDefaults = {
  auto: true,
  dots: true,
  hasVideo: false,
  delay: 10000,
  touch: true,
  wallop: getWallopClasses("Carousel")
}

export default {
  breakpoints,
  carouselDefaults
}
