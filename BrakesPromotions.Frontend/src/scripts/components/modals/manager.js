import {$} from '../../utilities/selector'
import {get, clear} from '../../utilities/cookie'

import LoadManager, {QUEUE} from '../load-manager'

import {Modal} from './modal'

class ModalManager {
  constructor() {
    this.onOpen = this.onOpen.bind(this);
    this.onEscape = this.onEscape.bind(this);

    this.modals = [...$("[data-js=modal]")]
      .map(x => new Modal(x, this))
      .reduce((accum, curr) => {
        accum[curr.id] = curr;
        return accum;
      }, {});

    this.state = {
      open: [],
      active: null
    };
    
    this.triggers = [...$("[data-js=modal-trigger]")];
    this.triggers.forEach(this.bindTrigger, this);

    this.openPrevious();
  }

  bindTrigger(el) {
    if ( ! el.hasAttribute("data-target") ) {
      throw new Error(`Modal trigger ${el} does not have a target`);
    }

    let target = el.getAttribute("data-target");
    if ( !this.modals[target] ) {
      throw new Error(`Modal ${target} was not found in the modal list`);
    }

    el.addEventListener("click", this.onOpen);
  }

  open(id) {
    let modal = this.modals[id];
    this.state.open.push(modal)
    this.state.active = modal;
    modal.open();
  }

  close(id) {
    let modal = this.modals[id];
    this.state.open = this.state.open.filter(x => x.id !== id);
    if ( this.state.active === modal ) {
      this.state.active = this.state.open[this.state.open.length] || null;
    }
    if ( ! this.state.active ) {
      document.body.classList.remove("has-modal");
      window.removeEventListener("click", this.escape);
      window.removeEventListener("keyup", this.escape);
    }
  }

  onOpen(e) {
    e.preventDefault();
    if ( ! this.state.active ) {
      document.body.classList.add("has-modal");
      window.addEventListener("click", this.escape);
      window.addEventListener("keyup", this.escape);
    }
    let id = e.currentTarget.getAttribute("data-target");
    this.open(id);
  }

  onEscape(e) {
    if ( 
      ! this.state.active 
      || e.key && e.key !== "Escape" 
      || e.target.hasAttribute("data-js") && e.target.getAttribute("data-js").includes("modal-trigger")
      || e.target === this.state.active.el
      || e.target.compareDocumentPosition(this.state.active.el) & Node.DOCUMENT_POSITION_CONTAINS
    )
      return;
    
    this.state.active.close();
  }

  openPrevious() {
    let activeModal = get("activeModal");
    
    // hacky hack - Checks for any form validation errors within modals before showing default "activeModal"
    let formError = $(".validation-summary-errors");

    if (activeModal && this.modals[activeModal]) {
      if(formError.length) {
        let error = $(".validation-summary-errors")[0];
        let modalError = error.closest(".Modal");
        let modalErrorID = modalError.getAttribute("id");
        let modalId = modalErrorID.replace('modal-', '');
        this.open(modalId);
      } else {
        this.open(activeModal);
      }
      clear("activeModal", null);
    }
  }
}

export default LoadManager.queue(() => new ModalManager(), QUEUE.DOM);