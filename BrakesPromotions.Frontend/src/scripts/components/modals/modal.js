import {getFocusableEl} from '../../utilities/accessibility'

export class Modal {
  constructor(el, manager) {
    this.close = this.close.bind(this);
    this.el = el;
    this.manager = manager;
    this.initialFocus = getFocusableEl(el);
    this.id = el.id.replace("modal-", "");
    this.closeButtons = [...el.querySelectorAll("[data-js=modal-close]")];
    this.closeButtons.forEach(x => {
      x.addEventListener("click", this.close);
    })
  }

  open() {
    this.el.classList.add("is-open");
    this.initialFocus.focus();
  }

  close() {
    this.el.classList.remove("is-open");
    this.manager.close(this.id);
  }
}