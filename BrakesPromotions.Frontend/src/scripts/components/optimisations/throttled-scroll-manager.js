import OptimisedEventManager from './optimised-event-manager';

import throttle from '../../utilities/throttle'

export const DIRECTION = {
  FORWARD: 'FORWARD',
  BACKWARD: 'BACKWARD',
  PAUSED: 'PAUSED',
};

class ScrollManager extends OptimisedEventManager {
  lastScrollPos = 0

  constructor() {
    super({
      func: throttle,
      event: "scroll",
      newEvent: "optimizedScroll"
    });
  }

  params() {
    const pos = window.scrollY;
    const params = {
      size: pos,
      direction: this.direction(pos),
      progress: this.progress()
    };
    this.lastScrollPos = pos;
    return params;
  }

  direction(newPos) {
    if ( this.lastScrollPos < newPos ) {
      return DIRECTION.FORWARD;
    }

    if ( this.lastScrollPos > newPos ) {
      return DIRECTION.BACKWARD;
    }

    return DIRECTION.PAUSED;
  }

  // Get
  progress() {
    return (
      window.scrollY / ( document.body.scrollHeight - window.innerHeight )
    ).toFixed(4)
  }
}

export default new ScrollManager();
