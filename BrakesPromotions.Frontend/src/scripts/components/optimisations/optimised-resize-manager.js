import OptimisedEventManager from './optimised-event-manager';

import throttle from '../../utilities/throttle'
import debounce from '../../utilities/debounce'

export const DebouncedResizeManager = new OptimisedEventManager({
  func: debounce,
  event: "resize",
  newEvent: "debouncedResize",
  rate: 300
});

export const ThrottledResizeManager = new OptimisedEventManager({
  func: throttle,
  event: "resize",
  newEvent: "throttledResize",
});
