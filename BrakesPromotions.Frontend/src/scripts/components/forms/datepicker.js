import Flatpickr from 'flatpickr'

import {$, $1} from '../../utilities/selector'

import LoadManager, {QUEUE} from '../load-manager'

const pickers = (el) => {

  const baseConfig = {
    minDate: new Date(),
    altInput: true,
    // defaultDate: new Date(),
    onClose: function(selectedDates, dateStr, instance) {
      instance.altInput.blur()
    }
  };

  const dateConfig = Object.assign({}, baseConfig)

  const dateTimeConfig = Object.assign({}, baseConfig, {
    enableTime: true
  })

  return {
    datePickers: [...$('[data-js=datepicker]')].map(el => new Flatpickr(el, dateConfig)),
    dateTimePickers: [...$('[data-js=datetimepicker]')].map(el => new Flatpicker(el, dateTimeConfig))
  }
}

export default LoadManager.queue(pickers, QUEUE.DOM)
