import {$, $1} from '../../utilities/selector'

import ComponentManager from '../component-manager'
import LoadManager, { QUEUE } from '../load-manager'

export class LinkedSelect {
  constructor(el, {condition}) {
    if ( ! el )
      throw new Error("Instance of LinkedSelect must have a valid element")

    if ( ! el.getAttribute('data-select-target') )
      throw new Error("LinkedSelect main element must have a data-select-target")

    if ( typeof condition !== "function" )
      throw new Error("Instance of LinkedSelect must have a valid function as the condition")

    this.el = el
    this.condition = condition
    this.target = $1(`[data-select-id=${el.getAttribute('data-select-target')}]`)

    if ( ! this.target )
      throw new Error("Instance of LinkedSelect could not find target")

    this.change()
    this.el.addEventListener('change', this.change.bind(this, true))
  }

  change(resetVal) {
    if ( resetVal ) {
      this.target.value = this.target.children[0].value;
    }

    [...this.target.children]
      .filter(el => !!el.value || el.children.length)
      .forEach(el => {
        if ( ! this.condition(this.el.value, el) ) {
          el.setAttribute('hidden', '')
        } else {
          el.removeAttribute('hidden')
        }
      });
  }
}

// Examples
//
// export const ParentSelect = LoadManager.queue(() => (
//   new ComponentManager(
//     '[data-js=parent-select]',
//     LinkedSelect,
//     {
//       condition: (value, el) => (
//         !value || el.getAttribute('data-parent') == value
//       )
//     }
//   )
// ), QUEUE.DOM);
//
// export const MinPriceSelect = LoadManager.queue(() => (
//   new ComponentManager(
//     '[data-js=minprice-select]',
//     LinkedSelect,
//     {
//       condition: (value, el) => (
//         parseInt(el.value) >= ( parseInt(value) || 0 )
//       )
//     }
//   )
// ), QUEUE.DOM);
