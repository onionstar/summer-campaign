import validate from "validate.js";

import {$, closest} from "../../utilities/selector";

import ComponentManager from "../component-manager";
import LoadManager, { QUEUE } from "../load-manager";

const ObserverOptions = {
  attributes: true,
  attributeFilter: ["style"]
};

class FormValidator {
  constructor(el) {
    this.el = el;

    // Get all fields to validate
    this.controls = [...$("[data-val]", el)];
    this.actions = [...$("[type=submit]", el)];
    this.fields = this.controls.reduce(FormValidator.getFields, {});

    // Get conditional fields - these will affect the constraints
    this.conditionals = [...$("[data-condition]", el)];

    // Bindings
    // this.conditionChange = this.conditionChange.bind(this);
    this.validate = this.validate.bind(this);

    // Defaults
    this.liveCheck = 0;

    // Setup
    this.updateConstraints();

    // Observer to watch for constraint changes
    this.observer = new MutationObserver(() => {
      this.updateConstraints();

      if ( this.liveCheck )
        this.validate();
    });

    this.conditionals.forEach(el => {
      this.observer.observe(el, ObserverOptions)
    })

    // Bind events
    this.bind();
  }

  updateConstraints() {
    this.constraints = this.controls.reduce(FormValidator.getConstraint, {});
  }

  bind() {
    this.el.addEventListener("submit", this.validate, true);
  }

  enableLiveCheck() {
    this.liveCheck = 1;
    this.controls.forEach(el => {
      el.addEventListener("blur", this.validate);
      el.addEventListener("change", this.validate);
    });
  }

  validate(e) {
    if ( e && e.target.hasAttribute("data-novalidate") )
      return;

    if ( ! this.liveCheck )
      this.enableLiveCheck();

    this.results = validate(this.el, this.constraints) || false;

    this.reset();

    if ( this.results ) {
      if ( e && e.type == "submit" && e.preventDefault ) {
        e.preventDefault();
        e.stopPropagation();
      }

      this.errors();

    } else {
      // if ( this.liveCheck ) {
      //   this.actions.forEach(FormValidator.enableAction);
      // }
      // Success handlers here, e.g. analytics
      //dataLayer.push({"event": this.el.getAttribute("data-name")});
    }
  }

  reset() {
    Object.keys(this.fields)
      .map(x => this.fields[x])
      .filter(x => x.hasError && !this.results[x.name])
      .forEach(FormValidator.clear);
  }

  errors() {
    //this.actions.forEach(FormValidator.disableAction);

    Object.keys(this.results)
      .map(x => ({error: this.results[x], field: this.fields[x]}))
      .forEach(FormValidator.error);
  }

  static error({error, field}) {
    field.error.innerHTML = error;
    if ( ! field.hasError ) {
      field.hasError = true;
      if ( field.container.childElementCount > 1 ) {
        // Not sure I entirely trust this method
        field.container.insertBefore(field.error, field.container.children[1]);
      } else {
        field.container.appendChild(field.error);
      }      
    } else {
      field.error.classList.remove("is-hidden");
    }
    field.inputs.forEach(x => x.classList.add("is-invalid"));
  }

  static clear(field) {
    // field.hasError = false;
    // field.container.removeChild(field.error);
    field.error.classList.add("is-hidden");
    field.inputs.forEach(x => x.classList.remove("is-invalid"));
  }

  static getFields(fields, field) {
    const name = field.name || field.getAttribute("name");
    const key = FormValidator.getKey(name);

    if ( typeof fields[field] == "undefined" ) {
      let container = FormValidator.getContainer(field);
      if ( ! container ) return fields;

      fields[key] = {
        name,
        container,
        inputs: [],
        hasError: false,
        error: FormValidator.createError(""),
      };
    }

    fields[key].inputs.push(field);

    return fields;
  }

  static getConstraint(constraints, field) {
    const name = field.name || field.getAttribute("name");
    const key = FormValidator.getKey(name);

    constraints[key] = {};

    // Don"t try to validate disabled fields
    if ( field.hasAttribute("disabled") )
      return constraints;

    // Required Validation
    if ( FormValidator.isRequired(field) ) {
      let msg = FormValidator.getMessage(field);

      constraints[key].presence = {
        allowEmpty: false
      }

      if ( msg ) {
        constraints[key].presence.message = `^${msg}`
      }

      // Checkbox
      if ( field.type === "checkbox" && ! field.attributes.value ) {
        constraints[key].inclusion = {
          within: [true],
          message: constraints[key].presence.message
        };
      }
    }

    // Regex Validation
    if ( field.hasAttribute("data-val-regex") ) {
      constraints[key].format = {
        pattern: field.getAttribute("data-regex"),
        message: "^" + field.getAttribute("data-val-regex")
      };
    }

    // Matches Validation
    if ( field.hasAttribute("data-match") ) {
      constraints[key].equality = {
        attribute: field.getAttribute("data-match"),
        comparator: FormValidator.equalityComparator
      };
      if ( field.hasAttribute("data-match-error") ) {
        constraints[key].equality.message =
          "^" + field.getAttribute("data-match-error")
      }
    }

    // Length Restrictions
    if ( field.hasAttribute("data-length") ) {
      let length = field.getAttribute("data-length");
      if ( length.includes(":") ) {
        let [min, max] = length.split(":").map(i => parseInt(i));
        constraints[key].length = {};
        if ( ! isNaN(min) ) {
          constraints[key].length.minimum = min;
          constraints[key].length.tooShort = field.getAttribute("data-tooshort-error") 
            || "must be at least %{count} characters";
        }
        if ( ! isNaN(max) ) {
          constraints[key].length.maximum = max;
          constraints[key].length.tooLong = field.getAttribute("data-toolong-error") 
            || "must be fewer than %{count} characters";
        }
      } else {
        let is = parseInt(length);
        if ( ! isNaN(is) ) {
          constraints[key].length = {
            is
          };
        }
      }
    }

    return constraints;
  }

  static isRequired(field) {
    return field.hasAttribute("data-val-required") ||
      field.hasAttribute("data-val-requiredlist");
  }

  static getMessage(field) {
    return field.getAttribute("data-val-required") ||
      field.getAttribute("data-val-requiredlist");
  }

  static createError(message) {
    let el = document.createElement("p");
    el.className = "Form-note Form-note--error";
    el.innerHTML = message;
    return el;
  }

  static getContainer(el) {
    return closest(".Form-field", el);
  }

  static enableAction(el) {
    el.disabled = false;
  }

  static disableAction(el) {
    el.disabled = true;
  }

  static equalityComparator(a, b) {
    return a.toLowerCase() === b.toLowerCase();
  }

  static getKey(name) {
    return name.replace(/\./, "\\\\\\\\\\.");
  }
}

export default LoadManager.queue(() => (
  new ComponentManager(FormValidator, "[data-js~=validator]")
), QUEUE.DOM);
