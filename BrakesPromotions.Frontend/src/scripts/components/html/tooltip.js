import LoadManager, {QUEUE} from '../load-manager'
import ComponentManager from '../component-manager'

export class Tooltip {
  static create(target) {
    let el = document.createElement("p");
    el.className = "Tooltip";
    el.innerText = target.getAttribute("data-content");
    document.body.appendChild(el);
    return el;
  }

  constructor(el) {
    if ( ! el.hasAttribute("data-content") ) {
      console.error(`Tooltip ${el} is missing data-content`);
      return;
    }
    this.hide = this.hide.bind(this);
    this.enter = this.enter.bind(this);
    this.leave = this.leave.bind(this);
    this.touch = this.touch.bind(this);
    this.release = this.release.bind(this);
    this.track = this.track.bind(this);
    this.updatePosition = this.updatePosition.bind(this);
    this.trigger = el;
    this.tapCount = 0;
    this.el = Tooltip.create(el);
    this.trigger.addEventListener("mouseenter", this.enter);
    this.trigger.addEventListener("mouseleave", this.leave);
    this.trigger.addEventListener("touchstart", this.touch);
    this.trigger.addEventListener("touchend", this.release);
  }

  show() {
    clearTimeout(this.hideTimer);
    this.el.classList.add("is-visible");
  }

  hide() {
    this.tapCount = 0;
    this.isTouchEvent = false;
    this.el.classList.remove("is-visible");
    this.isTracking = false;
  }

  enter(e) {
    if ( ! this.isTouchEvent ) {
      this.show();
      this.track(e);
      window.addEventListener("mousemove", this.track);
    }
  }

  leave() {
    if ( ! this.isTouchEvent ) {
      clearTimeout(this.hideTimer);
      this.hideTimer = setTimeout(this.hide, 250);
      window.removeEventListener("mousemove", this.track);
    }
  }

  touch(e) {
    this.isTouchEvent = true;
    if ( ! this.el.classList.contains("is-visible") ) {
      this.show();
      this.track(e.touches[0]);
    }
  }

  release(e) {
    if ( this.tapCount == 0 ) {
      e.preventDefault();
      this.tapCount++;
    }
    clearTimeout(this.hideTimer);
    this.hideTimer = setTimeout(this.hide, 1500);
  }

  track({clientX, clientY}) {
    this.cursorPosition = {
      clientX,
      clientY
    };
    window.requestAnimationFrame(this.updatePosition);
  }

  updatePosition() {
    let width = this.el.offsetWidth/2;
    let left = this.cursorPosition.clientX;
    let right = left + width;

    // Ensure tooltip doesn't go off screen
    if ( right > window.innerWidth ) {
      left -= right - window.innerWidth;
    }
    if ( left - width < 0 ) {
      left += Math.abs(left - width);
    }

    let top = this.cursorPosition.clientY + document.scrollingElement.scrollTop;
    
    // Adjust position to avoid being hidden by the cursor
    if ( this.isTouchEvent ) {
      top -= 75;
    } else {
      top += 35;
    }
    
    this.el.style.left = `${left}px`;
    this.el.style.top = `${top}px`;
  }
}

export default LoadManager.queue(() => {
  new ComponentManager(Tooltip, "[data-js=tooltip]");
}, QUEUE.DOM);