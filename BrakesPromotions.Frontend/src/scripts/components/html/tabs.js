import { $1, $ } from '../../utilities/selector'

import LoadManager, { QUEUE } from '../load-manager'
import ComponentManager from '../component-manager'

class Tabs {
  constructor(el) {
    this.el = el;
    this.buttons = [...$("[data-js=tabs-button]")];
    this.tabs = Object.assign({}, ...this.buttons.map(el => {
      let target = el.getAttribute("data-target");
      return {
        [target]: $1(`[data-js=tabs-content][data-id=${target}]`)
      };
    }));
    
    this.open = this.open.bind(this);

    this.buttons.forEach(el => {
      el.addEventListener('click', this.open);
    });
  }

  open(e) {
    // Deactivate the old buttons
    this.buttons.forEach(x => x.classList.remove("is-active"));

    // Find our target
    let target = e.currentTarget.getAttribute("data-target");

    // Set our new active button
    e.currentTarget.classList.add("is-active");
    
    // Enable our target, while disabling others
    Object.keys(this.tabs).forEach(x => {
      this.tabs[x].classList[x == target ? "add" : "remove"]("is-active");
    }, this)
  }
}

export default LoadManager.queue(() => {
  new ComponentManager(Tabs, "[data-js=tabs]")
}, QUEUE.DOM)