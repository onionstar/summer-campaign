import {$1, $} from '../../utilities/selector'
import {constrain} from '../../utilities/constrain'

import LoadManager, {QUEUE} from '../load-manager'
import ComponentManager from '../component-manager'
import {DebouncedResizeManager} from '../optimisations/optimised-resize-manager';

export class ScrollBar {
  constructor(el) {
    this.el = el;
    this.elements = {
      handle: $1("[data-js=scroll-handle]", el),
      inner: $1("[data-js=scroll-inner]", el),
      body: $1("[data-js=scroll-body]", el),
      track: $1("[data-js=scroll-track]", el),
    };

    this.startScroll = this.startScroll.bind(this);
    this.scroll = this.scroll.bind(this);
    this.startTracking = this.startTracking.bind(this);
    this.stopTracking = this.stopTracking.bind(this);
    this.track = this.track.bind(this);
    this.trackLoop = this.trackLoop.bind(this);
    this.jumpTo = this.jumpTo.bind(this);
    
    this.clearHandleTransition = this.clearHandleTransition.bind(this);

    this.isTracking = false;
    this.handlePosition = 0;
    this.progress = 0;

    this.jump = {
      transitionDelay: null,
      isJumping: false,
    };
    
    this.elements.handle.style.willChange = "transform";
    
    LoadManager.queue(() => {
      DebouncedResizeManager.add({
        action: this.recalculate.bind(this)
      });
      this.bind();
    }, QUEUE.RESOURCES);
  }

  recalculate() {
    this.wrapHeight = this.el.offsetHeight;
    this.scrollMax = this.elements.body.offsetHeight - this.wrapHeight;
    let handleStyles = getComputedStyle(this.elements.handle);
    this.handleHeight = this.elements.handle.offsetHeight + parseInt(handleStyles.marginTop) + parseInt(handleStyles.marginBottom);
    this.handleScrollMax = this.wrapHeight - this.handleHeight;
  }

  bind() {
    this.elements.inner.addEventListener("scroll", this.startScroll, {
      passive: true,
    });
    this.elements.handle.addEventListener("mousedown", this.startTracking);
    window.addEventListener("mouseup", this.stopTracking);
    this.elements.track.addEventListener("click", this.jumpTo);
  }
  
  startScroll() {
    window.requestAnimationFrame(this.scroll);
  }

  scroll() {
    if ( ! this.isTracking && ! this.jump.isJumping ) {
      this.progress = this.elements.inner.scrollTop / this.scrollMax;
      this.handlePosition = this.progress * this.handleScrollMax;
      this.updateHandle();
    }
  }

  startTracking(e) {
    this.clearHandleTransition();
    this.isTracking = true;
    this.handleLastPosition = e.clientY;
    window.addEventListener("mousemove", this.track, true);
  }

  stopTracking() {
    if ( this.isTracking ) {
      window.removeEventListener("mousemove", this.track, true);
      this.isTracking = false;
      this.startTrackingPosition = 0;
    }
  }

  track(e) {
    this._pointerPosition = e.clientY;
    window.requestAnimationFrame(this.trackLoop);
  }
  
  trackLoop() {
    let distanceMoved = this._pointerPosition - this.handleLastPosition;
    this.updateFromTrack(this.handlePosition + distanceMoved);
    this.handleLastPosition = this._pointerPosition;
  }

  jumpTo(e) {
    if ( e.target == this.elements.track ) {
      let clickPosition = e.clientY + this.elements.inner.getBoundingClientRect().y;
      setTimeout(this.setHandleTransition.bind(this, clickPosition), 1); 
    }
  }
  
  setHandleTransition(position) {
    this.elements.handle.style.transition = ".2s transform ease";
    this.updateFromTrack(position);
    this.jump.isJumping = true;
    this.jump.transitionDelay = setTimeout(this.clearHandleTransition, 200);
  }

  clearHandleTransition() {
    if ( this.jump.isJumping ) {
      clearTimeout(this.jump.transitionDelay);
      this.elements.handle.style.transition = "";
      this.jump.isJumping = false;
    }
  }
  
  updateFromTrack(position) {
    this.handlePosition = constrain(position, 0, this.handleScrollMax);
    this.progress = this.handlePosition / this.handleScrollMax;
    this.updateHandle();
    this.elements.inner.scrollTop = this.progress * this.scrollMax;
  }
  
  updateHandle() {
    this.elements.handle.style.transform = `translate3D(0, ${this.handlePosition}px, 0)`
  }
}

export default LoadManager.queue(() => {
  new ComponentManager(ScrollBar, "[data-js=scroll]");
}, QUEUE.DOM)