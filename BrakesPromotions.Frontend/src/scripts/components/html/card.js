import {$} from '../../utilities/selector';

import LoadManager, {QUEUE} from "../load-manager";
import ComponentManager from "../component-manager";

class Card {
  constructor(el) {
    this.el = el;
    this.flip = this.flip.bind(this);
    this.triggers = [...$("[data-js=card-trigger]", el)];
    this.triggers.forEach(this.bindTrigger, this);
  }

  bindTrigger(el) {
    el.addEventListener("click", this.flip);
  }

  flip() {
    this.el.classList.toggle("is-flipped");
  }
}

export default LoadManager.queue(x => {
  new ComponentManager(Card, "[data-js=card]")
}, QUEUE.RESOURCES)