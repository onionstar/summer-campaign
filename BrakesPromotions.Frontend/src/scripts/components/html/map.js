import settings from '../../settings';
import style from '../../settings/map-style.json';

import { $1 } from '../../utilities/selector';

import LoadManager, { QUEUE } from '../load-manager'

const defaults = {
  lat: 0,
  lng: 0,
  z: 2
};

const API_KEY = "AIzaSyBWifpa3SpUMvubquDIwYA82JXjoZr2L4E";

export class Map {
  constructor(el) {
    this.el = el || $1('[data-js=map]');
    this.done = false;
    this.fixedZoom = false;

    if (!this.el) return false;

    this.init = this.init.bind(this);
    this.boundsChanged = this.boundsChanged.bind(this);

    this.center = {
      lat: parseFloat(this.el.getAttribute('data-lat')) || defaults.lat,
      lng: parseFloat(this.el.getAttribute('data-lng')) || defaults.lng
    };
    this.zoom = parseInt(this.el.getAttribute('data-zoom')) || defaults.z;
    this.pin = this.el.getAttribute('data-pin');

    this.markers = [...this.el.children].map(this.getMarker, this);

    LoadManager.queue(() => {
      if (typeof google === "undefined") {
        this.loadApi();
      } else {
        this.init();
      }
    }, QUEUE.RESOURCES);
  }

  // Ensures we always have the API before we initialise the map
  loadApi() {
    if (window.hasGMapApi) return;

    let script = document.createElement('script');

    script.onload = this.init;
    script.src = `https://maps.googleapis.com/maps/api/js?key=${API_KEY}`;

    document.body.appendChild(script);

    window.hasGMapApi = true;
  }

  getMarker(el) {
    let icon = el.getAttribute('data-icon');
    let title = el.getAttribute('data-title');
    let address = el.getAttribute('data-address');

    return Object.assign(
      {
        position: {
          lat: parseFloat(el.getAttribute('data-lat')),
          lng: parseFloat(el.getAttribute('data-lng')),
        }
      },
      icon ? { icon } : { icon: this.pin },
      title ? { title } : {},
      address ? { address } : {}
    );
  }

  init() {
    if (!this.el || this.done) return false;
    let desktop = (window.innerWidth >= settings.breakpoints.desktop);
    this.bounds = new google.maps.LatLngBounds();

    this.map = new google.maps.Map(this.el, {
      center: this.center,
      zoom: this.zoom,
      zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
      },
      streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
      },
      mapTypeControl: false,
      scrollwheel: false,
      styles: style,
      //styles: [],
      disableDefaultUI: !desktop,
      disableDoubleClickZoom: !desktop,
      fullscreenControl: desktop,
      fullscreenControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
      },
      draggable: desktop
    });

    this.map.addListener('bounds_changed', this.boundsChanged)

    //this.markers.forEach(this.addMarker, this);

    //this.map.fitBounds(this.bounds);

    if (this.markers.length) {
      this.markers.forEach(this.addMarker, this);

      this.map.fitBounds(this.bounds);
    }

    this.done = true
  }

  addMarker(item) {
    if (!this.map) return false;

    item.map = this.map;

    item.instance = new google.maps.Marker(item);

    this.bounds.extend(item.position);

    item.instance.addListener('click', this.centerMarker.bind(this, item.instance));
  }

  centerMarker(marker) {
    this.map.setCenter(marker.getPosition());
  }

  boundsChanged() {
    if (!this.fixedZoom && this.map.getZoom() > this.zoom) {
      this.map.setZoom(this.zoom);
      this.fixedZoom = true;
    }
  }
}

export default LoadManager.queue(() => new Map(), QUEUE.DOM);
