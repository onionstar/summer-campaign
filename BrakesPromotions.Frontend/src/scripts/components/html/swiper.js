import Flickity from 'flickity'

import {$1, $} from '../../utilities/selector'

import LoadManager, {QUEUE} from '../load-manager'
import ComponentManager from '../component-manager'
import {DebouncedResizeManager} from '../optimisations/optimised-resize-manager'

export class Swiper {
  constructor(el) {
    this.el = el;
    this.list = $1('[data-js=swiper-list]', el);
    this.arrows = {
      prev: $1('[data-js=swiper-prev]', el),
      next: $1('[data-js=swiper-next]', el)
    };
    this.dots = [...$('[data-js=swiper-dot]', el)];

    this.onSelect = this.onSelect.bind(this);
    this.resize = this.resize.bind(this);
    
    if ( ! el || ! this.list ) return false;
    
    LoadManager.queue(this.init.bind(this), QUEUE.RESOURCES);
  }
 
  init() {
    this.flickity = new Flickity(this.list, {
      contain: true,
      selectedAttraction: 0.02,
      friction: 0.25,
      prevNextButtons: false,
      pageDots: false,
      percentPosition: false,
      cellAlign: 'left',
      resize: false
    });
    
    this.flickity.element.classList.add("is-enabled");

    if ( this.list.hasAttribute("data-flickity-classname") ) {
      this.flickity.slider.classList.add(this.list.getAttribute("data-flickity-classname"));
    }

    this.viewport = this.list.children[0];

    DebouncedResizeManager.add({
      action: this.resize
    });

    this.onSelect();
    this.flickity.on("select", this.onSelect);

    this.arrows.prev.addEventListener('click', this.flickity.previous.bind(this.flickity));
    this.arrows.next.addEventListener('click', this.flickity.next.bind(this.flickity));
    this.dots.forEach((el, i) => {
      el.addEventListener('click', this.flickity.select.bind(this.flickity, i, false, false));
    })
  }

  onSelect() {
    if ( this.flickity.selectedIndex == 0 ) {
      this.toggleArrow("prev", false);
    } else if ( this.arrows.prev.disabled ) {
      this.toggleArrow("prev", true);
    }
    if ( 
      this.flickity.selectedIndex == this.flickity.cells.length - 1
      || this.flickity.selectedCell.target >= this.flickity.slidesWidth
    ) {
      this.toggleArrow("next", false);
    } else if ( this.arrows.next.disabled ) {
      this.toggleArrow("next", true);
    }
  }

  toggleArrow(arrow, enabled) {
    this.arrows[arrow].disabled = !enabled;
    this.arrows[arrow].classList.toggle('is-disabled', !enabled);
  }

  resize() {
    this.flickity.resize()
    this.viewport.style.height = Math.ceil(parseInt(this.viewport.style.height) + 1) + "px";
  }
}

export default LoadManager.queue(() => {
  new ComponentManager(Swiper, '[data-js=swiper]')
}, QUEUE.DOM)