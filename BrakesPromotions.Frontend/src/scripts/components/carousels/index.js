import Carousel from './carousel'
import {
  ArrowsPlugin,
  AutoPlugin,
  DotsPlugin,
  HeightPlugin,
  ResizerPlugin,
  ResponsivePlugin,
  TouchPlugin,
  TransitionPlugin,
  VideoPlugin,
  VisibilityPlugin
} from './plugins'

import ComponentManager from '../component-manager'

export const AutoCarousels = new ComponentManager(
  Carousel,
  '[data-js~=carousel-auto]',
  () => ({
    plugins: [
      new VisibilityPlugin(),
      new AutoPlugin({
        delay: 4000
      }),
    ]
  })
);

export const CaseStudyCarousels = new ComponentManager(
  Carousel,
  '[data-js~=carousel-casestudy]',
  () => ({
    plugins: [
      VisibilityPlugin(),
      AutoPlugin({
        delay: 6000
      }),
      ArrowsPlugin(),
      DotsPlugin(),
      TouchPlugin(),
      TransitionPlugin({
        transition: function (el) {
          //const tl = new TimelineLite();
        }
      })
    ]
  })
);
