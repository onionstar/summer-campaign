import Wallop from 'wallop'

import CarouselPlugin from './plugins/base-plugin'

import {$1, $} from '../../utilities/selector'
import {event} from '../../utilities/event'

import LoadManager, {QUEUE} from '../load-manager'

export default class Carousel {
  constructor(el, options = {}) {
    // Ensure element exists
    this.el = el;
    if ( ! this.el ) return;

    // Set defaults
    this.initialised = false;
    this.elements = [];
    this.listeners = {};

    // Setup options
    this.options = Object.assign({}, Carousel.defaults, options);

    // Create wallop instance
    if ( ! this.wallop )
      this.wallop = new Wallop(this.el, this.options.wallop);

    // Bind `this` on methods
    this.bindMethods();

    // Enable plugins
    if ( this.options.plugins.length ) {
      this.plugins = this.options.plugins.filter(x => x instanceof CarouselPlugin ).map(plugin => plugin.enable(this));
    }

    // Get relevant elements
    this.getElements();

    // Resize plugin needs to hijack init - emit preinit to handle init conditions
    let initMethod = typeof this.listeners.preinit !== "undefined" ? this.emit.bind("preinit") : this.init;

    // Initialise on resource load
    LoadManager.queue(initMethod, QUEUE.RESOURCES);
  }

  static get defaults() {
    return Object.freeze({
      elements: [
        {
          key: 'list',
          multiple: false,
          selector: '[data-js~=carousel-list]'
        }
      ],
      plugins: [],
      wallop: Carousel.configureClassnames("Carousel")
    });
  };

  init() {
    // Set default previous item for animations
    this.wallop.previousItemIndex = this.wallop.currentItemIndex;

    // Initialise Plugins
    this.plugins.forEach(plugin => plugin.init());

    // Ready for extensions
    this.el.dispatchEvent(event("ready", {
      carousel: this
    }));

    // Add event listeners
    this.bindEvents();

    // Ready
    this.initialised = true;
  }

  destroy() {
    this.plugins.forEach(plugins => plugins.destroy());
    this.wallop.reset();
    this.listeners = {};
    this.getElements();
    this.initialised = false;
  }

  bindMethods() {
    // Self Methods
    this.init = this.init.bind(this);
    this.setPreviousItemState = this.setPreviousItemState.bind(this);

    // Wallop Methods
    this.next = this.wallop.next.bind(this.wallop);
    this.prev = this.wallop.previous.bind(this.wallop);
    this.goTo = this.wallop.goTo.bind(this.wallop);
  }

  bindEvents() {
    // Update previous state on change
    this.wallop.on('change', this.setPreviousItemState);
  }

  pushElement(...el) {
    // Make sure we haven't already populated the elements array
    // and that all of the elements we've passed are valid
    if ( this.elements.length || el.every(x => typeof x.key !== "string" || typeof x.selector !== "string") ) return false;
    this.options.elements.push(...el);
  }

  getElements() {
    // Get any relevant elements
    this.elements = this.options.elements.reduce((elements, el) => ({
      ...elements,
      [el.key]: el.multiple
        ? [...$(el.selector, this.getElementScope(el))]
        : $1(el.selector, this.getElementScope(el))
    }), {})
  }

  getElementScope({scope}) {
    return typeof scope === "function" ? scope(this.el) || this.el : this.el;
  }

  setPreviousItemState(e) {
    // Update previous item index with the current item
    this.wallop.previousItemIndex = e.detail.currentItemIndex;
  }

  hasPlugin(name) {
    return this.plugins.some(x => x.constructor.displayName === name);
  }

  getPlugin(name) {
    return this.plugins.find(x => x.constructor.displayName === name);
  }

  emit(evt, ...args) {
    if ( this.listeners[evt] && this.listeners[evt].length ) {
      this.listeners[evt].forEach(fn => fn(...args))
    }
  }

  on(evt, callback) {
    if ( typeof this.listeners[evt] === "undefined" ) {
      this.listeners[evt] = [];
    }

    this.listeners[evt].push(callback);
  }

  static configureClassnames(base) {
    return {
      baseClass: base,
      buttonPreviousClass: `${base}-buttonPrevious`,
      buttonNextClass: `${base}-buttonNext`,
      itemClass: `${base}-item`,
      currentItemClass: `${base}-item--current`,
      showPreviousClass: `${base}-item--showPrevious`,
      showNextClass: `${base}-item--showNext`,
      hidePreviousClass: `${base}-item--hidePrevious`,
      hideNextClass: `${base}-item--hideNext`
    }
  }
}
