import CarouselPlugin from './auto-plugin'

import {ThrottledResizeManager, DebouncedResizeManager} from '../../optimisations/optimised-resize-manager'

// Class
export default function ResizerCarouselPlugin(config) {
  if ( ! (this instanceof ResizerCarouselPlugin) )
    return new ResizerCarouselPlugin(config);
  
  this.throttled = this.emit.bind(this, "resizing");
  this.debounced = this.emit.bind(this, "resized");
}

// Extends
ResizerCarouselPlugin.prototype = Object.create(CarouselPlugin.prototype);
ResizerCarouselPlugin.prototype.constructor = ResizerCarouselPlugin;

// Static
ResizerCarouselPlugin.displayName = "ResizerCarouselPlugin";

// Methods
ResizerCarouselPlugin.prototype.init = function () {
  this.actions = [
    ThrottledResizeManager.add({
      action: this.throttled
    }),
    DebouncedResizeManager.add({
      action: this.debounced
    })
  ];
};

ResizerCarouselPlugin.prototype.destroy = function () {
  ThrottledResizeManager.remove(this.actions[0]);
  DebouncedResizeManager.remove(this.actions[1]);
}

ResizerCarouselPlugin.prototype.emit = function (event, ...args) {
  this.carousel.emit(event, ...args);
};
