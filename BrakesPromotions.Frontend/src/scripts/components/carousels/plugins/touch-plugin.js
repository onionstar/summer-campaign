import Hammer from 'hammerjs'
import CarouselPlugin from './base-plugin'

// Class
export default function TouchCarouselPlugin(config) {
  if ( ! (this instanceof TouchCarouselPlugin) )
    return new TouchCarouselPlugin(config);

  this.options = Object.assign({}, TouchCarouselPlugin.defaults, config);
  
  this.swipe = this.swipe.bind(this);
}

// Extends
TouchCarouselPlugin.prototype = Object.create(CarouselPlugin.prototype);
TouchCarouselPlugin.prototype.constructor = TouchCarouselPlugin;

// Static
TouchCarouselPlugin.displayName = "TouchCarouselPlugin";

TouchCarouselPlugin.defaults = {
  elements: [
    {
      key: 'touch',
      multiple: false,
      selector: '[data-js~=carousel-touch]'
    }
  ],
  hammer: {
    recognizers: [
      [Hammer.Swipe, { direction: Hammer.DIRECTION_HORIZONTAL }]
    ]
  }
};

// Methods
TouchCarouselPlugin.prototype.init = function () {
  let els = this.carousel.elements;
  this.el = els.touch || els.list || this.carousel.el;

  if ( this.el ) {
    this.hammer = new Hammer.Manager(this.el, this.options.hammer);
  }

  if (els.dots.length) {
    // Change cursor if there are multiple carousel items
    this.el.style.cursor = "ew-resize";
  }
  
  this.hammer.on("swipe", this.swipe);
  this.autoPlugin = this.carousel.getPlugin("AutoCarouselPlugin") || null;
};

TouchCarouselPlugin.prototype.destroy = function () {
  this.hammer.destroy();
  this.hammer = null;
};

TouchCarouselPlugin.prototype.swipe = function (e) {
  if ( e.direction === Hammer.DIRECTION_LEFT ) {
    this.carousel.next();
  } else {
    this.carousel.prev();
  }
  if ( this.autoPlugin ) {
    this.autoPlugin.stop();
  }
};
