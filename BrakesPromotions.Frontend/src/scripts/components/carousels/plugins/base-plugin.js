export default function CarouselPlugin() {
  if ( ! (this instanceof CarouselPlugin) )
    return new CarouselPlugin();
}

CarouselPlugin.prototype.enable = function (carousel) {
  // Store carousel reference
  this.carousel = carousel;
  
  // Push any elements this plugin uses
  if ( this.options && this.options.elements && this.options.elements.length )
    carousel.pushElement(...this.options.elements);
  
  return this;
};

CarouselPlugin.displayName = "CarouselPlugin";

CarouselPlugin.prototype.init = function () {};

CarouselPlugin.prototype.destroy = function () {};
