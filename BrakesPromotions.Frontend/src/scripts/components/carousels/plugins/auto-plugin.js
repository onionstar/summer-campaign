import CarouselPlugin from './base-plugin'

export default function AutoCarouselPlugin(config) {
  if ( ! (this instanceof AutoCarouselPlugin) )
    return new AutoCarouselPlugin(config);

  // Setup defaults
  this.options = Object.assign({}, AutoCarouselPlugin.defaults, config);
  this.isPlaying = false;
  this.isHover = false;
  this.start = this.start.bind(this);
  this.stop = this.stop.bind(this);

  this.mouseenter = this.mouseenter.bind(this);
  this.mouseleave = this.mouseleave.bind(this);
}

// Extends
AutoCarouselPlugin.prototype = Object.create(CarouselPlugin.prototype);
AutoCarouselPlugin.prototype.constructor = AutoCarouselPlugin;

// Static
AutoCarouselPlugin.displayName = "AutoCarouselPlugin";

AutoCarouselPlugin.defaults = {
  delay: 7000
};

// Methods
AutoCarouselPlugin.prototype.init = function () {
  this.carousel.el.addEventListener("mouseenter", this.mouseenter);
  this.carousel.el.addEventListener("mouseleave", this.mouseleave);
  
  if ( this.carousel.hasPlugin("VisibilityCarouselPlugin") ) {
    this.carousel.on("enter", this.start);
    this.carousel.on("leave", this.stop);
  } else {
    this.start();
  }
};

AutoCarouselPlugin.prototype.destroy = function() {
  this.carousel.el.removeEventListener("mouseenter", this.mouseenter);
  this.carousel.el.removeEventListener("mouseenter", this.mouseleave);
};

AutoCarouselPlugin.prototype.start = function () {
  if ( this.carousel.wallop.allItemsArray.length > 1 && ! this.isPlaying ) {
    this.isPlaying = true;
    this.interval = setInterval(this.carousel.next, this.options.delay)
  }
};

AutoCarouselPlugin.prototype.stop = function () {
  if ( this.isPlaying ) {
    this.isPlaying = false;
    clearInterval(this.interval)
  }
};

AutoCarouselPlugin.prototype.mouseenter = function (e) {
  this.isHover = true;
  this.stop();
};

AutoCarouselPlugin.prototype.mouseleave = function (e) {
  this.isHover = false;
  this.start()
};
