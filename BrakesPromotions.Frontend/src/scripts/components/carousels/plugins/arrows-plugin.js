import {$1} from '../../../utilities/selector'

import CarouselPlugin from './base-plugin'

// Class
export default function ArrowCarouselPlugin(config) {
  if ( ! (this instanceof ArrowCarouselPlugin) )
    return new ArrowCarouselPlugin(config);
  
  this.options = Object.assign({}, ArrowCarouselPlugin.defaults, config);
}

// Extends
ArrowCarouselPlugin.prototype = Object.create(CarouselPlugin.prototype);
ArrowCarouselPlugin.prototype.constructor = ArrowCarouselPlugin;

// Static
ArrowCarouselPlugin.displayName = "ArrowCarouselPlugin";

ArrowCarouselPlugin.defaults = {
  elements: [
    {
      key: 'next',
      multiple: false,
      selector: '[data-js~=carousel-next]',
      //scope: () => $1('[data-js~=carousel-controls]')
    },
    {
      key: 'previous',
      multiple: false,
      selector: '[data-js~=carousel-prev]',
      //scope: () => $1('[data-js~=carousel-controls]')
    }
  ]
};

// Methods
ArrowCarouselPlugin.prototype.init = function () {
  // Make sure we have the buttons then bind click action
  if ( this.carousel.elements.next ) {
    this.carousel.elements.next.addEventListener('click', this.carousel.next);
  }
  
  if ( this.carousel.elements.previous ) {
    this.carousel.elements.previous.addEventListener('click', this.carousel.prev);
  }
};

ArrowCarouselPlugin.prototype.destroy = function () {
  this.carousel.elements.next.removeEventListener('click');
  this.carousel.elements.previous.removeEventListener('click');
};