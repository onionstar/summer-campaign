import CarouselPlugin from './auto-plugin'

// Class
export default function TransitionCarouselPlugin(config) {
  if ( ! (this instanceof TransitionCarouselPlugin) )
    return new TransitionCarouselPlugin(config);

  // Setup defaults
  this.options = Object.assign({}, TransitionCarouselPlugin.defaults, config);
}

// Extends
TransitionCarouselPlugin.prototype = Object.create(CarouselPlugin.prototype);
TransitionCarouselPlugin.prototype.constructor = TransitionCarouselPlugin;

// Static Methods
TransitionCarouselPlugin.displayName = "TransitionCarouselPlugin";

TransitionCarouselPlugin.defaults = {
  transition: function (el) {},
  methods: {
    enter: 'play',
    leave: 'playBackwards'
  }
};

// Methods
TransitionCarouselPlugin.prototype.init = function () {
  this.timelines = this.carousel.wallop.allItemsArray.map(this.options.transition.bind(this));
  
  if ( this.timelines.length ) {
    this.enter(this.carousel.wallop.currentItemIndex);
  }
  
  this.carousel.wallop.on('change', this.change.bind(this))
};

TransitionCarouselPlugin.prototype.change = function (e) {
  if ( ! isNaN( this.carousel.wallop.previousItemIndex ) ) {
    this.leave(this.carousel.wallop.previousItemIndex);
  }
  this.enter(e.detail.currentItemIndex);
};

TransitionCarouselPlugin.prototype.run = function (key, i) {
  let tl = this.timelines[i];
  let m = this.options.methods[key];
  if ( tl && m && typeof tl[m] === "function" )
    tl[m]();
};

TransitionCarouselPlugin.prototype.enter = function (i) {
  this.run("enter", i)
};

TransitionCarouselPlugin.prototype.leave = function (i) {
  this.run("leave", i)
};
