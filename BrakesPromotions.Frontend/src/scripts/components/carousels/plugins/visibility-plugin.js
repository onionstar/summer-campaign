import CarouselPlugin from './auto-plugin'

import {isInView} from '../../../utilities/in-view'

import ScrollManager from '../../optimisations/throttled-scroll-manager'

// Constants
export const VISIBILITY_EVENTS = {
  ENTER: "enter",
  LEAVE: "leave"
};

// Class
export default function VisibilityCarouselPlugin(config) {
  if ( ! (this instanceof VisibilityCarouselPlugin) )
    return new VisibilityCarouselPlugin(config);
  
  // Setup defaults
  this.isVisible = false;
  this.check = this.check.bind(this);
}

// Extends
VisibilityCarouselPlugin.prototype = Object.create(CarouselPlugin.prototype);
VisibilityCarouselPlugin.prototype.constructor = VisibilityCarouselPlugin;

// Static
VisibilityCarouselPlugin.displayName = "VisibilityCarouselPlugin";

// Methods
VisibilityCarouselPlugin.prototype.init = function () {
  this.scrollEvent = ScrollManager.add({
    action: this.check
  });
};

VisibilityCarouselPlugin.prototype.destroy = function () {
  ScrollManager.remove(this.scrollEvent);
};

VisibilityCarouselPlugin.prototype.check = function () {
  let visible = isInView(this.carousel.el);
  if ( visible !== this.isVisible ) {
    this.isVisible = visible;
    this.carousel.emit(visible ? VISIBILITY_EVENTS.ENTER : VISIBILITY_EVENTS.LEAVE);
  }
};
