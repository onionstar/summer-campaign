import CarouselPlugin from './base-plugin'

export default function HeightCarouselPlugin(config) {
  if ( ! (this instanceof HeightCarouselPlugin) )
    return new HeightCarouselPlugin(config);

  // Setup defaults
  this.options = Object.assign({}, HeightCarouselPlugin.defaults, config);

  this.setHeight = this.setHeight.bind(this);
}

// Extends
HeightCarouselPlugin.prototype = Object.create(CarouselPlugin.prototype);
HeightCarouselPlugin.prototype.constructor = HeightCarouselPlugin;

// Static
HeightCarouselPlugin.displayName = "HeightCarouselPlugin";

HeightCarouselPlugin.defaults = {
  className: ""
};

HeightCarouselPlugin.prototype.init = function () {
  this.el = this.carousel.elements.list || this.carousel.el;

  if ( this.options.className )
    this.el.classList.add(this.options.className);

  this.carousel.wallop.on("change", this.setHeight);

  if ( this.carousel.hasPlugin("ResizerCarouselPlugin") ) {
    this.carousel.on("resizing", this.setHeight)
  }
};

HeightCarouselPlugin.prototype.destroy = function () {
  this.el.classList.remove(this.options.className);
  this.el.style.height = "auto";
};

HeightCarouselPlugin.prototype.setHeight = function (e) {
  let index = e.detail
    ? e.detail.currentItemIndex
    : this.carousel.wallop.currentItemIndex;

  let el = this.carousel.wallop.allItemsArray[index];
  if ( el ) {
    this.el.style.height = el.offsetHeight + 'px';
  }
};
