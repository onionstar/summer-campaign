import {$1} from 'utilities/selector'

import CarouselPlugin from './base-plugin'

// Class
export default function DotsCarouselPlugin(config) {
  if ( ! (this instanceof DotsCarouselPlugin) )
    return new DotsCarouselPlugin(config);

  this.options = Object.assign({}, DotsCarouselPlugin.defaults, config);

  // Bind goto
  this.jump = this.jump.bind(this);
}

// Extends
DotsCarouselPlugin.prototype = Object.create(CarouselPlugin.prototype);
DotsCarouselPlugin.prototype.constructor = DotsCarouselPlugin;

// Static
DotsCarouselPlugin.displayName = "DotsCarouselPlugin";
DotsCarouselPlugin.defaults = {
  elements: [
    {
      key: 'dots',
      multiple: true,
      selector: '[data-js~=carousel-dot]',
      //scope: () => $1('[data-js~=carousel-dots]')
    }
  ]
};

// Methods
DotsCarouselPlugin.prototype.init = function () {
  // Make sure we have dots
  if ( this.carousel.elements.dots && this.carousel.elements.dots.length ) {
    // cache dots
    this.dots = this.carousel.elements.dots;

    // setup dots
    this.dots.forEach(this.setupDot, this);

    // Update on change
    this.carousel.wallop.on('change', this.change.bind(this));
  }
};

DotsCarouselPlugin.prototype.setupDot = function (dot, i) {
  // Set current to active
  i !== this.carousel.wallop.currentItemIndex || dot.classList.add('is-active');

  // Add index data attr to item
  dot.setAttribute('data-index', i);

  // Setup click event
  dot.addEventListener('click', this.jump);
};

DotsCarouselPlugin.prototype.change = function (e) {
  let oldI = parseInt(this.carousel.wallop.previousItemIndex);
  let newI = parseInt(e.detail.currentItemIndex);
  isNaN(oldI) || this.dots[oldI].classList.remove('is-active');
  isNaN(newI) || this.dots[newI].classList.add('is-active');
};

DotsCarouselPlugin.prototype.jump = function (e) {
  let i = parseInt(e.currentTarget.getAttribute('data-index'));
  this.carousel.goTo(i);
};
