import CarouselPlugin from './auto-plugin'

// Class
export default function ResponsiveCarouselPlugin(config) {
  if ( ! (this instanceof ResponsiveCarouselPlugin) )
    return new ResponsiveCarouselPlugin(config);
  
  this.options = Object.assign({}, ResponsiveCarouselPlugin.defaults, config);
  this.cache = null;
}

// Extends
ResponsiveCarouselPlugin.prototype = Object.create(CarouselPlugin.prototype);
ResponsiveCarouselPlugin.prototype.constructor = ResponsiveCarouselPlugin;

// Static
ResponsiveCarouselPlugin.displayName = "ResponsiveCarouselPlugin";

ResponsiveCarouselPlugin.defaults = {
  condition: function (e, v) {
    return true;
  }
};

// Methods
ResponsiveCarouselPlugin.prototype.enable = function(carousel) {
  Object.getPrototypeOf(this).enable(carousel);

  this.cache = carousel.el.cloneNode(true);
  
  carousel.on("preinit", this.preinit.bind(this));
  
  return this;
};

ResponsiveCarouselPlugin.prototype.preinit = function () {
  if ( ! this.carousel.hasPlugin("ResizerCarouselPlugin") ) {
    throw new Error("Responsive plugin cannot work without the resizer plugin");
  }

  if ( typeof this.options.conditions === "function" )
    this.carousel.on("resized", this.resize.bind(this))
};

ResponsiveCarouselPlugin.prototype.destroy = function() {
  //this.carousel.el.parentNode.replaceChild(this.cache, this.carousel.el);
  this.carousel.el.innerHTML = this.cache.innerHTML;
};

ResponsiveCarouselPlugin.prototype.resize = function (e, v) {
  // If condition is met and we are yet to init
  let check = this.options.conditions(e, v);
  if ( check && ! this.carousel.initialised ) {
    this.carousel.init();
  } else if ( ! check && this.carousel.initialised ) {
    this.carousel.destroy();
  }
};
