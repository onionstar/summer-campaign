import CarouselPlugin from './auto-plugin'

// Class
export default function VideoCarouselPlugin(config) {
  if ( ! (this instanceof VideoCarouselPlugin) )
    return new VideoCarouselPlugin(config);
  
  this.options = Object.assign({}, VideoCarouselPlugin.defaults, config);
  this.videos = [];
  this.auto = false;
  this.change = this.change.bind(this);
  this.onPlay = this.onPlay.bind(this);
  this.onPause = this.onPause.bind(this);
}

// Extends
VideoCarouselPlugin.prototype = Object.create(CarouselPlugin.prototype);
VideoCarouselPlugin.prototype.constructor = VideoCarouselPlugin;

// Static
VideoCarouselPlugin.displayName = "VideoCarouselPlugin";

VideoCarouselPlugin.defaults = {
  elements: [
    {
      key: 'videos',
      multiple: true,
      selector: '[data-js~=carousel-video]',
    }
  ],
  condition: null
};

VideoCarouselPlugin.stop = function (video) {
  video.pause();
  video.currentTime = 0;
};

// Methods
VideoCarouselPlugin.prototype.init = function () {
  if ( ! this.carousel.elements.videos.length ) return;

  this.carousel.wallop.on("change", this.change);
  
  // If auto is on, we want to stop it when the video starts
  if ( this.carousel.hasPlugin("AutoCarouselPlugin") ) {
    this.auto = this.carousel.getPlugin("AutoCarouselPlugin");
    this.videos.forEach(this.handleAuto.bind(this));
  }
};


VideoCarouselPlugin.prototype.destroy = function () {
  this.auto = null;
  video.removeEventListener('play', this.onPlay);
  video.removeEventListener('pause', this.onPause);
};

VideoCarouselPlugin.prototype.handleAuto = function (video) {
  video.addEventListener('play', this.onPlay);
  video.addEventListener('pause', this.onPause);
};

VideoCarouselPlugin.prototype.onPlay = function (e) {
  this.auto.stop();
};

VideoCarouselPlugin.prototype.onPause = function (e) {
  !e.target.ended || this.carousel.next();
  this.auto.start()
};

VideoCarouselPlugin.prototype.change = function (e) {
  if ( typeof this.options.condition === "function" && ! this.options.condition() ) return;
  let wallop = this.carousel.wallop;
  
  // Stop previous video if exists and reset
  if ( ! isNaN(wallop.previousItemIndex) ) {
    let old = items[wallop.previousItemIndex].querySelector("video");
    if ( old ) {
      this.currentSlideIsVideo = false;
      this.stop(old);
    }
  }

  // Play new video if exists
  let video =  wallop.allItemsArray[e.detail.currentItemIndex].querySelector("video");
  if ( video ) {
    this.currentSlideIsVideo = true;
    video.play();
  }
};
