import {$, $1} from '../../utilities/selector'

import LoadManager, {QUEUE} from '../load-manager'

export function hoverControls() {
  const els = [...$("[data-js=video-hover-controls]")]
  els.forEach(hoverControls.bind);
}

hoverControls.bind = function(el) {
  el.controls = false;

  el.parentNode.addEventListener("mouseenter", hoverControls.show);
  el.parentNode.addEventListener("mouseleave", hoverControls.hide);
}

hoverControls.show = function(e) {
  e.currentTarget.children[0].controls = true;
  console.log(e.currentTarget.children[0]);
}

hoverControls.hide = function(e) {
  e.currentTarget.children[0].controls = false;
  console.log(e.currentTarget.children[0]);
}

export default LoadManager.queue(hoverControls, QUEUE.DOM);
