import {$, $1} from '../../utilities/selector'

import LoadManager, { QUEUE } from '../load-manager'

const ar = {
  "16:9": 1920/1080,
  "3:4": 640/480
};

const LoadVideo = () => {
  let container = document.createElement('div');

  [...$('[data-js=async-video]')].forEach(el => {

    container.innerHTML = el.innerHTML;

    let video = el.parentNode;

    let getSource = getSourceData.bind(null, video.offsetWidth);

    // Build list of sources
    let sources = [...container.children].reduce(getSource, {});

    // The closest resolution to current screen size
    let closest = Object.values(sources).reduce(getClosest);

    // Add all sources to DOM
    closest.elements.forEach(video.appendChild, video);
  })

  // Create object containing all source data
  function getSourceData(containerWidth, sources, src) {
    let res = parseInt(src.getAttribute('data-resolution'));

    if ( ! res ) return sources;

    if ( typeof sources[res] == "undefined" ) {
      let aspectRatio = res > 480 ? ar["16:9"] : ar["3:4"];
      let width = (res * aspectRatio);
      let diff = Math.abs(containerWidth - width);
      sources[res] = {
        diff,
        elements: []
      };
    }

    sources[res].elements.push(src);

    return sources;
  }

  function getClosest(closest, curr) {
    return curr.diff < closest.diff ? curr : closest
  }
}

export default LoadManager.queue(LoadVideo, QUEUE.RESOURCES);
