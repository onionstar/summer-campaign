import anime from 'animejs'

import {$} from '../../utilities/selector'

import LoadManager, { QUEUE } from '../load-manager'

import {iconPlay, iconPause} from '../../templates/icons'

function createPlayButton() {
  let el = document.createElement("button");
  let svg = createSvg("svg");
  let p = createSvg("path");

  // Button
  el.type = "button";
  el.className = "Video-button u-center";
  el.appendChild(svg);

  // SVG
  svg.setAttribute("class", "Video-buttonIcon");
  svg.setAttribute("viewBox", "0 0 100 100");
  svg.setAttribute("width", "100%");
  svg.setAttribute("height", "100%");
  svg.appendChild(p)

  // Path
  p.setAttribute("d", iconPlay);
  p.setAttribute("stroke", "none");

  return el;
}

function createSvg(name) {
  return document.createElementNS("http://www.w3.org/2000/svg", name);
}

function createTween(el) {
  return anime({
    targets: el,
    d: [iconPlay, iconPause],
    duration: 250,
    autoplay: false,
    easing: "easeInSine"
  })
}

function toggle(tween, e) {
  let v = e.currentTarget;
  if ( v.paused ) {
    v.play()
    onPlay(v.parentNode, tween)
  } else {
    v.pause()
    onPause(v.parentNode, tween)
  }
}

function onPlay(video, tween) {
  video.classList.add('is-playing');
  tween.play();
  if ( tween.reversed ) {
    tween.reverse();
  }
}

function onPause(video, tween) {
  video.classList.remove('is-playing');
  tween.play();
  if ( ! tween.reversed ) {
    tween.reverse();
  }
}

const LoadVideo = () => {

  [...$('[data-js~=video-play]')].forEach(el => {
    let btn = createPlayButton();
    let tween = createTween(btn.querySelector('path'));
    el.parentNode.appendChild(btn);
    el.addEventListener("click", toggle.bind(null, tween));
    el.addEventListener("play", onPlay.bind(null, el.parentNode, tween));
    el.addEventListener("pause", onPause.bind(null, el.parentNode, tween));
  })

}


export default LoadManager.queue(LoadVideo, QUEUE.RESOURCES);
