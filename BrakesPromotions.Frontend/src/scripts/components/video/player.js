import anime from 'animejs'

import {$1, $} from '../../utilities/selector'

import LoadManager, {QUEUE} from '../load-manager'
import ComponentManager from '../component-manager'

import {controls} from '../../templates/video-controls';
import {iconPlay, iconPause} from '../../templates/icons'

export class VideoPlayer {
  static initControls(video, template) {
    let wrap = document.createElement("div");
    wrap.innerHTML = template(video);
    let container = video.parentNode.appendChild(wrap.children[0]);

    return {
      container,
      play: $1("[data-js=controls-play]", container),
      playIcon: $1("[data-js=controls-playIcon]", container),
      track: $1("[data-js=controls-track]", container),
      progress: $1("[data-js=controls-progress]", container),
      mute: $1("[data-js=controls-mute]", container),
      muteIcon: $("[data-js=mute-icon]", container)
    };
  }

  static playTween(tween, forward) {
    if ( ! tween )
      return;
    if ( forward && tween.reversed ) {
      tween.reverse();
    } else if ( ! forward && ! tween.reversed ) {
      tween.reverse();
    }
    tween.play();
  }

  constructor(video, {template}) {
    // Disable on iOS
    if ( !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform) ) {
      return;
    }

    this.video = video;
    this.template = template;

    this.togglePlayback = this.togglePlayback.bind(this);

    this.controls = VideoPlayer.initControls(video, template);
    this.bindControls();

    this.video.controls = false;
  }

  bindControls() {
    if (! this.controls.container)
      return;

    let parent = this.video.parentNode;

    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);

    parent.addEventListener("mouseenter", this.show);
    parent.addEventListener("mouseleave", this.hide);

    this.video.addEventListener("click", this.togglePlayback);

    if ( this.controls.play ) {
      this.resetPlayButton = this.resetPlayButton.bind(this);
      this.controls.play.addEventListener("click", this.togglePlayback);
      this.video.addEventListener("pause", this.resetPlayButton);
    }

    if ( this.controls.playIcon ) {
      this.playIconTween = anime({
        targets: this.controls.playIcon,
        d: [iconPlay, iconPause],
        duration: 250,
        autoplay: false,
        easing: "easeInSine"
      });
    }

    if ( this.controls.track ) {
      this.updateTrack = this.updateTrack.bind(this);
      this.jumpTo = this.jumpTo.bind(this);
      this.video.addEventListener("timeupdate", this.updateTrack);
      this.controls.track.addEventListener("click", this.jumpTo);
    }

    if ( this.controls.mute ) {
      this.toggleVolume = this.toggleVolume.bind(this);
      this.controls.mute.addEventListener("click", this.toggleVolume);
    }

    if ( this.controls.muteIcon ) {
      this.muteIconTween = anime({
        targets: this.controls.muteIcon,
        opacity: [1, 0],
        duration: 250,
        delay: (el, i) => i * 100,
        autoplay: false,
        easing: "easeInSine"
      })
      if ( this.video.muted ) {
        this.muteIconTween.play();
      }
    }
  }

  show() {
    this.controls.container.classList.add("is-visible");
  }

  hide() {
    this.controls.container.classList.remove("is-visible");
  }

  togglePlayback(e) {
    if ( this.video.paused ) {
      VideoPlayer.playTween(this.playIconTween, true)
      this.video.play()
    } else {
      VideoPlayer.playTween(this.playIconTween, false)
      this.video.pause()
    }
  }

  resetPlayButton(e) {
    if ( e.currentTarget.ended && this.playIconTween ) {
      VideoPlayer.playTween(this.playIconTween, true)
    }
  }

  updateTrack() {
    let progress = this.video.currentTime / this.video.duration;
    this.controls.progress.style.transform = `scaleX(${progress})`;
  }

  jumpTo(e) {
    let position = e.layerX / this.controls.track.offsetWidth;
    this.video.fastSeek(position * this.video.duration);
  }

  toggleVolume() {
    this.video.muted = !this.video.muted;
    VideoPlayer.playTween(this.muteIconTween, this.video.muted);
  }
}

export default LoadManager.queue(() => {
  new ComponentManager(VideoPlayer, "[data-js=video-player]", {template: controls});
}, QUEUE.DOM)
