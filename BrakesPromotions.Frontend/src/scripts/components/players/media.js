import {$1} from '../../utilities/selector'

import LoadManager, {QUEUE} from '../load-manager'
import ComponentManager from '../component-manager'

function MediaPlayer(el, options) {
  if ( ! el ) 
    throw new Error('MediaPlayer element not found');
  
  if ( ! (this instanceof MediaPlayer) )
    return new MediaPlayer(el, options);
  
  this.toggle = this.toggle.bind(this);
  this.onPlay = this.onPlay.bind(this);
  this.onPause = this.onPause.bind(this);
  
  this.el = el;
  
  this.options = options;
  this.autoplay = !!this.el.getAttribute('autoplay');
  this.controls = !!this.el.getAttribute('controls');
  
  this.media = $1('[data-js=media-player]', this.el);
  this.overlay = $1('[data-js=media-overlay]', this.el);
  
  LoadManager.queue(this.init.bind(this), QUEUE.RESOURCES)
}

MediaPlayer.prototype.init = function () {
  this.media.addEventListener('click', this.toggle);
  this.media.addEventListener('play', this.onPlay);
  this.media.addEventListener('pause', this.onPause);
  
  if ( this.overlay ) {
    this.overlay.addEventListener('click', this.toggle)
  }
};

MediaPlayer.prototype.toggle = function () {
  if ( this.media.paused ) {
    this.media.play()
  } else {
    this.media.pause();
  }
};

MediaPlayer.prototype.onPlay = function () {
  this.el.classList.add('is-playing');
};

MediaPlayer.prototype.onPause = function () {
  this.el.classList.remove('is-playing');
};

export default LoadManager.queue(() => (
  new ComponentManager(MediaPlayer, "[data-js~=media]")
), QUEUE.DOM)