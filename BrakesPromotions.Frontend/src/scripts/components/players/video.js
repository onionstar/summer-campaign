import {$1} from '../../utilities/selector'

import LoadManager, {QUEUE} from '../load-manager'
import ComponentManager from '../component-manager'
import {DebouncedResizeManager} from '../optimisations/optimised-resize-manager'

function Video(el, options) {
  if ( ! el ) 
    throw new Error('Video element not found');
  
  if ( ! (this instanceof Video) )
    return new Video(el, options);
  
  this.toggle = this.toggle.bind(this);
  this.play = this.play.bind(this);
  this.onPlay = this.onPlay.bind(this);
  this.onMouseEnter = this.onMouseEnter.bind(this);
  this.onMouseLeave = this.onMouseLeave.bind(this);
  
  this.el = el;
  
  this.options = options;
  this.autoplay = !!this.el.getAttribute('autoplay');
  this.controls = !!this.el.getAttribute('controls');
  this.currentSrc = null;
  
  this.media = $1('video', this.el);
  this.overlay = $1('[data-js=video-overlay]', this.el);
  let sources = $1('[data-js=video-sources]', this.el);
  
  if ( sources ) {
    this.sources = JSON.parse(sources.innerHTML.trim());
    this.sizes = this.sources.map(x => x.Resolution);
    
    LoadManager.queue(this.init.bind(this), QUEUE.RESOURCES)
  } else {
    throw new Error("Video has no sources")
  }
}

Video.prototype.init = function () {
  this.source = Video.createSource();
  this.media.insertBefore(this.source, this.media.children[0]);
  
  this.media.addEventListener('click', this.toggle);
  this.media.addEventListener('play', this.onPlay);
  this.media.addEventListener('mouseenter', this.onMouseEnter);
  this.media.addEventListener('mouseleave', this.onMouseLeave);

  if ( this.autoplay ) {
    this.media.addEventListener('load', this.play);
  }
  
  if ( this.overlay ) {
    this.overlay.addEventListener('click', this.play)
  }
  
  // Resize handling
  DebouncedResizeManager.add({
    action: (e, p)  => {
      this.currentSrc = Video.getCurrentSrc(Video.getClosestResolution(this.sizes), this.sources);
      if ( this.currentSrc.Url !== this.source.getAttribute('src') ) {
        this.source.setAttribute('src', this.currentSrc.Url);
        this.media.load();
      }
    }
  });
};

Video.prototype.toggle = function () {
  if ( this.media.paused ) {
    this.media.play()
  } else {
    this.media.pause();
  }
};

Video.prototype.play = function () {
  this.media.play();
};

Video.prototype.onPlay = function () {
  this.el.classList.add('is-playing');
};

Video.prototype.onMouseEnter = function () {
  clearTimeout(this.overlayTimer);
};

Video.prototype.onMouseLeave = function () {
  if ( this.media.paused ) {
    this.overlayTimer = setTimeout(() => {
      this.el.classList.remove('is-playing');
    }, 1000);
  }
};

Video.createSource = function () {
  let el = document.createElement('source');
  el.setAttribute('type', 'video/mp4');
  return el;
};

Video.getCurrentSrc = function(target, sources) {
  return sources.find(x => x.source === target) || sources[0];
};

// Find which resolution is the closest to our current
// window size
Video.getClosestResolution = function (sizes) {
  let w = window.innerWidth;
  
  return sizes[
    sizes
      .map( x => ( x * ( x > 480 ? 1920 / 1080 : 640/480 ) ) / w )
      .findIndex( (x, i, arr) => x === arr.reduce(Video.getSmallestDiff))
    ];
};

Video.getSmallestDiff = function (accum, curr) {
  return Math.abs(curr - 1) < Math.abs(accum - 1) ? curr : accum;
};

export default LoadManager.queue(() => (
  new ComponentManager(Video, "[data-js~=video]")
), QUEUE.DOM)