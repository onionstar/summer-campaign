import MediaPlayer from './media'
import VideoPlayer from './video'

export default {
  MediaPlayer,
  VideoPlayer
};
