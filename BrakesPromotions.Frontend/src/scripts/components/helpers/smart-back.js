import {$1} from '../../utilities/selector'

import LoadManager, {QUEUE} from '../load-manager'

const BackButton = ({el, condition}) => {
  if ( ! el ) return;
  el.addEventListener('click', (e) => {
    if ( condition(el) ) {
      e.preventDefault();
      window.history.back();
    }
  })
}

export default LoadManager.queue(() => {

  // const UsedBackButton = BackButton({
  //   el: $1('[data-js=used-back-button]'),
  //   condition: el => document.referrer.indexOf('/used') > 0
  // })

}, QUEUE.DOM);
