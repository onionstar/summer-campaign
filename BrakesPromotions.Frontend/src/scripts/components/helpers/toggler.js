import {$1, $} from '../../utilities/selector'

import LoadManager, {QUEUE} from '../load-manager'

export const Toggler = function(el) {
  el = el || $1('[data-js~=toggler]');

  if ( ! el ) return;

  if ( ! el.hasAttribute('data-id') )
    throw new Error("Togglable element must have an ID");

  const target = el.getAttribute('data-id');

  const triggers = {
    open: [...$(`[data-js~=toggler-open][data-target=${target}]`)],
    close: [...$(`[data-js~=toggler-close][data-target=${target}]`)],
    toggle: [...$(`[data-js~=toggler-toggle][data-target=${target}]`)]
  };

  if ( ! triggers.open.length && ! triggers.close.length && ! triggers.toggle.length )
    throw new Error(`Togglable element ${target} has no triggers`);

  Object.keys(triggers).forEach(action => {
    triggers[action].forEach(el => {
      el.addEventListener('click', Toggler[action])
    })
  })
}

Toggler.getTarget = function(el) {
  const id = el.getAttribute('data-target');
  const target = $1(`[data-js~=toggler][data-id=${id}]`);
  if ( ! target )
    throw new Error(`Target ${id} was not found`);
  
  return target;
}

Toggler.open = function(e) {
  const target = Toggler.getTarget(e.currentTarget);
  target.classList.add('is-open');
}

Toggler.close = function(e) {
  const target = Toggler.getTarget(e.currentTarget);
  target.classList.remove('is-open');
}

Toggler.toggle = function(e) {
  const target = Toggler.getTarget(e.currentTarget);
  target.classList.toggle('is-open');
}

export default LoadManager.queue(() => {
  [...$("[data-js~=toggler]")].forEach(Toggler);
}, QUEUE.DOM);
