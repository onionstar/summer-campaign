import {$1} from '../../utilities/selector';
import {scrollTo} from '../../utilities/scrollto';
import {getOffset} from '../../utilities/offset';

import {DebouncedResizeManager} from '../optimisations/optimised-resize-manager';
import ComponentManager from '../component-manager';
import LoadManager, { QUEUE } from '../load-manager'

const scrollEl = (document.scrollingElement||document.body);

const comparePosition = {
  above: function(p, h) {
    return scrollEl.scrollTop < p
  },
  below: function(p, h) {
    return scrollEl.scrollTop > (p + h);
  }
}

export class ScrollTo {
  constructor(el) {
    this.el = el;
    this.conditions = [];
    this.init();
  }

  init() {
    if ( this.el.hasAttribute('data-target') ) {
      this.target = $1(`[data-anchor=${this.el.getAttribute('data-target')}]`);
    } else if ( this.el.href && this.el.getAttribute("href")[0] === "#" ) {
      this.target = document.getElementById(this.el.getAttribute("href").substr(1));
    } else {
      console.warn("ScrollTo trigger element must have a target");
      return;
    }

    if ( this.target ) {
      if ( this.el.hasAttribute('data-scroll-conditions') ) {
        this.conditions = this.el.getAttribute('data-scroll-conditions')
          .split(' ')
          .filter(x => typeof comparePosition[x] === "function");
      }

      LoadManager.queue(this.ready.bind(this), QUEUE.RESOURCES)
    } else {
      console.warn("ScrollTo target could not be found");
    }
  }

  ready() {
    // Calculate the offset from the size of the header
    // Update when the browser resizes
    // DebouncedResizeManager.add({
    //   action: () => {
    //     this.offset = parseInt(document.body.style.paddingTop || 95);
    //   }
    // })
    this.bind();
  }

  bind() {
    this.el.addEventListener('click', this.go.bind(this));
  }

  go(e) {
    e.preventDefault();
    if ( ! this.conditions.length || this.checkConditions() )
      scrollTo(this.target, -this.offset);
  }

  targetBounds() {
    return [getOffset(this.target) - this.offset, this.target.offsetHeight];
  }

  checkConditions() {
    let t = this.targetBounds();
    return this.conditions.some(x => comparePosition[x](...t));
  }
}

export default LoadManager.queue(() => (
  new ComponentManager(ScrollTo, '[data-js~=scroll-to]')
), QUEUE.DOM);
