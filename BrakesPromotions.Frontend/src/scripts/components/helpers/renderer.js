export default class Renderer {
  constructor(context, method) {
    if ( ! context )
      throw new Error("Renderer must have a context");

    if ( ! method )
      throw new Error("Renderer must have a render method");

    this.context = context;
    this.method = method;
    this.render = this.render.bind(this);
    this.wrapper = context.cloneNode(false);
    this.wrapper.style.width = context.offsetWidth;
    this.context.addEventListener("render", this.render);
  }

  render({detail}) {
    if ( detail.html ) {
      this.wrapper.innerHTML = detail.html;
    }
    this.method(this.context, [...this.wrapper.children], detail);
  }
}
