import {getOffset} from '../../utilities/offset'
import {parseBreakpoints, conditionsMet} from '../../utilities/breakpoints'

import ComponentManager from '../component-manager';
import LoadManager, {QUEUE} from '../load-manager';
import ScrollManager from '../optimisations/throttled-scroll-manager'
import {DebouncedResizeManager} from '../optimisations/optimised-resize-manager'

const scrollEl = (document.scrollingElement||document.body);

const STATE = {
  START: 0,
  STUCK: 1,
  END: 2,
}

class Sticky {
  constructor(el) {
    this.el = el;
    this.top = parseInt(el.getAttribute('data-sticky-top')) || 0;
    this.breakpoints = parseBreakpoints(el.getAttribute('data-sticky-at'));
    this.isStuck = false;
    this.state = STATE.START;
    this.docLength = document.body.offsetHeight;
    this.recalculate = this.recalculate.bind(this);

    LoadManager.queue(this.ready.bind(this), QUEUE.RESOURCES)
  }

  ready() {
    this.buildPlaceholder()

    DebouncedResizeManager.add({
      action: this.recalculate
    })

    window.addEventListener("redraw", this.recalculate);

    ScrollManager.add({
      action: this.check.bind(this)
    })
  }

  recalculate() {
    this.unstick(false)

    if ( conditionsMet(this.breakpoints) ) {
      this.buildPlaceholder()
      this.position = getOffset(this.el)
      this.offset = parseInt(document.body.style.paddingTop) || 0;
      let {height, paddingTop, paddingBottom} = getComputedStyle(this.el.parentNode);
      this.docLength = document.body.offsetHeight;
      this.parentHeight = parseFloat(height) - (parseFloat(paddingTop) + parseFloat(paddingBottom));
      this.check()
    }
  }

  check() {
    if ( ! conditionsMet(this.breakpoints) ) return;
    if ( document.body.offsetHeight != this.docLength ) return this.recalculate();
    let top = scrollEl.scrollTop + this.offset + this.top;
    let end = this.position + this.parentHeight;
    let elHeight = this.el.offsetHeight;

    if ( top >= this.position && top + elHeight <= end ) {
      this.state === STATE.STUCK || this.stick()
    } else if ( top + elHeight >= end ) {
      this.state === STATE.END || this.unstick(true, end - elHeight);
    } else {
      this.state === STATE.START || this.unstick(false);
    }
  }

  stick() {
    let {width} = getComputedStyle(this.el);
    Object.assign(this.el.style, {
      top: this.offset + this.top + 'px',
      position: 'fixed',
      width
    });
    if ( ! this.el.parentNode.contains(this.placeholder) ) {
      this.el.parentNode.insertBefore(this.placeholder, this.el)
    }
    this.el.classList.add('is-stuck');
    this.state = STATE.STUCK;
  }

  unstick(isBottom, distance) {
    Object.assign(this.el.style, isBottom ? {
      top: distance + 'px',
      position: 'absolute'
    } : {
      top: 'auto',
      position: 'static',
      width: 'auto'
    });
    if ( !isBottom && this.el.parentNode.contains(this.placeholder) ) {
      this.el.parentNode.removeChild(this.placeholder)
    }
    this.el.classList.remove('is-stuck');
    this.state = isBottom ? STATE.END : STATE.START;
  }

  buildPlaceholder() {
    if ( this.placeholder == null ) {
      this.placeholder = document.createElement('div');
    }
    let {height, marginTop, marginBottom} = getComputedStyle(this.el);
    Object.assign(this.placeholder.style, {
      height,
      marginTop,
      marginBottom
    });
  }
}

export default LoadManager.queue(() => {
  new ComponentManager(Sticky, "[data-js=sticky]");
}, QUEUE.DOM)
