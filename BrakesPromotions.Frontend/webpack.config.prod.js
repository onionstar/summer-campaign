const config = require('config');
const webpack = require('webpack');

module.exports = function(defaults) {
  // Setup source maps
  defaults.devtool = config.get('debug') ? 'source-map' : 'hidden-source-map';

  // Minify
  defaults.plugins.push(new webpack.optimize.UglifyJsPlugin({
    sourceMap: config.get('debug'),
    compress: {
      warnings: false,
      screw_ie8: true,
      conditionals: true,
      unused: true,
      comparisons: true,
      sequences: true,
      dead_code: true,
      evaluate: true,
      if_return: true,
      join_vars: true
    }
  }))

  return defaults;
}
