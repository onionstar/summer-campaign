const fs = require('fs');
const path = require('path');
const glob = require('glob');
const prettyBytes = require('pretty-bytes');
const config = require('config');
const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');
const imageminSvgo = require('imagemin-svgo');

const src = path.resolve(__dirname, '../src/images');
const build = path.resolve(__dirname, '../', config.get('buildPath'), 'images');

const imgPattern = '*.{jpg,jpeg,png,svg}';

const imageminOpt = {
  plugins: [
    imageminMozjpeg(),
    imageminPngquant({
      quality: '65-80'
    }),
    imageminSvgo({
      plugins: [
        { removeTitle: true },
        { removeViewBox: false },
        { removeDesc: true },
        { transformsWithOnePath: true },
        { convertStyleToAttrs: true },
        { removeStyleElement: true },
        { 
          cleanupIDs: {
            preserve: ["thermometer-progress", "thermometer-goals"]
          }
        }
      ]
    }),
  ]
};

glob(src + "\\**\\*\\", (err, folders) => {
  if ( err ) throw err;

  minifyImages("");

  folders.forEach(folder => {
    minifyImages(path.relative(src, folder));
  })
})

function minifyImages(folder) {
  var source = path.resolve(src, folder, imgPattern);
  var output = path.resolve(build, folder);
  imagemin([source], output, imageminOpt).then(logSuccess);
}

function logSuccess(files) {
  // Output savings
  for ( let file of files ) {
    const name = path.relative(build, file.path);
    const original = fs.statSync(path.resolve(src, name));
    const optimal = fs.statSync(file.path);
    const saved = original.size - optimal.size;
    const percent = original.size > 0 ? ( saved / original.size ) * 100 : 0;
    const savedMsg = `saved ${prettyBytes(saved)} - ${percent.toFixed(1).replace(/\.0$/, '')}%`;
    const msg = saved > 0 ? savedMsg : 'already optimised';

    console.log(`${name}: ${msg}`);
  }
}
