var fs = require('fs');
var path = require('path');
var glob = require('glob');

class WebpackPostcleanPlugin {

  constructor(options = {}) {
    this.options = options;
  }

  apply(compiler) {
    const outputPath = compiler.options.output.path;

    compiler.plugin('done', (stats) => {
      if (compiler.outputFileSystem.constructor.name !== 'NodeOutputFileSystem') {
        return;
      }

      glob(this.options.paths, {cwd: outputPath}, (err, files) => {
        if ( err ) console.error(err);

        if ( files && files.length ) {
          if ( this.options.dry ) {
            console.log('%s file(s) would be deleted:', files.length);
            files.forEach(file => console.log('    %s', file));
          } else {
            if ( this.options.verbose )
              console.log('%s file(s) will be deleted:', files.length);

            files.forEach(file => {
              fs.unlink(path.resolve(outputPath, file), err => (
                err ? console.error(err) : !this.options.verbose || console.log('    %s', file)
              ))
            });
          }
        } else {
          console.log("No files found");
        }
      })
    });
  }

}

module.exports = WebpackPostcleanPlugin;
