var fs = require('fs');
var path = require('path');
var glob = require('glob');
var fse = require('fs-extra');
var config = require('config');
var Fontmin = require('fontmin');
var ttf2woff2 = require('ttf2woff2');

const src = path.resolve(__dirname, '../src/fonts');
const build = path.resolve(__dirname, '../', config.get('buildPath'), 'fonts');

function fromTtf(files) {
  toWoff(files);
  toWoff2(files);
}

function toWoff(files) {
  const fontmin = new Fontmin();
  fontmin
    .src(files)
    .use(Fontmin.ttf2woff({
      deflate: true
    }))
    .dest(build)
    .run((err, files) => {
      if ( err ) throw err;
      files.forEach(file => {
        logResult(file.history.pop());
      })
    });
}

function toWoff2(files) {
  glob(files, (err, files) => {
    if ( err ) throw err;

    files.forEach(file => {

      fs.readFile(file, (err, data) => {
        if ( err ) throw err;
        writeFile(file, 'woff2', data);
      })

    })

  })
}

function writeFile(file, ext, data) {
  var name = path.resolve(build, path.basename(file).replace('ttf', ext));
  fse.outputFile(name, ttf2woff2(data), err => {
    if (err) throw err;
    logResult(name);
  });
}

function logResult(name) {
  console.log(`Generated: ${path.basename(name)}`);
}

fromTtf(path.resolve(src, '*.ttf'));
