var fse = require('fs-extra');
var path = require('path');
var glob = require('glob');
var config = require('config');

const src = path.resolve(__dirname, '../src/copy');
const build = path.resolve(__dirname, '../', config.get('buildPath'));

glob(path.resolve(src, '**/*.*'), (err, files) => {
  if ( err ) throw err;

  files.forEach(file => {
    var relative = path.relative(src, file);
    var newFile = path.resolve(build, relative);
  
    fse.copy(file, newFile, err => {
      if (err) return console.error(err)
      console.log(`${relative} moved to ${path.relative(path.resolve(__dirname, '../../'), newFile)}`);
    })
  })
});