const fs = require('fs');
const mkdirp = require('mkdirp');
const path = require('path');
const glob = require('glob');
const config = require('config');
const svgSpriter = require('svg-sprite');

const src = path.resolve(__dirname, '../src/icons');
const build = path.resolve(__dirname, '../', config.get('buildPath'), 'icons');

const svgoConfig = {
  plugins: [
    { rootAttributes: { xmlns: "http://www.w3.org/2000/svg" } },
    { sortAttrs: true },
    { removeTitle: true },
    { removeViewBox: false },
    { removeDesc: true },
    { transformsWithOnePath: true },
    { convertStyleToAttrs: true },
    { removeStyleElement: true }
  ]
};

const spriter = new svgSpriter({
  dest: build,
  shape: {
    transform: [{ svgo: svgoConfig }]
  },
  svg: svgoConfig,
  mode: {
    symbol: {
      inline: true,
      dest: "",
      prefix: ".icon-%s",
      sprite: "icons.svg"
    }
  }
});

glob(path.resolve(src, '**/*.svg'), (err, result) => {
  if (err) throw err;

  result.forEach(file => {
    let name = path.relative(src, file);
    spriter.add(file, name, fs.readFileSync(file));
  });

  spriter.compile((error, result) => {
    for (let mode in result) {
      for (let resource in result[mode]) {
        mkdirp.sync(path.dirname(result[mode][resource].path));
        fs.writeFileSync(result[mode][resource].path, result[mode][resource].contents);
      }
    }
  });
});
