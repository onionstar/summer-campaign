var fs = require('fs');
var fse = require('fs-extra');
var path = require('path');
var config = require('config');

const src = path.resolve(__dirname, '../src/');
const build = path.resolve(__dirname, '../', config.get('buildPath'));

var folders = []; // Add any extra folders you'd like to clean in here

if ( config.get('cms') ) {

  var defaults = ['scripts', 'styles', 'fonts', 'images', 'icons'];
  var copyPath = path.resolve(src, 'copy');
  var copyFolders = fs.existsSync(copyPath) ? fs.readdirSync(copyPath) : [];
  var folders = new Set([].concat(defaults, copyFolders, folders).map(f => (
    path.resolve(build, f)
  )));

  folders.forEach(f => {
    if ( fs.existsSync(f) ) {
      fse.remove(f, err => {
        if ( err ) throw err;
        console.log(`${path.relative(build, f)} folder cleaned!`);
      });
    }
  });

} else {

  fse.emptyDir(build, err => {
    if ( err ) throw err;
    console.log('Build folder cleaned!');
  });

}
