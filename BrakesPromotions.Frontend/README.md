# Setup
To run build tasks, please make sure you have the following VS packages installed:

- [Markdown Editor, which will make this file more readable!](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.MarkdownEditor)
- [Editor Config for Visual Studio](https://marketplace.visualstudio.com/items?itemName=EditorConfigTeam.EditorConfig)
- [Node.js Tools for Visual Studio](https://www.visualstudio.com/vs/node-js/)
- [NPM Task Runner](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.NPMTaskRunner)

If this is your first time running node, you will also need to install the windows-build-tools. This can be done either via the command line using `npm i -g --production windows-build-tools`, or by a much more long winded GUI process, described here:
https://github.com/nodejs/node-gyp

## Installing Node Modules
Next, you will need to install the node modules, which is how we handle all front end dependencies. This can be done either from within Visual Studio, using either the Task Runner Explorer or from the Solution Explorer, or via CLI. Use whatever you are more comfortable with.

### Via Solution Explorer
Locate the NodeJS project in your solution (usually named `[ProjectName].Frontend`). At the top of the project, there should be an item labelled "npm". Simply right-click on this, and select Install Missing NPM Packages.

### Via Task Runner
Simply navigate to the task runner window of Visual Studio, and run `package.json -> Default -> Install`.

### Via CLI
Open up your preferred command prompt (cmder is recommended) and navigate to the folder containing the package.json, then simply run `npm i`.

### Notes on NPM Packages
While SemVer should ensure updating NPM packages is safe, there is always the potential for package authors to not adhere to SemVer. This can potentially result in updating packages producing breaking changes. Unless told otherwise, leave updating node packages to the front end team.

## Configuration
Some settings may need to be configured to get everything working as desired. This is handled by a combination of your environment variables and JSON configuration files.

### Configuration Files
If you are working on an existing project, you can skip steps 1 and 2. All of the configuration files are stored within the `config` folder and must be either JSON or JS files. By default, there will be `default.js`, `development.json`, `staging.json`. Tasks will load these configurations as needed based on which conditions are met.

All files which meet the settings for the current environment will be loaded. First the default will be loaded, then the file for the matching environment (as defined in the NODE_ENV environment variable), followed by the file matching the local hostname. This is the value returned when you type `hostname` into your preferred CLI. This allows us to set specific settings for different hosting environments, as well as setting local settings.

If you wish to learn more about how configurations are loaded, [check out this page](https://github.com/lorenwest/node-config/wiki/Configuration-Files).

#### 1. Configure Defaults
First you will need to configure the defaults. This can be done by editing `config/default.json`. This file will look something like this:
```JSON
{
  "debug": false,
  "buildPath": "../MSBuildExplore.Site",
  "url": "CHANGEME.visarcwebdev.co.uk",
  "cms": true
}
```

`debug` defines whether or not to include debug utilities, such as sourcemaps. This should be false by default, and changed using environment configuration files.

`buildPath` defines the path to the folder containing the main site project. In an Umbraco project, this will likely be PROJECTNAME.Site. For static sites, it should be `/build/`.

`url` defines the URL of the site, if it has one. This is only used for the webpack dev server to proxy to the existing local website.

`cms` if CMS is set to false, webpack will also handle template compilation.

#### 2. Configure Environment Settings
Now you have the defaults all setup, you'll want to configure the individual environments. There are 3 environments we want to consider, development, staging and production. The appropriate file will be loaded depending on the value of your local NODE_ENV environment variable.

The default settings for these should often be adequate, but you can change any if you need to.

#### 3. Configure Host Settings
With the defaults and environment settings all configured, you are now ready to setup the host configuration file.

This is the file which will only be loaded while running tasks on your machine. It's primary purpose is simply to setup the URL of your local website, so that the webpack dev server can proxy it.

### Environment Variables
You may need to configure your environment variables. This can be easily done either through visual studio or via CLI.

To change it within VS, right click on the front end project within your solution and click properties. There should be a textarea at the bottom where you may enter your environment variables. You will likely want to be working in development mode, so just paste the following into the box.

`NODE_ENV=development`

If you wish to set your environment variables via CLI, open your CLI of choice and simply type

`set NODE_ENV=development`

## Wrapping up
Now you should be all ready to press on using front end assets. Simply use the VS Task Runner to run the tasks you need.



# Tasks

## Build
`npm run build` - Compiles all frontend assets.

`npm run build:assets`/`webpack` - Compiles the JS and CSS files.

`npm run build:images` - Minifies and copies images.

`npm run build:icons` - Generates icon maps for reference usage.

`npm run build:fonts` - Generate WOFF and WOFF2 web fonts from TTF files.

`npm run build:copy` - Copies any static files over. Any files/folders placed in `src/copy/` will be copied over into the root of the build folder.

## Watch
`npm run watch`/`webpack --watch` - Runs the watch task for CSS/JS compilation. Any changes made to files will trigger a recompile.

## Server
`npm run server`/`webpack-dev-server` - Starts a development server which watches for file changes and live reloads on update. Compiles changes much faster than other options, but does not produce physical files.

*Only works in `NODE_ENV=development`*

## Clean
`npm run clean` - Deletes any previously generated files, ready for a fresh run. Please note, some files imported by the copy task may be left behind.


## Todo

### Lint
- Styles
- Scripts

### Optional
- Environment Toggle
