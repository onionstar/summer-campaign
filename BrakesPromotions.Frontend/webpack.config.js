console.log(process.env.NODE_ENV)

// Load the appropriate config based on current environment
let env = process.env.NODE_ENV === 'development' ? "dev" : "prod";
let config = require(`./webpack.config.${env}.js`)(
  require('./webpack.config.default.js')
)

module.exports = config;
