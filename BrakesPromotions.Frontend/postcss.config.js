const autoprefixer = require('autoprefixer');
const cssmqpacker = require('css-mqpacker');
const cssnano = require('cssnano');

const isMsMq = (val) => val.indexOf("-ms-high-contrast") >= 0
const isMaxMq = (val) => val.indexOf("max-width") >= 0
const isMinMq = (val) => val.indexOf("min-width") >= 0

const regexMaxWidth = /\(max-width: (\d+)px\)/;
const regexMinWidth = /\(min-width: (\d+)px\)/;
const getValue = (regex, mq) => parseInt(mq.match(regex)[1]);

const parseQuery = (val) => {
  let obj = {
    ms: isMsMq(val),
    max: null,
    min: null
  };
  if (isMaxMq(val) && regexMaxWidth.test(val)) {
    obj.max = getValue(regexMaxWidth, val)
  }
  if (isMinMq(val) && regexMinWidth.test(val)) {
    obj.min = getValue(regexMinWidth, val)
  }
  return obj
}

const sizeEmpty = (obj) => (obj.min == null && obj.max == null);
const sizeFull = (obj) => (obj.min != null && obj.max != null);

const sort = (a, b) => {
  a = parseQuery(a);
  b = parseQuery(b);

  if (a.ms && !b.ms) {
    return 1;
  }
  if (!a.ms && b.ms) {
    return -1;
  }
  if (a.ms && b.ms) {
    let aEmpty = sizeEmpty(a);
    let bEmpty = sizeEmpty(b);
    if (aEmpty && !bEmpty) {
      return -1;
    }
    if (!aEmpty && bEmpty) {
      return 1;
    }
  }
  if (a.min != null && b.min == null) {
    return -1;
  }
  if (a.min == null && b.min != null) {
    return 1;
  }
  if (a.min != null && b.min != null && a.min != b.min) {
    if (a.min < b.min) {
      return -1;
    } else {
      return 1;
    }
  }
  if (a.max != null && b.max == null) {
    return 1;
  }
  if (a.max == null && b.max != null) {
    return -1;
  }
  if (a.max != null && b.max != null && a.max != b.max) {
    if (a.max < b.max) {
      return 1;
    } else {
      return -1;
    }
  }
  return 0;
}

module.exports = {
  plugins: [
    autoprefixer({
      browsers: ['last 2 versions'],
      //grid: true,
      flexbox: "no-2009"
    }),
    cssmqpacker({
      sort: sort
    }),
    cssnano({
      preset: ['default', {
        discardUnused: {
          namespace: false
        },
        discardComments: {
          remove: function(comment) {
            return comment.indexOf("umb_") == -1;
          }
        }
      }]
    })
  ]
}
