module.exports = {
  plugins: [
    "stylelint-selector-bem-pattern",
    "stylelint-at-rule-no-children",
    "stylelint-declaration-strict-value"
  ],
  rules: {
    "at-rule-no-unknown": [true, {
      ignoreAtRules: [
        "include", 
        "mixin", 
        "content", 
        "return", 
        "function", 
        "if",
        "for",
        "foreach",
        "else", 
        "extend", 
        "debug", 
        "warn"
      ]
    }],
    "block-no-empty": null,
    "color-no-invalid-hex": true,
    "comment-no-empty": true,
    "comment-empty-line-before": [ "always", {
      "ignore": ["stylelint-commands", "after-comment"]
    } ],
    "declaration-colon-space-after": "always-single-line",
    "declaration-colon-newline-after": "always-multi-line",
    "font-family-no-duplicate-names": true,
    "font-weight-notation": "numeric",
    "function-calc-no-unspaced-operator": true,
    "function-comma-space-after": "always-single-line",
    "function-linear-gradient-no-nonstandard-direction": true,
    "indentation": [2, {
      //"except": ["value"],
      "ignore": ["value"],
      "message": "Please use 2 spaces for indentation. Tabs make The Architect grumpy."
    }],
    "keyframe-declaration-no-important": true,
    "max-empty-lines": 2,
    "media-feature-name-no-unknown": true,
    "no-descending-specificity": true,
    "no-extra-semicolons": true,
    "property-no-unknown": true,
    "rule-empty-line-before": [ "always", {
      "except": ["first-nested"],
      "ignore": ["after-comment"]
    } ],
    "selector-max-specificity": ["0,3,1", {
      ignoreSelectors: [":global", ":local", "/my-/"]
    }],
    "selector-pseudo-class-no-unknown": true,
    "string-quotes": "double",
    "unit-case": "lower",
    "unit-no-unknown": true,
    "declaration-property-unit-blacklist": {
      "font-size": ["px"],
      "width": ["px"],
      "height": ["px"],
      "line-height": ["px"]
    },
    "shorthand-property-no-redundant-values": true,
    "plugin/selector-bem-pattern": {
      "preset": "suit",
      "implicitComponents": [
        "**\components\**\*.scss", "**\objects\**\*.scss",
        "**/components/**/*.scss", "**/objects/**/*.scss"
      ],
      componentSelectors: function(componentName, presetOptions) {
        componentName = componentName.split(/[-_]/).map(ucFirst).join('');
        const ns = (presetOptions && presetOptions.namespace) ? `${presetOptions.namespace}-` : '';
        const WORD = '[a-z0-9][a-zA-Z0-9]*';
        const descendant = `(?:-${WORD})?`;
        const modifier = `(?:--${WORD}(?:\.${ns}${componentName}${descendant}--${WORD})*)?`;
        const attribute = '(?:\\[.+\\])?';
        const state = `(?:\\.is-${WORD})*`;
        const body = descendant +
          modifier +
          attribute +
          state;
        return new RegExp(`^\\.${ns}${componentName}\\b${body}$`);
      },
      ignoreSelectors: ["^\\.s-[a-zA-Z ]*", ">"]
    }
  }
}

function ucFirst(x) {
  return x[0].toUpperCase() + x.slice(1);
}