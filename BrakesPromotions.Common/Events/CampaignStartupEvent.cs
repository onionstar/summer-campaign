﻿namespace BrakesPromotions.Common.Events
{
    // Namespaces
    using Manager.Services;
    using System;
    using System.Linq;
    using Umbraco.Core;
    using Umbraco.Core.Models;

    public class CampaignStartupEvent : ApplicationEventHandler
    {
        private static CampaignService campaignService = new CampaignService();
        private static GiftTierService giftTierService = new GiftTierService();
        private static GiftService giftService = new GiftService();

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            var campaigns = campaignService.GetAll();
            giftTierService.GetAll();
            giftService.GetAll();

            if (campaigns != null && campaigns.Any())
            {
                var memberGroupService = applicationContext.Services.MemberGroupService;
                foreach (var campaign in campaigns)
                {
                    var memberGroup = memberGroupService.GetByName(campaign.Name);
                    if (memberGroup == null)
                    {
                        memberGroup = new MemberGroup
                        {
                            Name = campaign.Name,
                            CreateDate = DateTime.UtcNow,
                            UpdateDate = DateTime.UtcNow,
                            CreatorId = -1
                        };
                        memberGroupService.Save(memberGroup, false);
                    }
                }
            }
        }
    }
}
