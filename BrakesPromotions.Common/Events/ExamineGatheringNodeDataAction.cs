﻿namespace BrakesPromotions.Common.Events
{
    // Namespaces
    using Examine;
    using System;
    using System.Linq;
    using Umbraco.Core;

    public class ExamineGatheringNodeDataAction : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            var myIndexer = ExamineManager.Instance.IndexProviderCollection["ExternalIndexer"];
            myIndexer.GatheringNodeData += IndexerOnGatheringNodeData;
        }

        private void IndexerOnGatheringNodeData(object sender, IndexingNodeDataEventArgs e)
        {

            var path = e.Fields["path"];
            var searchablePath = String.Join(" ", path.Split(',').Select(x => String.Format("{1}{0}{1}", x.ToLower(), '|')));

            if (e.Fields.ContainsKey("searchablePath"))
            {
                e.Fields["searchablePath"] = searchablePath;
            }
            else
            {
                e.Fields.Add("searchablePath", searchablePath);
            }
        }

    }
}
