﻿namespace BrakesPromotions.Common.Events
{
    // Namespaces
    using Controllers;
    using System.Web.Http;
    using Umbraco.Core;
    using Umbraco.Web.Mvc;

    public class RoutingEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //By registering this here we can make sure that if route hijacking doesn't find a controller it will use this controller.
            //That way each page will always be routed through one of our controllers.
            DefaultRenderMvcControllerResolver.Current.SetDefaultControllerType(typeof(DefaultController));

            //With the content finder we can match nodes to urls.
            //ContentFinderResolver.Current.InsertTypeBefore<ContentFinderByNotFoundHandlers, ErrorPageFinder>();

        }

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            // Map attribute routes for API controllers
            GlobalConfiguration.Configuration.MapHttpAttributeRoutes();
        }
    }
}
