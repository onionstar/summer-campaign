﻿namespace BrakesPromotions.Common.Events
{
    // Namespaces
    using Umbraco.Core;

    public class ExtendedViewEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            System.Web.Mvc.ViewEngines.Engines.Add(new ExtendedViewEngine());

            base.ApplicationStarting(umbracoApplication, applicationContext);
        }
    }
}
