﻿namespace BrakesPromotions.Common.Events
{
    // Namespaces
    using DevTrends.MvcDonutCaching;
    using System.Collections.Generic;
    using System.Linq;
    using Umbraco.Core;
    using Umbraco.Core.Cache;
    using Umbraco.Core.Events;
    using Umbraco.Core.Models;
    using Umbraco.Core.Services;
    using Umbraco.Core.Sync;
    using Umbraco.Web;
    using Umbraco.Web.Cache;

    public class CacheBustingEvents : ApplicationEventHandler
    {
        protected static UmbracoHelper Umbraco => new UmbracoHelper(UmbracoContext.Current);

        protected static OutputCacheManager CacheManager = new OutputCacheManager();

        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            PageCacheRefresher.CacheUpdated += ClearDonutCache;

            // Clear custom caches
            ContentService.Saving += ClearNodeFinderCache;
        }

        #region Cache Management

        private static void ClearDonutCache(PageCacheRefresher sender, CacheRefresherEventArgs e)
        {
            switch (e.MessageType)
            {
                case MessageType.RefreshById:
                case MessageType.RemoveById:
                    var contentId = (int)e.MessageObject;
                    var item = UmbracoContext.Current.ContentCache.GetById(contentId);
                    var docType = item?.DocumentTypeAlias ?? "";
                    ClearPageOutputCache(contentId, docType);
                    break;
                case MessageType.RefreshByInstance:
                case MessageType.RemoveByInstance:
                    if (!(e.MessageObject is IContent content))
                    {
                        return;
                    }
                    ClearPageOutputCache(content.Id, content.ContentType.Alias);
                    break;
                case MessageType.RefreshAll:
                    ClearPageOutputCache();
                    break;
            }
        }

        private static void ClearPageOutputCache(int contentId, string doctype)
        {
            if (!(OutputCache.Instance is IEnumerable<KeyValuePair<string, object>> enumerableCache))
            {
                return;
            }

            // get all output cache keys that contain current page id
            var keysToDelete = enumerableCache
                .Where(x => !string.IsNullOrEmpty(x.Key) && x.Key.Contains($"pageid={contentId};"))
                .Select(x => x.Key);

            // remove all caches for current page
            foreach (var key in keysToDelete)
            {
                OutputCache.Instance.Remove(key);
            }

        }

        private static void ClearPageOutputCache()
        {
            CacheManager.RemoveItems();
        }

        private static void ClearNodeFinderCache(IContentService sender, SaveEventArgs<IContent> e)
        {
            foreach (var content in e.SavedEntities)
            {
                var alias = content.ContentType.Alias;
                var cacheName = $"{alias}Node";
                if (content.HasPublishedVersion && System.Web.HttpRuntime.Cache[cacheName] != null)
                {
                    // Clear item from cache
                    System.Web.HttpRuntime.Cache.Remove(cacheName);
                }
            }
        }

        #endregion
    }
}
