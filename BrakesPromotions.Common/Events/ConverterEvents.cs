﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web.PropertyEditors.ValueConverters;

namespace BrakesPromotions.Common.Events
{
    public class ConverterEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            // Nested Content
            PropertyValueConvertersResolver.Current.RemoveType<NestedContentManyValueConverter>();
            PropertyValueConvertersResolver.Current.RemoveType<NestedContentSingleValueConverter>();

            // Media
            PropertyValueConvertersResolver.Current.RemoveType<MediaPickerPropertyConverter>();
        }
    }
}
