﻿namespace BrakesPromotions.Common.Controllers.Api
{
    //Namespaces
    using Helpers;
    using Manager.Services;
    using Models.Custom;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Web;
    using System.Web.Http;
    using Umbraco.Web;
    using Umbraco.Web.Security;
    using Umbraco.Web.WebApi;

    public class PdfController : UmbracoApiController
    {
        private static FileEventService fileEventService = new FileEventService();

        [Route("documents/{docid}")]
        [HttpGet]
        public HttpResponseMessage Display(int docid)
        {
            var media = Umbraco.TypedMedia(docid);

            byte[] fileData = File.ReadAllBytes(HttpContext.Current.Server.MapPath(media.Url));

            if (fileData == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var campaignGuid = GuidHelper.GetGuid("8a4df49c-3552-453d-9d1a-ac6763129211");

            MembershipHelper membershipHelper = new MembershipHelper(UmbracoContext.Current);
            var Member = Services.MemberService.GetByUsername(membershipHelper.GetCurrentMemberProfileModel().UserName);

            HttpResponseMessage Response = new HttpResponseMessage(HttpStatusCode.OK);

            fileEventService.Insert(new Manager.Models.Rdbms.EventTrack
            {
                Event = $"download {media.Name}",
                MediaId = media.GetKey(),
                EventDate = DateTime.UtcNow,
                EventGuid = Guid.NewGuid(),
                CampaignGuid = campaignGuid,
                MemberGuid = Member.Key
            });

            Response.Content = new ByteArrayContent(fileData);
            Response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline")
            {
                FileName = media.Name
            };
            Response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

            return Response;
        }
    }
}
