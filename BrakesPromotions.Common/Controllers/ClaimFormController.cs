﻿namespace BrakesPromotions.Common.Controllers
{
    // Namespaces
    using BLL;
    using BrakesPromotions.Common.Helpers;
    using BrakesPromotions.Manager.Services;
    using Controllers.Base;
    using DevTrends.MvcDonutCaching;
    using Models;
    using Models.Custom;
    using Models.Generated;
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Web.Mvc;
    using Umbraco.Core.Models;
    using Umbraco.Web;
    using Utilities;

    public class ClaimFormController : SurfaceRenderMvcController
    {
        private static GiftService giftService = new GiftService();
        private static GiftTypeService giftTypeService = new GiftTypeService();

        protected static UmbracoHelper _UmbracoHelper => new UmbracoHelper(UmbracoContext.Current);

        private static bool TestMode => bool.Parse(ConfigurationManager.AppSettings["Visarc.Testmode"]);

        [UmbracoDonutOutputCache(CacheProfile = "LongUserPageCache",
            Options = OutputCacheOptions.NoCacheLookupForPosts & OutputCacheOptions.ReplaceDonutsInChildActions,
            Order = 100)]
        public ActionResult SummerCampaignClaim()
        {
            var model = ModelLogic.CreateMasterModel() as MasterModel<Models.Generated.ClaimForm>;

            if (Members.IsLoggedIn() && !model.Campaign.IsAuthorised)
                return Redirect("/register/");

            if (!model.Campaign.ClaimOpen && !TestMode)
                return Redirect("/");

            if(Request.QueryString["q"]== null)
                return Redirect("/");

            if (model.Campaign.Accounts != null && model.Campaign.Accounts.Any())
            {
                if (model.Campaign.ActiveAccount.ClaimedGiftGuid.HasValue && (model.Campaign.ActiveAccount.ClaimedGiftGuid != null || model.Campaign.ActiveAccount.ClaimedGiftGuid != Guid.Empty))
                {
                    model.Content.Gift = new Gift
                    {
                        GiftGuid = model.Campaign.ActiveAccount.ClaimedGiftGuid.Value,
                        Name = giftService.FullGiftName(model.Campaign.ActiveAccount.ClaimedGiftGuid.Value, model.Campaign.ActiveAccount.ClaimedGiftTierGuid.Value),
                        IsClaimed = true
                    };

                    var giftType = giftTypeService.GetByTypeGuid(model.Campaign.ActiveAccount.ClaimedGiftTierGuid.Value);
                    var gift = giftService.GetByGiftGuid(model.Campaign.ActiveAccount.ClaimedGiftGuid.Value);

                    if (giftType.HasMedia)
                    {
                        IMedia content = Services.MediaService.GetById(gift.MediaId);
                        IPublishedContent media = content == null ? null : _UmbracoHelper.TypedMedia(content.Id);

                        if (media != null) model.Content.Gift.Image = media as Image;
                    }
                }
                else
                {
                    model.Content.SelectedGift = GuidHelper.GetGuid(Request.QueryString["q"]);
                }
            }

            return CurrentTemplate(model);
        }

        [ChildActionOnly]
        [UmbracoDonutOutputCache(CacheProfile = "NoCache",
        Options = OutputCacheOptions.NoCacheLookupForPosts)]
        public ActionResult ShowClaimForm(Guid GiftGuid, Guid AccountGuid)
        {
            var model = new Models.Custom.ClaimForm(GiftGuid, AccountGuid);

            return PartialView("SummerCampaign/Partials/Claim", model);
        }
    }
}
