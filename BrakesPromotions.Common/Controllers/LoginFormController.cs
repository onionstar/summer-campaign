﻿namespace BrakesPromotions.Common.Controllers
{
    // Namespaces
    using BLL;
    using Controllers.Base;
    using DevTrends.MvcDonutCaching;
    using Models;
    using Models.Custom;
    using Models.Generated;
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Web.Mvc;
    using Utilities;

    public class LoginFormController : SurfaceRenderMvcController
    {
        private static bool TestMode 
            => bool.Parse(ConfigurationManager.AppSettings["Visarc.Testmode"]);

        [AllowAnonymous]
        [UmbracoDonutOutputCache(
            CacheProfile = "NoCache",
            Options = OutputCacheOptions.NoCacheLookupForPosts &
                OutputCacheOptions.ReplaceDonutsInChildActions, 
            Order = 100
        )]
        public ActionResult SummerCampaignLogin()
        {
            var model = ModelLogic.CreateMasterModel() as MasterModel<LoginForm>;

            if (model.Campaign.Accounts.Any())
            {
                return Redirect("/");
            }
            return CurrentTemplate(model);
        }
    }
}
