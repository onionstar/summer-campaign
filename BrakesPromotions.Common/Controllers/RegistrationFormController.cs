﻿namespace BrakesPromotions.Common.Controllers
{
    // Namespaces
    using BLL;
    using Controllers.Base;
    using DevTrends.MvcDonutCaching;
    using Models;
    using Models.Custom;
    using Models.Generated;
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Web.Mvc;
    using Utilities;

    public class RegistrationFormController : SurfaceRenderMvcController
    {
        private static bool TestMode => bool.Parse(ConfigurationManager.AppSettings["Visarc.Testmode"]);

        [AllowAnonymous]
        [UmbracoDonutOutputCache(CacheProfile = "NoCache",
        Options = OutputCacheOptions.NoCacheLookupForPosts &
        OutputCacheOptions.ReplaceDonutsInChildActions, Order = 100)]
        public ActionResult SummerCampaignRegister()
        {
            var model = ModelLogic.CreateMasterModel() as MasterModel<RegistrationForm>;

            if (model.Campaign.Accounts.Any())
            {
                return Redirect("/");
            }
            return CurrentTemplate(model);
        }

        [ChildActionOnly]
        [UmbracoDonutOutputCache(CacheProfile = "NoCache",
        Options = OutputCacheOptions.NoCacheLookupForPosts)]
        public ActionResult ShowRegister(Guid CampaignGuid)
        {
            var model = new RegisterAccount { CampaignGuid = CampaignGuid };

            return PartialView("SummerCampaign/Partials/Register", model);
        }

        [ChildActionOnly]
        [UmbracoDonutOutputCache(CacheProfile = "NoCache",
        Options = OutputCacheOptions.NoCacheLookupForPosts)]
        public ActionResult ShowReIssue(Guid CampaignGuid, Guid AccountGuid)
        {
            var model = new ReIssueCode { CampaignGuid = CampaignGuid };

            return PartialView("SummerCampaign/Partials/ReIssue", model);
        }

        [ChildActionOnly]
        [UmbracoDonutOutputCache(CacheProfile = "NoCache",
        Options = OutputCacheOptions.NoCacheLookupForPosts)]
        public ActionResult ShowValidate(Guid CampaignGuid, Guid AccountGuid)
        {
            var model = new ValidateAccount { CampaignGuid = CampaignGuid, AccountGuid = AccountGuid };

            return PartialView("SummerCampaign/Partials/Validate", model);
        }
    }
}
