﻿namespace BrakesPromotions.Common.Controllers
{
    // Namespaces
    using Base;
    using BLL;
    using DevTrends.MvcDonutCaching;
    using System.Web.Mvc;
    using Umbraco.Web.Models;
    using Utilities;

    public class DefaultController : SurfaceRenderMvcController
    {
        [UmbracoDonutOutputCache(CacheProfile = "LongUserPageCache",
            Options = OutputCacheOptions.NoCacheLookupForPosts & OutputCacheOptions.ReplaceDonutsInChildActions,
            Order = 100)]
        public override ActionResult Index(RenderModel model)
        {
            var masterModel = ModelLogic.CreateMasterModel();
            return CurrentTemplate(masterModel);
        }
    }
}
