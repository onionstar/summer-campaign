﻿namespace BrakesPromotions.Common.Controllers
{
    // Namespaces
    using BLL;
    using Controllers.Base;
    using DevTrends.MvcDonutCaching;
    using Models;
    using Models.Generated;
    using System.Web.Mvc;
    using Utilities;

    public class NectarPointsController : SurfaceRenderMvcController
    {
        [UmbracoDonutOutputCache(CacheProfile = "LongUserPageCache",
            Options = OutputCacheOptions.NoCacheLookupForPosts & OutputCacheOptions.ReplaceDonutsInChildActions,
            Order = 100)]
        public ActionResult SummerCampaignNectar()
        {
            var model = ModelLogic.CreateMasterModel() as MasterModel<NectarPoints>;

            if (!Members.IsLoggedIn() || model.Campaign.ActiveAccount == null)
            {
                return Redirect("/");
            }

            if (!model.Campaign.ActiveAccount.NectarCustomer && model.Campaign.ActiveAccount.Brand.ToLower() == "brakes")
            {
                return Redirect("https://www.brake.co.uk/spring-loaded/");
            }

            if (!model.Campaign.ActiveAccount.NectarCustomer && model.Campaign.ActiveAccount.Brand.ToLower() == "woodward")
            {
                return Redirect("https://www.woodward-foodservice.com/promotions/3-ways-to-help-you-save/");
            }

            return CurrentTemplate(model);
        }
    }
}
