﻿namespace BrakesPromotions.Common.Controllers
{
    // Namespaces
    using BLL;
    using Controllers.Base;
    using DevTrends.MvcDonutCaching;
    using Models;
    using Models.Generated;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Utilities;

    public class HomepageController : SurfaceRenderMvcController
    {
        [UmbracoDonutOutputCache(CacheProfile = "LongUserPageCache",
            Options = OutputCacheOptions.NoCacheLookupForPosts & OutputCacheOptions.ReplaceDonutsInChildActions,
            Order = 100)]
        public ActionResult SummerCampaignHome()
        {
            var model = ModelLogic.CreateMasterModel() as MasterModel<Homepage>;

            if (Members.IsLoggedIn() && !model.Campaign.IsAuthorised)
                return Redirect("/register/");

            model.Content.Links = new List<System.Tuple<string, bool>>();

            if (model.Campaign.Accounts != null && model.Campaign.Accounts.Any())
            {

                if (model.Campaign.ActiveAccount.Brand == "Brakes")
                {
                    model.Content.Links.Add(new System.Tuple<string, bool>("https://www.brake.co.uk/pimms", true));

                    if (model.Campaign.ActiveAccount.NectarCustomer)
                    {
                        model.Content.Links.Add(new System.Tuple<string, bool>("/bonus-nectar-points/", false));
                    }
                    else
                    {
                        model.Content.Links.Add(new System.Tuple<string, bool>("https://www.brake.co.uk/3-ways-to-help-you-save/", true));
                    }
                    model.Content.Links.Add(new System.Tuple<string, bool>("/recipes/", false));
                    model.Content.Links.Add(new System.Tuple<string, bool>("/grow-profits/", false));
                }
                else
                {
                    model.Content.Links.Add(new System.Tuple<string, bool>("https://www.woodward-foodservice.com/pimms", true));

                    if (model.Campaign.ActiveAccount.NectarCustomer)
                    {
                        model.Content.Links.Add(new System.Tuple<string, bool>("/bonus-nectar-points/", false));
                    }
                    else
                    {
                        model.Content.Links.Add(new System.Tuple<string, bool>("https://www.woodward-foodservice.com/promotions/3-ways-to-help-you-save/", true));
                    }
                    model.Content.Links.Add(new System.Tuple<string, bool>("/recipes/", false));
                    model.Content.Links.Add(new System.Tuple<string, bool>("/grow-profits/", false));
                }
            }

            return CurrentTemplate(model);
        }
    }
}
