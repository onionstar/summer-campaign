﻿using BrakesPromotions.Common.Utilities;
using DevTrends.MvcDonutCaching;

namespace BrakesPromotions.Common.Controllers
{
    using BrakesPromotions.Common.Helpers;
    // Namespaces
    using Controllers.Base;
    using Manager.Services;
    using Models.Custom;
    using Models.Requests;
    using PhoneNumbers;
    using SendGrid.Helpers.Mail;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;
    using Twilio;
    using Twilio.Converters;
    using Twilio.Rest.Api.V2010.Account;
    using Twilio.Rest.Lookups.V1;
    using Umbraco.Core.Logging;

    public class MembersController : SurfaceRenderMvcController
    {
        private static bool TestMode => bool.Parse(ConfigurationManager.AppSettings["Visarc.Testmode"]);
        private static CampaignService campaignService = new CampaignService();
        private static MemberService memberService = new MemberService();
        private static BrandService brandService = new BrandService();
        private static GiftService giftService = new GiftService();

        [HttpPost]
        public ActionResult HandleRegister(RegisterAccount model)
        {
            if (ModelState.IsValid)
            {
                if (!model.AgreePrivacyPolicy)
                {
                    // y u no agree to privacy policy?
                    ModelState.AddModelError("AgreePrivacyPolicy", "You must agree to the privacy policy");
                }

                if (!model.AgreeTerms)
                {
                    // y u no agree terms?
                    ModelState.AddModelError("AgreeTerms", "You must agree to the terms and conditions");
                }

                if (Services.MemberService.GetByEmail(model.EmailAddress) != null)
                {
                    // account exists
                    ModelState.AddModelError("EmailAddress", "Email address is already registered");
                }

                if (!model.MobileNumber.StartsWith("07"))
                {
                    ModelState.AddModelError("MobileNumber", "Please enter a valid mobile number");
                }

                if (model.EmailAddress != model.VerifiedEmailAddress)
                {
                    ModelState.AddModelError("VerifiedEmailAddress", "Email address does not match");
                }

                if (model.Password.Length < 8)
                {
                    ModelState.AddModelError("Password", "Password must be at least 8 characters");
                }

                if (model.Password != model.VerifiedPassword)
                {
                    ModelState.AddModelError("VerifiedPassword", "Password does not match");
                }

                var vpmMember = memberService.GetByClaimCode(model.ClaimCode, model.CampaignGuid);

                if (vpmMember == null)
                {
                    // campaign member account does not exists
                    ModelState.AddModelError("ClaimCode", "Invalid claim code");
                }
                else if (vpmMember.UmbracoMemberGuid != null && vpmMember.UmbracoMemberGuid != Guid.Empty)
                {
                    // campaign member account is already registered
                    ModelState.AddModelError("ClaimCode", "Claim code already in use");
                }

                if (!ModelState.IsValid)
                {
                    ViewData.Model = model;
                    return CurrentUmbracoPage();
                }

                var cmsMember = Services.MemberService.CreateMember(model.EmailAddress, model.EmailAddress, model.EmailAddress, "Member");
                var campaign = campaignService.GetByGuid(model.CampaignGuid);

                cmsMember.IsApproved = true;

                // Generate and send SMS code
                var smsCode = "";

                try
                {
                    smsCode = GetUniqueKey(4);


                    // Check is the SMS Code property exists and set the value
                    if (cmsMember.HasProperty("smsCode"))
                        cmsMember.SetValue("smsCode", smsCode);

                    // Check is the SMS Expiry property exists and set the value
                    if (cmsMember.HasProperty("smsExpiry"))
                        cmsMember.SetValue("smsExpiry", DateTime.UtcNow.AddHours(6));
                }
                catch (Exception ex)
                {
                    LogHelper.Error<MembersController>("Error generating SMS code", ex);
                }

                try
                {
                    // Save CMS member
                    Services.MemberService.Save(cmsMember);

                    // Save password
                    Services.MemberService.SavePassword(cmsMember, model.Password);

                    // Assign to group
                    Services.MemberService.AssignRole(cmsMember.Id, campaign.Name);

                    string[] newsContactOptions = (Request.Form["LatestNewsContactPref"] ?? "").Split(new char[] { ',' });

                    model.OffersEmail = newsContactOptions.Any(x => x.ToLower().Contains("email"));
                    model.OffersTelephone = newsContactOptions.Any(x => x.ToLower().Contains("phone"));
                    model.OffersMobile = newsContactOptions.Any(x => x.ToLower().Contains("sms"));
                    model.OffersPost = newsContactOptions.Any(x => x.ToLower().Contains("post"));

                    // Update campaign member
                    UpdateMember(vpmMember, model, cmsMember.Key);

                    // Send SMS to registered mobile number
                    if (!string.IsNullOrWhiteSpace(smsCode))
                    {
                        SendSMS(model.MobileNumber.Replace(" ", "").Trim(), brandService.GetByGuid(vpmMember.BrandGuid).Name, smsCode);
                    }

                    // Do login and complete part one of registration
                    Members.Login(model.EmailAddress, model.Password);

                    return RedirectToCurrentUmbracoPage();
                }
                catch (Exception ex)
                {
                    LogHelper.Error<MembersController>("Error creating member", ex);
                    return CurrentUmbracoPage();
                }
            }

            return CurrentUmbracoPage();

        }

        [HttpPost]
        public ActionResult HandleLogout()
        {
            if (Members.IsLoggedIn())
            {
                FormsAuthentication.SignOut();
                Session.Clear();
                Session.Abandon();

                HttpCookie rFormsCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "")
                {
                    Expires = DateTime.Now.AddYears(-1)
                };
                Response.Cookies.Add(rFormsCookie);

                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetNoStore();
            }

            return Redirect("/");
        }

        [HttpPost]
        public ActionResult HandleForgottenPassword(ResetAccount model)
        {
            if (Members.IsLoggedIn())
            {
                TempData["resetStatus"] = "You are already logged in";
                return RedirectToCurrentUmbracoUrl();
            }

            var emailAddress = model.EmailAddress.Trim();
            var cmsMember = Services.MemberService.GetByEmail(emailAddress);

            if (cmsMember != null && cmsMember.IsApproved)
            {
                Umbraco.DisableCache();

                var fromValues = ConfigurationManager.AppSettings["Visarc.EmailFrom"].Split(',').ToList();
                var from = new EmailAddress(fromValues[0], fromValues[1]);

                var toAddress = new EmailAddress(emailAddress, emailAddress.Split('@')[0]);
                if (Request.Url != null)
                {
                    var domain = Request.Url.GetLeftPart(UriPartial.Authority);

                    string validateGuid = Guid.NewGuid().ToString("N");
                    cmsMember.SetValue("validateGuid", validateGuid);
                    cmsMember.SetValue("validateGuidExpiry", DateTime.Now.AddHours(2));

                    Services.MemberService.Save(cmsMember);

                    var recoveryUrl = $"{domain}/reset-password/?email={emailAddress}&validate={validateGuid}";
                    var recoveryEmailModel = new AccountRecoveryEmail(recoveryUrl, domain);

                    var recoveryEmail = ViewRenderer.RenderPartialView("Email/RecoveryEmail", recoveryEmailModel);
                    var subjectLine = TestMode ? "Spring into Summer Password Recovery Test" : "Spring into Summer Password Recovery";

                    if (!CommunicationsHelper.SendEmail(recoveryEmail, from, toAddress, null, subjectLine))
                    {
                        LogHelper.Error<MembersController>("Unable to send customer zone recovery email", null);
                    }
                }
            }
            else
            {
                Umbraco.DisableCache();

                var fromValues = ConfigurationManager.AppSettings["Visarc.EmailFrom"].Split(',').ToList();
                var from = new EmailAddress(fromValues[0], fromValues[1]);

                var toAddress = new EmailAddress(emailAddress, emailAddress.Split('@')[0]);
                if (Request.Url != null)
                {
                    var domain = Request.Url.GetLeftPart(UriPartial.Authority);

                    var recoveryEmailModel = new AccountRecoveryEmail(domain);

                    var recoveryEmail = ViewRenderer.RenderPartialView("Email/BlankRecoveryEmail", recoveryEmailModel);
                    var subjectLine = TestMode ? "Spring into Summer Password Recovery Test" : "Spring into Summer Password Recovery";

                    if (!CommunicationsHelper.SendEmail(recoveryEmail, from, toAddress, null, subjectLine))
                    {
                        LogHelper.Error<MembersController>("Unable to send customer zone recovery email", null);
                    }
                }
            }

            return Redirect("/forgotten-password/");

        }

        [HttpPost]
        public ActionResult HandleValidate(ValidateAccount model)
        {
            var cmsMember = Services.MemberService.GetByKey(model.AccountGuid);

            if (cmsMember.HasProperty("authorised") && cmsMember.GetValue<bool>("authorised"))
            {
                // Account is already authorised, bugger off
                return CurrentUmbracoPage();
            }

            if (!cmsMember.HasProperty("smsCode") || cmsMember.GetValue<string>("smsCode") != model.ValidateCode)
            {
                // Bad validate code
                ModelState.AddModelError("ValidateCode", "Your validation code was invalid.");
                return CurrentUmbracoPage();
            }

            if (!cmsMember.HasProperty("smsExpiry") || cmsMember.GetValue<DateTime>("smsExpiry") < DateTime.UtcNow)
            {
                // Validate code expired
                ModelState.AddModelError("ValidateCode", "Your validation code has expired");
                return CurrentUmbracoPage();
            }

            // Everything is good, authorise the account and back to page
            cmsMember.SetValue("smsCode", "");
            cmsMember.SetValue("smsExpiry", "");
            cmsMember.SetValue("authorised", true);

            // Save CMS member
            Services.MemberService.Save(cmsMember);

            return CurrentUmbracoPage();
        }

        [ChildActionOnly]
        public ActionResult ResendCode(ValidateAccount model)
        {
            var resent = false;
            if (!string.IsNullOrWhiteSpace(Request.QueryString["resend"]))
            {
                var cmsMember = Services.MemberService.GetByKey(model.AccountGuid);
                var smsCode = GetUniqueKey(4);

                // Check is the SMS Code property exists and set the value
                if (cmsMember.HasProperty("smsCode"))
                    cmsMember.SetValue("smsCode", smsCode);

                // Check is the SMS Expiry property exists and set the value
                if (cmsMember.HasProperty("smsExpiry"))
                    cmsMember.SetValue("smsExpiry", DateTime.UtcNow.AddHours(6));

                // Save CMS member
                Services.MemberService.Save(cmsMember);

                var vpmMember = memberService.GetByUmbracoMember(model.AccountGuid, model.CampaignGuid).FirstOrDefault();

                if (vpmMember?.Mobile != null)
                {
                    // Send SMS to registered mobile number
                    SendSMS(vpmMember.Mobile?.Replace(" ", "").Trim(), brandService.GetByGuid(vpmMember.BrandGuid).Name, smsCode);
                    resent = true;
                }
                else
                {
                    LogHelper.Warn<MembersController>($"User {cmsMember.Email} does not have a valid mobile number");
                }
            }

            return PartialView("SummerCampaign/Partials/ResendCode", resent);
        }

        [ChildActionOnly]
        [UmbracoDonutOutputCache(CacheProfile = "NoCache",
            Options = OutputCacheOptions.NoCacheLookupForPosts)]
        public ActionResult ShowAddAccount(Guid accountGuid)
        {
            var model = new NewAccount { MemberGuid = accountGuid };

            return PartialView("SummerCampaign/Partials/AddAccount", model);
        }

        [ChildActionOnly]
        [UmbracoDonutOutputCache(CacheProfile = "NoCache",
            Options = OutputCacheOptions.NoCacheLookupForPosts)]
        public ActionResult ResetPassword(Guid CampaignGuid)
        {
            var model = new ResetAccount { CampaignGuid = CampaignGuid };

            return PartialView("SummerCampaign/Partials/ResetPassword", model);
        }

        [ChildActionOnly]
        [UmbracoDonutOutputCache(CacheProfile = "NoCache",
            Options = OutputCacheOptions.NoCacheLookupForPosts)]
        public ActionResult ShowSwitchAccounts(Guid UmbracoMemberGuid, Guid CampaignGuid)
        {
            var accounts = memberService.GetByUmbracoMember(UmbracoMemberGuid, CampaignGuid);

            var model = new List<SwitchAccount>();

            foreach (var account in accounts)
            {
                model.Add(new SwitchAccount
                {
                    AccountGuid = account.MemberGuid,
                    BusinessName = account.BusinessName,
                    ClaimCode = account.ClaimCode
                });
            }

            return PartialView("SummerCampaign/Partials/SwitchAccount", model);
        }

        [ChildActionOnly]
        [UmbracoDonutOutputCache(CacheProfile = "NoCache",
            Options = OutputCacheOptions.NoCacheLookupForPosts)]
        public ActionResult ShowChangePassword(Guid CampaignGuid)
        {
            var model = new NewPassword
            {
                CampaignGuid = CampaignGuid,
                Email = HttpContext.Request.QueryString["email"] ?? string.Empty,
                ValidateGUID = HttpContext.Request.QueryString["validate"] ?? string.Empty
            };

            var member = Services.MemberService.GetByEmail(model.Email);

            if (member != null)
            {
                string resetPasswordGuid = member.GetValue<string>("validateGUID");
                DateTime resetPasswordGuidExpiry = member.GetValue<DateTime>("validateGUIDExpiry");

                if (model.ValidateGUID != String.Empty && resetPasswordGuid == model.ValidateGUID && DateTime.Now < resetPasswordGuidExpiry && model.Password == model.VerifiedPassword)
                {
                    model.Expired = false;
                    return PartialView("SummerCampaign/Partials/ChangePassword", model);
                }
                else
                {
                    model.Expired = true;
                    return PartialView("SummerCampaign/Partials/ChangePassword", model);
                }
            }
            else
            {
                model.Expired = true;
                return PartialView("SummerCampaign/Partials/ChangePassword", model);
            }
        }

        [HttpPost]
        public ActionResult HandleChangeForgottenPassword(NewPassword model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();
            if (model.Expired)
                return CurrentUmbracoPage();

            if (model.Password.Length < 8)
            {
                ModelState.AddModelError("Password", "Password must be at least 8 characters");
            }

            if (model.Password != model.VerifiedPassword)
            {
                ModelState.AddModelError("VerifiedPassword", "Password does not match");
            }

            var member = Services.MemberService.GetByEmail(model.Email);
            if (member != null)
            {
                string resetPasswordGuid = member.GetValue<string>("validateGUID");
                DateTime resetPasswordGuidExpiry = member.GetValue<DateTime>("validateGUIDExpiry");

                if (model.ValidateGUID != String.Empty && resetPasswordGuid == model.ValidateGUID && DateTime.UtcNow < resetPasswordGuidExpiry && model.Password == model.VerifiedPassword)
                {
                    member.IsLockedOut = false;  // if the user has tried repeatedly they might have locked their account
                    member.SetValue("validateGUIDExpiry", DateTime.UtcNow.AddHours(-1));

                    // remember to save
                    Services.MemberService.Save(member);

                    // save their password
                    Services.MemberService.SavePassword(member, model.Password);

                    return Redirect("/");
                }
            }
            return CurrentUmbracoPage();

        }

        [HttpPost]
        public ActionResult HandleNewAccount(NewAccount model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            var currentAccount = memberService.GetByMemberGuid(model.MemberGuid);

            if (currentAccount == null)
            {
                ModelState.AddModelError("MemberGuid", "Something went wrong...");
                return CurrentUmbracoPage();
            }

            var newLinkedAccount = memberService.GetByClaimCode(model.ClaimCode, currentAccount.CampaignGuid);

            if (newLinkedAccount == null)
            {
                // campaign member account does not exists
                // Claim code invalid
                ModelState.AddModelError("ClaimCode", "Claim code is invalid");
            }
            else if (newLinkedAccount.UmbracoMemberGuid != null && newLinkedAccount.UmbracoMemberGuid != Guid.Empty)
            {
                // campaign member account is already registered
                ModelState.AddModelError("ClaimCode", "Account is already registered");
            }

            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            newLinkedAccount.UmbracoMemberGuid = currentAccount.UmbracoMemberGuid;
            newLinkedAccount.Email = currentAccount.Email;
            newLinkedAccount.Updated = DateTime.UtcNow;

            // copy GDPR preferences from current account
            newLinkedAccount.GdprSalesContact = currentAccount.GdprSalesContact;
            newLinkedAccount.SalesEmail = currentAccount.SalesEmail;
            newLinkedAccount.SalesTelephone = currentAccount.SalesTelephone;
            newLinkedAccount.SalesMobile = currentAccount.SalesMobile;
            newLinkedAccount.GdprOffers = currentAccount.GdprOffers;
            newLinkedAccount.OffersEmail = currentAccount.OffersEmail;
            newLinkedAccount.OffersTelephone = currentAccount.OffersTelephone;
            newLinkedAccount.OffersMobile = currentAccount.OffersMobile;
            newLinkedAccount.OffersPost = currentAccount.OffersPost;

            newLinkedAccount.AgreeTerms = currentAccount.AgreeTerms;
            newLinkedAccount.AgreePrivacyPolicy = currentAccount.AgreePrivacyPolicy;

            memberService.Update(newLinkedAccount, currentAccount.UmbracoMemberGuid.Value);

            if (model.Switch)
            {
                Session["activeAccount"] = newLinkedAccount.MemberGuid;
            }
            else
            {
                Session["activeAccount"] = model.MemberGuid;
            }
            return Redirect("/");
        }

        [HttpPost]
        public ActionResult ChangeAccount(Guid id)
        {
            Session["activeAccount"] = id;
            return Redirect("/");
        }

        [HttpPost]
        public ActionResult HandleClaim(ClaimForm model)
        {
            var currentAccount = memberService.GetByMemberGuid(model.MemberGuid);
            var currentCampaign = campaignService.GetByGuid(currentAccount.CampaignGuid);

            if (currentAccount.ClaimedGiftGuid != Guid.Empty)
            {
                // You have already claimed
                return CurrentUmbracoPage();
            }

            if (currentAccount.UmbracoMemberGuid == Guid.Empty)
            {
                // You have not yet registered
                return CurrentUmbracoPage();
            }

            if (currentCampaign.ClaimEndDate < DateTime.UtcNow || currentCampaign.ClaimStartDate > DateTime.UtcNow)
            {
                // Claim form not yet open
                return CurrentUmbracoPage();
            }

            currentAccount.ClaimedDate = DateTime.UtcNow;
            currentAccount.ClaimedGiftGuid = model.ClaimedGiftGuid;
            currentAccount.ClaimedGiftTierGuid = giftService.GetByGiftGuid(model.ClaimedGiftGuid).GiftTierGuid;

            currentAccount.FirstName = model.FirstName;
            currentAccount.LastName = model.LastName;
            currentAccount.Address1 = model.Address1;
            currentAccount.Address2 = model.Address2;
            currentAccount.Address3 = model.Address3;
            currentAccount.Town = model.Town;
            currentAccount.County = model.County;
            currentAccount.Postcode = model.Postcode;
            currentAccount.NectarCardNumber = model.NectarCardNumber;

            currentAccount.GdprSalesContact = model.SalesContact;

            currentAccount.GdprOffers = model.OffersContact;

            currentAccount.AgreeTerms = model.AgreeTerms;
            currentAccount.AgreePrivacyPolicy = model.AgreePrivacyPolicy;

            memberService.Update(currentAccount, Guid.Empty);

            //send email

            // do log!

            return Redirect("/");

        }

        private Manager.Models.Rdbms.Member UpdateMember(Manager.Models.Rdbms.Member vpmMember, RegisterAccount model, Guid UmbracoMemberGuid)
        {
            try
            {
                vpmMember.UmbracoMemberGuid = UmbracoMemberGuid;
                vpmMember.Updated = DateTime.UtcNow;
                vpmMember.Mobile = model.MobileNumber;
                vpmMember.Email = model.EmailAddress;
                vpmMember.GdprSalesContact = model.SalesContact;
                vpmMember.SalesEmail = model.SalesByEmail;
                vpmMember.SalesTelephone = model.SalesByTelephone;
                vpmMember.SalesMobile = model.SalesByMobile;
                vpmMember.GdprOffers = model.OffersContact;
                //vpmMember.OffersEmail = model.LatestNewsContactPref.Contains("Via Email");
                vpmMember.OffersEmail = model.OffersEmail;
                //vpmMember.OffersTelephone = model.LatestNewsContactPref.Contains("Via Phone");
                vpmMember.OffersTelephone = model.OffersTelephone;
                //vpmMember.OffersMobile = model.LatestNewsContactPref.Contains("Via SMS");
                vpmMember.OffersMobile = model.OffersMobile;
                //vpmMember.OffersPost = model.LatestNewsContactPref.Contains("Via Post");
                vpmMember.OffersPost = model.OffersPost;
                vpmMember.AgreeTerms = model.AgreeTerms;
                vpmMember.AgreePrivacyPolicy = model.AgreePrivacyPolicy;
                if (model.GiftChoice != null && model.GiftChoice != Guid.Empty)
                {
                    vpmMember.TypeGuid = model.GiftChoice;
                }
                memberService.Update(vpmMember, Guid.Empty);
            }
            catch (Exception ex)
            {
                LogHelper.Error<MembersController>($"Error updating member {vpmMember.ClaimCode}", ex);
            }
            return vpmMember;
        }

        private static void SendSMS(string toMobile, string brand, string code)
        {
            string accountSid = ConfigurationManager.AppSettings["Twilio.AccountSid"];
            string authToken = ConfigurationManager.AppSettings["Twilio.AuthToken"];
            string fromNumber = ConfigurationManager.AppSettings["Twilio.PhoneNumber"];

            TwilioClient.Init(accountSid, authToken);

            try
            {
                var util = PhoneNumberUtil.GetInstance();
                var number = util.Parse(toMobile, "GB");

                toMobile = util.Format(number, PhoneNumberFormat.E164);
                var to = new Twilio.Types.PhoneNumber(toMobile);
                var message = MessageResource.Create(
                    to,
                    from: "Spring2Summ",
                    body: $"Spring into Summer with {brand}. Your verification code is: {code}");

                var response = message.Sid;
            }
            catch (NumberParseException e)
            {
                LogHelper.Error<MembersController>("Error sending SMS", e);
            }
        }

        private static bool VerifyMobileNumber(string toMobile)
        {
            string accountSid = ConfigurationManager.AppSettings["Twilio.AccountSid"];
            string authToken = ConfigurationManager.AppSettings["Twilio.AuthToken"];

            TwilioClient.Init(accountSid, authToken);

            try
            {
                var util = PhoneNumberUtil.GetInstance();
                var number = util.Parse(toMobile, "GB");

                toMobile = util.Format(number, PhoneNumberFormat.E164);

                var phoneNumber = PhoneNumberResource.Fetch(type: Promoter.ListOfOne(""),
                    pathPhoneNumber: new Twilio.Types.PhoneNumber(toMobile)
        );

            }
            catch (NumberParseException e)
            {
                LogHelper.Error<MembersController>("Mobile number invalid", e);
            }


            return true;
        }

        private static string GetUniqueKey(int maxSize)
        {
            char[] chars = new char[62];
            chars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
    }
}
