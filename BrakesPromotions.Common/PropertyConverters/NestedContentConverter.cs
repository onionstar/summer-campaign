﻿using System;
using System.Collections.Generic;
using System.Linq;
using BrakesPromotions.Common.Helpers;
using Newtonsoft.Json.Linq;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.PropertyEditors.ValueConverters;

namespace BrakesPromotions.Common.PropertyConverters
{
    public class NestedContentManyConverter : NestedContentManyValueConverter
    {
        private readonly PropertyConverterHelper _helper = PropertyConverterHelper.Instance();

        public override object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            var content = base.ConvertDataToSource(propertyType, source, preview);

            try
            {
                var contentTypes = GetContentTypes(propertyType);
                var multi = _helper.CheckPrevalueInt(propertyType, "maxItems") != 1;

                if (content != null && contentTypes != null && content is List<IPublishedContent> list && list.Any())
                {
                    return TypeHelper.Instance.GetGeneratedContentModel(content, multi);
                }
            }
            catch (Exception e)
            {
                LogHelper.Error<NestedContentManyConverter>("Error converting value", e);
            }

            return content;
        }

        public override Type GetPropertyValueType(PublishedPropertyType propertyType)
        {
            var multi = _helper.CheckPrevalueInt(propertyType, "maxItems") != 1;

            // Auto return type
            try
            {
                var contentTypes = GetContentTypes(propertyType);
                if (contentTypes.Count == 1)
                {
                    var docType = contentTypes[0].Value<string>("ncAlias");
                    var type = TypeHelper.Instance.GetDocumentModelType(docType);
                    if (type != null) return TypeHelper.ReturnType(type, multi);
                }
            }
            catch (Exception e)
            {
                LogHelper.Error<NestedContentManyConverter>("Error finding value type", e);
            }

            // Default
            return TypeHelper.ReturnType<IPublishedContent>(multi);
        }

        public JArray GetContentTypes(PublishedPropertyType propertyType)
        {
            try
            {
                var contentTypesRaw = _helper.CheckPrevalueString(propertyType, "contentTypes");
                var contentTypes = JArray.Parse(contentTypesRaw);
                return contentTypes;
            }
            catch (Exception e)
            {
                LogHelper.Error<NestedContentManyConverter>("Error converting value", e);
            }
            return null;
        }
    }
}