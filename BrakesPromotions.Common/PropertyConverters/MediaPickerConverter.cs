﻿using System;
using System.Collections.Generic;
using System.Linq;
using BrakesPromotions.Common.Helpers;
using BrakesPromotions.Common.Models.Generated;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web.PropertyEditors.ValueConverters;

namespace BrakesPromotions.Common.PropertyConverters
{
    using File = Models.Generated.File;

    public class MediaPickerConverter : MediaPickerPropertyConverter, IPropertyValueConverterMeta
    {
        private readonly PropertyConverterHelper _helper = PropertyConverterHelper.Instance();

        public override object ConvertSourceToObject(PublishedPropertyType propertyType, object source, bool preview)
        {
            var converted = base.ConvertSourceToObject(propertyType, source, preview);
            if (converted == null)
            {
                return null;
            }

            var isMultiplePicker = _helper.CheckPrevalueBool(propertyType, "multiPicker");

            if (_helper.CheckPrevalueBool(propertyType, "onlyImages"))
            {
                var image = _helper.ConvertObject<Image>(converted, isMultiplePicker);
                if (image != null)
                    return image;

                // SVG Files
                if (converted is IEnumerable<IPublishedContent> list)
                {
                    return list.Where(x => x.DocumentTypeAlias == "File").Select(x => new Image(x));
                }

                if (converted is IPublishedContent content && content.DocumentTypeAlias == "File")
                {
                    return new Image(content);
                }
            }
            if (_helper.CheckPrevalueBool(propertyType, "disableFolderSelect"))
            {
                return _helper.ConvertObject<File>(converted, isMultiplePicker);
            }

            // return IPublishedContent
            return converted;
        }

        public new Type GetPropertyValueType(PublishedPropertyType propertyType)
        {
            var multi = _helper.CheckPrevalueBool(propertyType, "multiPicker");

            if (_helper.CheckPrevalueBool(propertyType, "onlyImages"))
            {
                return TypeHelper.ReturnType<Image>(multi);
            }
            if (_helper.CheckPrevalueBool(propertyType, "disableFolderSelect"))
            {
                return TypeHelper.ReturnType<File>(multi);
            }

            return TypeHelper.ReturnType<IPublishedContent>(multi);
        }
    }
}