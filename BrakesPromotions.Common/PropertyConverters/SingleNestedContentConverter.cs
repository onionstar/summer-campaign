﻿using System;
using BrakesPromotions.Common.Helpers;
using Newtonsoft.Json.Linq;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.PropertyEditors.ValueConverters;

namespace BrakesPromotions.Common.PropertyConverters
{
    public class NestedContentSingleConverter : NestedContentSingleValueConverter
    {
        private readonly PropertyConverterHelper _helper = PropertyConverterHelper.Instance();
        
        public override object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            var content = base.ConvertDataToSource(propertyType, source, preview);
            
            try
            {
                if ( content is IPublishedContent publishedContent )
                {
                    return TypeHelper.Instance.GetGeneratedContentModel(publishedContent);
                }
            }
            catch (Exception e)
            {
                LogHelper.Error<NestedContentSingleConverter>("Error converting value", e);
            }

            return content;
        }

        public override Type GetPropertyValueType(PublishedPropertyType propertyType)
        {            
            // Auto
            try
            {
                var contentTypesRaw = _helper.CheckPrevalueString(propertyType, "contentTypes");
                var contentTypes = JArray.Parse(contentTypesRaw);
                if ( contentTypes.Count == 1 )
                {
                    var docType = contentTypes[0].Value<string>("ncAlias");
                    var type = TypeHelper.Instance.GetDocumentModelType(docType);
                    if ( type != null ) return type;
                }
            }
            catch ( Exception e )
            {
                LogHelper.Error<NestedContentSingleConverter>("Error finding value type", e);
            }
            
            // Default
            return typeof(IPublishedContent);
        }
    }
}