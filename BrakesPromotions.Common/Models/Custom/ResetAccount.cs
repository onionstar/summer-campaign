﻿using System;

namespace BrakesPromotions.Common.Models.Custom
{
    // Namesapces
    using System.ComponentModel.DataAnnotations;

    public class ResetAccount
    {
        [Display(Name = "Email Address")]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [Required]
        public Guid CampaignGuid { get; set; }
    }
}
