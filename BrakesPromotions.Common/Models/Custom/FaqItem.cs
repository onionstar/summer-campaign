﻿namespace BrakesPromotions.Common.Models.Custom
{
    public class FaqItem
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}