﻿using System;
using System.Collections.Generic;

namespace BrakesPromotions.Common.Models.Custom
{
    public class Member
    {
        public Umbraco.Web.Models.ProfileModel CmsMember { get; set; }
        public Guid ActiveAccountGuid { get; set; }
        public IList<Account> Accounts { get; set; }
    }
}
