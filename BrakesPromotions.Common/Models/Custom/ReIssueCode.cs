﻿using System;

namespace BrakesPromotions.Common.Models.Custom
{
    // Namespaces
    using System.ComponentModel.DataAnnotations;

    public class ReIssueCode
    {
        public Guid CampaignGuid { get; set; }
        public Guid AccountGuid { get; set; }

        [Display(Name = "Mobile Number")]
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string MobileNumber { get; set; }
    }
}
