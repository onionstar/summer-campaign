﻿using System;

namespace BrakesPromotions.Common.Models.Custom
{
    public class SwitchAccount
    {
        public string BusinessName { get; set; }
        public string ClaimCode { get; set; }
        public Guid AccountGuid { get; set; }
    }
}
