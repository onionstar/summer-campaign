﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BrakesPromotions.Common.Models.Custom
{
    public class NewPassword
    {
        [Display(Name = "Password")]
        [Required]
        [DataType(DataType.Password)]
        [StringLength(128, MinimumLength = 8, ErrorMessage = "Password must be at least 8 characters")]
        public string Password { get; set; }

        [Display(Name = "Confirm Password")]
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        [StringLength(128, MinimumLength = 8, ErrorMessage = "Password must be at least 8 characters")]
        public string VerifiedPassword { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string ValidateGUID { get; set; }
        public bool Expired { get; set; }
        public Guid CampaignGuid { get; set; }

        public NewPassword() { }
    }
}
