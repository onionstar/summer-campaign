﻿namespace BrakesPromotions.Common.Models.Custom
{
    // Namespaces
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class GiftTier
    {
        public string Brand { get; set; }
        public Guid TierGuid { get; set; }
        public string Name { get; set; }
        public bool TargetReached { get; set; }
        [DisplayFormat(DataFormatString = "£{0:N}")]
        public decimal TargetGap { get; set; }
        public IList<Gift> Gifts { get; set; }
        public bool IsClaimed { get; set; }
        public bool ClaimOpen { get; set; }
    }
}
