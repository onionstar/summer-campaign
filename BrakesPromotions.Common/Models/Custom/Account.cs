﻿namespace BrakesPromotions.Common.Models.Custom
{
    // Namespaces
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    public class Account
    {
        public Guid AccountGuid { get; set; }
        public Guid? ClaimedGiftTierGuid { get; set; }
        public Guid? ClaimedGiftGuid { get; set; }
        public Guid? TypeGuid { get; set; }
        public Guid UmbracoMemberGuid { get; set; }

        public string BusinessName { get; set; }
        public string ClaimCode { get; set; }
        public string URN { get; set; }
        public string Brand { get; set; }
        public string TradingState { get; set; }
        public string TelesalesNumber { get; set; }

        public int? Tokens { get; set; }
        public int? TradeDiscount { get; set; }

        [DisplayFormat(DataFormatString = "£{0:N}")]
        public decimal SpendBalance { get; set; }
        [DisplayFormat(DataFormatString = "£{0:N}")]
        public decimal DistanceToTarget => Targets.Last() - SpendBalance;
        [DisplayFormat(DataFormatString = "£{0:N}")]
        public List<decimal> Targets { get; set; }
        public List<Promotion> Promotions { get; set; }

        public bool NectarCustomer { get; set; }
        public bool Active { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Updated { get; set; }

        public SpendingGoals SpendingGoal { get; set; }

        public int? NectarPointsBalance { get; set; }
        public int? NectarBonusPoints { get; set; }
        public int? NectarBonusTarget { get; set; }
        public int? NectarBonusDeliveryCount { get; set; }
        public double? NectarBonusMinimum { get; set; }
    }
}
