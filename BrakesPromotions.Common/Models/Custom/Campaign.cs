﻿using System;

namespace BrakesPromotions.Common.Models.Custom
{
    // Namespaces
    using System.Collections.Generic;
    using System.Linq;

    public class Campaign
    {
        public string Name { get; set; }
        public Guid CampaignGuid { get; set; }
        public bool IsLive { get; set; }
        public bool ClaimOpen { get; set; }
        public IList<GiftTier> Tiers { get; set; }
        public IList<Account> Accounts { get; set; }
        public Account ActiveAccount => Accounts.FirstOrDefault(x => x.Active);
        public bool IsAuthorised { get; set; }
    }
}
