﻿using System;

namespace BrakesPromotions.Common.Models.Custom
{
    public class ValidateAccount
    {
        public Guid CampaignGuid { get; set; }
        public Guid AccountGuid { get; set; }
        public string ValidateCode { get; set; }
    }
}
