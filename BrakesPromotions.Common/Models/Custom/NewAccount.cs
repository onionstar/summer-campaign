﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BrakesPromotions.Common.Models.Requests
{
    public class NewAccount
    {
        [Required]
        public Guid MemberGuid { get; set; }

        [Required]
        public string ClaimCode { get; set; }

        public string GiftPref { get; set; }

        public bool Switch { get; set; }
    }
}
