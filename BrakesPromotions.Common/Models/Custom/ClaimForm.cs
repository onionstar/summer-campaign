﻿using BrakesPromotions.Common.Models.Generated;
using BrakesPromotions.Manager.Services;
using System;
using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace BrakesPromotions.Common.Models.Custom
{
    public class ClaimForm
    {
        private static GiftService giftService = new GiftService();
        private static MemberService memberService = new MemberService();
        private static GiftTypeService giftTypeService = new GiftTypeService();
        private static BrandService brandService = new BrandService();
        protected static UmbracoHelper _UmbracoHelper => new UmbracoHelper(UmbracoContext.Current);

        public Guid ClaimedGiftGuid { get; set; }
        public Guid MemberGuid { get; set; }
        public string Brand { get; set; }

        public IList<Gift> Gifts { get; set; }
        public Guid SelectedGift { get; set; }

        public string NectarCardNumber { get; set; }

        public string BusinessName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }

        public string Telephone { get; set; }
        public string Mobile { get; set; }

        public bool? SalesContact { get; set; }
        public bool? SalesEmail { get; set; }
        public bool? SalesTelephone { get; set; }
        public bool? SalesMobile { get; set; }
        public bool? OffersContact { get; set; }
        public bool? OffersEmail { get; set; }
        public bool? OffersTelephone { get; set; }
        public bool? OffersMobile { get; set; }
        public bool? OffersPost { get; set; }
        public bool? AgreeTerms { get; set; }
        public bool? AgreePrivacyPolicy { get; set; }

        public bool IsBusinessOwner { get; set; }
        public bool? HasPermission { get; set; }
        public string BusinessOwnerName { get; set; }

        public ClaimForm(Guid GiftGuid, Guid MemberGuid)
        {
            var selectedGift = giftService.GetByGiftGuid(GiftGuid);
            var giftModels = giftService.GetByGiftTierGuid(selectedGift.GiftTierGuid);
            Brand = brandService.GetByGuid(memberService.GetByMemberGuid(MemberGuid).BrandGuid).Name;

            if (giftModels != null)
            {
                Gifts = new List<Gift>();
                foreach (var gift in giftModels)
                {
                    var giftType = giftTypeService.GetByTypeGuid(gift.TypeGuid);
                    var newGift = new Gift
                    {
                        GiftGuid = gift.GiftGuid,
                        Name = giftService.FullGiftName(gift.GiftGuid, gift.TypeGuid),
                        IsClaimed = gift.GiftGuid == GiftGuid
                    };
                    switch (giftType.Name)
                    {
                        case "Gadget 1":
                            newGift.GiftType = Enums.GiftEnums.GiftType.Gadget;
                            break;
                        case "Gadget 2":
                            newGift.GiftType = Enums.GiftEnums.GiftType.Gadget;
                            break;
                        case "Love2Shop Vouchers":
                            newGift.GiftType = Enums.GiftEnums.GiftType.Love2ShopVoucher;
                            break;
                        case "Nectar Points":
                            newGift.GiftType = Enums.GiftEnums.GiftType.NectarPoints;
                            break;
                        case "Brakes or Woodward Vouchers":
                            newGift.GiftType = Enums.GiftEnums.GiftType.BrandVoucher;
                            break;
                        case "TTS Vouchers":
                            newGift.GiftType = Enums.GiftEnums.GiftType.SchoolVoucher;
                            break;
                    }
                    if (giftType.HasMedia && gift.MediaId != Guid.Empty)
                    {
                        IMedia content = UmbracoContext.Current.Application.Services.MediaService.GetById(gift.MediaId);
                        IPublishedContent media = content == null ? null : _UmbracoHelper.TypedMedia(content.Id);

                        if (media != null) newGift.Image = media as Image;
                    }

                    Gifts.Add(newGift);
                }
            }




            var member = memberService.GetByMemberGuid(MemberGuid);

            NectarCardNumber = member.NectarCardNumber;
            BusinessName = member.BusinessName;
            FirstName = member.FirstName;
            LastName = member.LastName;
            Address1 = member.Address1;
            Address2 = member.Address2;
            Address3 = member.Address3;
            Town = member.Town;
            County = member.County;
            Postcode = member.Postcode;

            Telephone = member.Telephone;
            Mobile = member.Mobile;

            SalesContact = member.GdprSalesContact; ;
            SalesEmail = member.SalesEmail;
            SalesTelephone = member.SalesTelephone;
            SalesMobile = member.SalesMobile;
            OffersContact = member.GdprOffers;
            OffersEmail = member.OffersEmail;
            OffersTelephone = member.OffersTelephone;
            OffersMobile = member.OffersMobile;
            OffersPost = member.OffersPost;

            AgreeTerms = member.AgreeTerms;
            AgreePrivacyPolicy = member.AgreePrivacyPolicy;
        }
    }
}
