﻿using System.ComponentModel.DataAnnotations;

namespace BrakesPromotions.Common.Models.Custom
{
    public class Promotion
    {
        public int ProductListProductId { get; set; }
        public int Code { get; set; }
        public int ProductId { get; set; }
        public string PromoPrice { get; set; }
        public string Prefix { get; set; }
        public string Name { get; set; }
        public string PackSize { get; set; }
        public string MinimumPackSize { get; set; }
        public string ApproxSizeCountPtn { get; set; }
        [DisplayFormat(DataFormatString = "£{0:N}")]
        public string PriceEach { get; set; }
        [DisplayFormat(DataFormatString = "£{0:N}")]
        public string ListPrice { get; set; }
        [DisplayFormat(DataFormatString = "£{0:N}")]
        public string ProductUrl { get; set; }
        public string ProductImageUrl => $"remote.axd/www.brake.co.uk/_Images/{Code}_1.jpg?format=jpg&404=https://www.brake.co.uk/images/no-image.jpg?width=300&quality=65&format=jpg";
        public string OrderOnlineUrl => "https://orderonline.brake.co.uk/";
        public string ShortDescription { get; set; }
        public string SecondaryTitle { get; set; }
    }
}
