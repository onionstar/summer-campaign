﻿using System;

namespace BrakesPromotions.Common.Models.Custom
{
    public class NuPickersDropDownPicker
    {
        public String Key { get; set; }
        public String Label { get; set; }
    }
}
