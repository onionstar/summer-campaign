﻿namespace BrakesPromotions.Common.Models.Custom
{
    // Namespaces
    using Models.Generated;
    using System;
    using Enums;

    public class Gift
    {
        public Guid GiftGuid { get; set; }
        public string Name { get; set; }
        public Image Image { get; set; }
        public bool IsClaimed { get; set; }
        public GiftEnums.GiftType GiftType { get; set; }

    }
}
