﻿using System;

namespace BrakesPromotions.Common.Models.Custom
{
    // Namesapces
    using System.ComponentModel.DataAnnotations;

    public class RegisterAccount
    {
        [Display(Name = "Claim Code")]
        [Required(ErrorMessage = "Claim code cannot be verified, please try again or contact your ASM.")]
        [StringLength(6, ErrorMessage = "Claim code cannot be verified, please try again or contact your ASM.")]
        public string ClaimCode { get; set; }

        [Display(Name = "Email Address")]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [Display(Name = "Confirm Email Address")]
        [Required]
        [DataType(DataType.EmailAddress)]
        [Compare("EmailAddress", ErrorMessage = "Email addresses do not match")]
        public string VerifiedEmailAddress { get; set; }

        [Display(Name = "Password")]
        [Required]
        [DataType(DataType.Password)]
        [StringLength(128, MinimumLength = 8, ErrorMessage = "Password must be at least 8 characters")]
        public string Password { get; set; }

        [Display(Name = "Confirm Password")]
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        [StringLength(128, MinimumLength = 8, ErrorMessage = "Password must be at least 8 characters")]
        public string VerifiedPassword { get; set; }

        [Display(Name = "Mobile Number")]
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string MobileNumber { get; set; }

        public bool SalesContact { get; set; }

        public bool SalesByEmail { get; set; }

        public bool SalesByTelephone { get; set; }

        public bool SalesByMobile { get; set; }

        public bool OffersContact { get; set; }

        public string LatestNewsContactPref { get; set; }

        public bool OffersEmail { get; set; }

        public bool OffersTelephone { get; set; }

        public bool OffersMobile { get; set; }

        public bool OffersPost { get; set; }

        [Required]
        public bool AgreePrivacyPolicy { get; set; }

        [Required]
        public bool AgreeTerms { get; set; }

        [Required]
        public Guid CampaignGuid { get; set; }

        public Guid GiftChoice { get; set; }
    }
}
