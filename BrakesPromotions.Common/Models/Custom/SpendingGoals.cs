﻿namespace BrakesPromotions.Common.Models.Custom
{
    // Namespaces
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class SpendingGoals
    {
        public float CurrentSpendPercentage { get; set; }
        [DisplayFormat(DataFormatString = "£{0:N}")]
        public float FinalGoalAmount { get; set; }
        [DisplayFormat(DataFormatString = "£{0:N}")]
        public float AmountToNextGoal { get; set; }
        public List<SpendingGoal> Goals { get; set; }
    }
    public class SpendingGoal
    {
        public string Label { get; set; }
        [DisplayFormat(DataFormatString = "£{0:N}")]
        public float Amount { get; set; }
        public float Percentage { get; set; }
    }
}
