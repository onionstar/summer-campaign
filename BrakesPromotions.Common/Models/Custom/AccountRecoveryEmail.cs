﻿namespace BrakesPromotions.Common.Models.Custom
{
    public class AccountRecoveryEmail
    {
        public string SiteDomain { get; set; }
        public string PhoneIcon => $"{SiteDomain}/images/icon-email-phone.gif";
        public string Logo => $"{SiteDomain}/images/logo-campaign-summer.png";
        public string RecoveryUrl { get; set; }
        public string BrandName { get; set; }

        public AccountRecoveryEmail(string recoveryUrl, string siteDomain)
        {
            SiteDomain = siteDomain;
            RecoveryUrl = recoveryUrl;
            BrandName = "Spring into Summer";
        }

        public AccountRecoveryEmail(string siteDomain)
        {
            SiteDomain = siteDomain;
            BrandName = "Spring into Summer";
        }
    }
}
