//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace BrakesPromotions.Common.Models.Generated
{
	// Mixin content Type 1071 with alias "loggedOutView"
	/// <summary>Logged out view</summary>
	public partial interface ILoggedOutView : IPublishedContent
	{
		/// <summary>Copy</summary>
		IHtmlString LoggedOutCopy { get; }

		/// <summary>Heading</summary>
		string LoggedOutHeading { get; }

		/// <summary>Sub-Heading</summary>
		string LoggedOutSubHeading { get; }
	}

	/// <summary>Logged out view</summary>
	[PublishedContentModel("loggedOutView")]
	public partial class LoggedOutView : PublishedContentModel, ILoggedOutView
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "loggedOutView";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public LoggedOutView(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<LoggedOutView, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Copy
		///</summary>
		[ImplementPropertyType("loggedOutCopy")]
		public IHtmlString LoggedOutCopy
		{
			get { return GetLoggedOutCopy(this); }
		}

		/// <summary>Static getter for Copy</summary>
		public static IHtmlString GetLoggedOutCopy(ILoggedOutView that) { return that.GetPropertyValue<IHtmlString>("loggedOutCopy"); }

		///<summary>
		/// Heading
		///</summary>
		[ImplementPropertyType("loggedOutHeading")]
		public string LoggedOutHeading
		{
			get { return GetLoggedOutHeading(this); }
		}

		/// <summary>Static getter for Heading</summary>
		public static string GetLoggedOutHeading(ILoggedOutView that) { return that.GetPropertyValue<string>("loggedOutHeading"); }

		///<summary>
		/// Sub-Heading
		///</summary>
		[ImplementPropertyType("loggedOutSubHeading")]
		public string LoggedOutSubHeading
		{
			get { return GetLoggedOutSubHeading(this); }
		}

		/// <summary>Static getter for Sub-Heading</summary>
		public static string GetLoggedOutSubHeading(ILoggedOutView that) { return that.GetPropertyValue<string>("loggedOutSubHeading"); }
	}
}
