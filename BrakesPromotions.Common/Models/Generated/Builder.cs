﻿using Umbraco.ModelsBuilder;
//Don't generate the folder media type, because it only has the Folder Browser property.

[assembly: IgnoreContentType("Folder")]