﻿using BrakesPromotions.Common.Models.Custom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrakesPromotions.Common.Models.Generated
{
    public partial class ClaimForm
    {
        public bool GiftClaimed => Gift != null;
        public Gift Gift { get; set; }

        public IList<Gift> AvailableGifts { get; set; }
        public Guid SelectedGift { get; set; }
    }
}
