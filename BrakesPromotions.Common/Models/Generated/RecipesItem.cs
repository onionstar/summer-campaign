﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrakesPromotions.Common.Models.PartialViewModels;

namespace BrakesPromotions.Common.Models.Generated
{
    public partial class RecipesItem
    {
        private static readonly string[] IngredientsTableHeadings = {"Name", "Quantity"};

        private IEnumerable<string[]> IngredientsTableRows 
            => Ingredients?.Select(x => new []{x.Heading, $"{x.Quantity} {x.Measurement}"})
                ?? Enumerable.Empty<string[]>();

        private Table _ingredientsTable;

        public Table IngredientsTable
            => _ingredientsTable 
                ?? (_ingredientsTable = new Table(
                    "Ingredients",
                    IngredientsTableHeadings, 
                    IngredientsTableRows
                ));
    }
}
