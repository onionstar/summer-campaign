﻿using System;
using System.Collections.Generic;

namespace BrakesPromotions.Common.Models.Generated
{
    public partial class Homepage
    {
        public List<Tuple<string, bool>> Links { get; set; }

    }
}
