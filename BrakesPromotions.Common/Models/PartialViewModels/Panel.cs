﻿using System.Web;
using BrakesPromotions.Common.Models.Generated;

namespace BrakesPromotions.Common.Models.PartialViewModels
{
    public class Panel : HtmlComponent
    {
        public override string ObjectName => "Panel";
        public string Heading { get; set; }
        public Picture Picture { get; set; }
        public IHtmlString Body { get; set; }
        public IHtmlString Cta { get; set; }
        public IHtmlString ExtendedContent { get; set; }

        public static Panel CreateInstance<T>(T obj, IHtmlString cta = null, IHtmlString ec = null)
            where T : ICmGenericContent, ICmGenericFeaturedImage
        {
            return new Panel
            {
                Heading = obj.Heading,
                Picture = new Picture(obj.Image, Picture.CropGroup.ArticlePhoto),
                Body = obj.Body,
                Cta = cta,
                ExtendedContent = ec
            };
        }
    }
}
