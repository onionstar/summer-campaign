﻿using System.Web;

namespace BrakesPromotions.Common.Models.PartialViewModels
{
    public class Modal : HtmlComponent
    {
        public string Id { get; }
        public string Heading { get; }
        public IHtmlString Content { get; }

        public Modal(string id, string heading, IHtmlString content)
        {
            Id = id;
            Heading = heading;
            Content = content;
        }

        public Modal(string heading, IHtmlString content)
            : this(heading.ToLower().Replace(" ", "-"), heading, content)
        {}
    }
}
