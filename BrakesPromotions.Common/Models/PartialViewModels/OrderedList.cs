﻿using System.Collections.Generic;

namespace BrakesPromotions.Common.Models.PartialViewModels
{
    public class OrderedList : HtmlComponent
    {
        public override string ObjectName => "OrderedList";
        public string Heading { get; }
        public IEnumerable<string> Items { get; }

        public OrderedList(string heading, IEnumerable<string> items)
        {
            Heading = heading;
            Items = items;
        }
    }
}
