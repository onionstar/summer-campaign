﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using BrakesPromotions.Common.Models.Generated;

namespace BrakesPromotions.Common.Models.PartialViewModels
{
    public class Picture : HtmlComponent
    {
        public enum CropGroup
        {
            ArticlePhoto,
            RecipePhoto
        };

        public CropGroup Crop { get; }
        public Image Image { get; }
        public IEnumerable<PictureCrop> Crops { get; }
        public bool ObjectFit { get; set; }
        public string Alt { get; set; }

        public Picture(Image image, CropGroup crop)
        {
            Image = image;
            Crop = crop;
            var start = Regex.Replace(crop.ToString(), @"([A-Z])", " $1").Trim();
            Crops = image.UmbracoFile.Crops
                .Where(x => x.Alias.StartsWith(start))
                .Select(x => new PictureCrop(x.Alias))
                .OrderByDescending(x => x.Breakpoint)
                .ToList();
            Alt = image.Name;
        }
    }

    public class PictureCrop
    {
        public static Dictionary<string, int> Breakpoints = new Dictionary<string, int>
        {
            {"Desktop", 1025},
            {"Tablet (L)", 769},
            {"Tablet (S)", 500},
            {"Mobile (L)", 375},
            {"Mobile (S)", 0}
        };

        public string Alias { get; }
        public int Breakpoint { get; }
        public string MediaQuery => $"min-width: {Breakpoint}px";

        public PictureCrop(string alias)
        {
            Alias = alias;
            var bp = alias.Substring(alias.IndexOf("-", StringComparison.Ordinal) + 1).Trim();
            if (Breakpoints.ContainsKey(bp))
            {
                Breakpoint = Breakpoints[bp];
            }
        }
    }
}
