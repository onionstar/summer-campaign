﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BrakesPromotions.Common.Models.PartialViewModels
{
    public class Table : HtmlComponent
    {
        public override string ObjectName { get; set; } = "Table";

        public string Title { get; }
        public IEnumerable<string> Headings { get; }
        public IEnumerable<IEnumerable<string>> Rows { get; }

        public Table(
            string title, 
            IEnumerable<string> headings, 
            IEnumerable<IEnumerable<string>> rows
        )
        {
            Title = title;
            Headings = headings;
            if (rows.Any(row => row.Count() > headings.Count()))
            {
                throw new ArgumentException("Heading column count does not match row column count.");
            }

            Rows = rows;
        }
    }
}
