﻿using System.Collections.Generic;
using System.Text;

namespace BrakesPromotions.Common.Models.PartialViewModels
{
    public class HtmlComponent
    {
        public virtual string ObjectName { get; set; }
        public virtual string ComponentName { get; set; }
        public string ClassName { get; set; }
        public Dictionary<string, string> Attributes { get; set; } 
            = new Dictionary<string, string>();

        public HtmlComponent() {}

        public HtmlComponent(string className = "")
        {
            ClassName = className;
        }

        private string BuildSuitClassName(string component, string element, string modifier)
        {
            var sb = new StringBuilder(component);
            if (element != string.Empty)
            {
                sb.Append($"-{element}");
            }

            if (modifier != string.Empty)
            {
                sb.Append($"--{modifier}");
            }

            return sb.ToString();
        }

        public string GetClassName(string element = "", string modifier = "")
        {
            var sb = new StringBuilder();
            if (
                !string.IsNullOrWhiteSpace(ClassName) 
                && element == string.Empty 
                && modifier == string.Empty
            )
            {
                sb.Append($"{ClassName} ");
            }

            if (!string.IsNullOrWhiteSpace(ComponentName))
            {
                sb.Append($"{BuildSuitClassName(ComponentName, element, modifier)} ");
            }

            if (!string.IsNullOrWhiteSpace(ObjectName))
            {
                sb.Append($"{BuildSuitClassName(ObjectName, element, modifier)} ");
            }

            return sb.ToString().Trim();
        }
    }

    public class HtmlComponent<T> : HtmlComponent
        where T : class
    {
        public T Content { get; }

        public HtmlComponent(T content, string className = "")
            : base(className)
        {
            Content = content;
        }
    }
}
