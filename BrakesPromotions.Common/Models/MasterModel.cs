﻿namespace BrakesPromotions.Common.Models
{
    // Namespaces
    using Models.Custom;
    using Umbraco.Core.Models;
    using Umbraco.Web.Models;

    public interface IMasterModel
    {
        IPublishedContent Content { get; }

        string SeoTitle { get; set; }
        string SeoDescription { get; set; }
        string SeoAuthor { get; set; }
        bool SeoNoIndex { get; set; }
        string TitleSuffix { get; set; }
        string PageTitle { get; set; }
        string GtmCode { get; set; }
        Campaign Campaign { get; set; }
    }

    public class MasterModel<T> : RenderModel<T>, IMasterModel
        where T : class, IPublishedContent
    {
        public MasterModel(T content) : base(content) { }

        public string SeoTitle { get; set; }
        public string SeoDescription { get; set; }
        public string SeoAuthor { get; set; }
        public bool SeoNoIndex { get; set; }
        public string TitleSuffix { get; set; }
        public string PageTitle { get; set; }
        public string GtmCode { get; set; }
        public Campaign Campaign { get; set; }
    }
}
