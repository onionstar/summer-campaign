﻿using System;

namespace BrakesPromotions.Common.Utilities
{
    // Namespaces
    using System.Web;
    using Umbraco.Web;

    public static class ExtensionMethods
    {
        private static UmbracoHelper Umbraco => new UmbracoHelper(UmbracoContext.Current);


        /// <summary>
        /// Disable the cache of a page.
        /// </summary>
        public static void DisableCache(this UmbracoHelper umbraco)
        {
            HttpContext.Current.Response.AddHeader("pragma", "no-cache");
            HttpContext.Current.Response.AddHeader("cache-control", "private");
            HttpContext.Current.Response.CacheControl = "no-cache";
            HttpContext.Current.Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetNoStore();
        }
    }
}
