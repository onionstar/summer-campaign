﻿namespace BrakesPromotions.Common.Utilities
{
    // Namespaces
    using Umbraco.Core;
    using Umbraco.Core.Models;
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Services;
    using Umbraco.Web;
    using Umbraco.Web.PublishedCache;

    public class BaseClass
    {
        protected static UmbracoDatabase Database => ApplicationContext.Current.DatabaseContext.Database;

        protected static DatabaseContext DatabaseContext => ApplicationContext.Current.DatabaseContext;

        protected static ServiceContext Services => ApplicationContext.Current.Services;

        protected static IPublishedContent CurrentPage => UmbracoContext.Current.PublishedContentRequest.PublishedContent;

        protected static UmbracoHelper Umbraco => new UmbracoHelper(UmbracoContext.Current);

        protected static ContextualPublishedContentCache ContentCache => UmbracoContext.Current.ContentCache;
    }
}
