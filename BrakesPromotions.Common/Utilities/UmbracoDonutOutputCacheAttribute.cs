﻿using System.Configuration;
using System.Web.UI;

namespace BrakesPromotions.Common.Utilities
{
    // Namespaces
    using DevTrends.MvcDonutCaching;
    using System.Web.Mvc;
    using Umbraco.Web;

    public class UmbracoDonutOutputCacheAttribute : DonutOutputCacheAttribute
    {
        private OutputCacheLocation? _originalLocation;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var disableCache = !CacheSettingsManager.IsCachingEnabledGlobally ||
                               UmbracoContext.Current.InPreviewMode ||
                               UmbracoContext.Current.HttpContext.Request.QueryString["dtgePreview"] == "1";

            // source: https://stackoverflow.com/questions/30501337/mvc-donut-caching-disable-caching-programmaticallys
            if (disableCache)
            {
                _originalLocation = _originalLocation ?? Location;
                Location = OutputCacheLocation.None;
                Duration = 0;
            }
            else
            {
                Location = _originalLocation ?? Location;
                Duration = -1;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
