﻿namespace BrakesPromotions.Common.Utilities
{
    // Namespaces
    using Helpers;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Umbraco.Web;

    public class Global : UmbracoApplication
    {
        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            var result = base.GetVaryByCustomString(context, custom);
            if (string.IsNullOrEmpty(custom))
            {
                return result;
            }
            if (UmbracoContext.Current.IsFrontEndUmbracoRequest)
            {
                List<string> keys = custom.Split(new string[] { ";" }, StringSplitOptions.None).ToList();
                if (keys.Contains("url"))
                {
                    result += $"pageid={UmbracoContext.Current.PageId};";
                    result += $"url={context.Request.Url};";
                }
                if (keys.Contains("user"))
                {
                    var memberShipHelper = new Umbraco.Web.Security.MembershipHelper(UmbracoContext.Current);
                    if (memberShipHelper.IsLoggedIn())
                    {
                        result += $"userid={memberShipHelper.CurrentUserName};";
                        if (HttpContext.Current.Session != null && HttpContext.Current.Session["activeAccount"] != null)
                        {
                            var activeAccount = GuidHelper.GetString((Guid)HttpContext.Current.Session["activeAccount"]);
                            if (!string.IsNullOrWhiteSpace(activeAccount))
                            {
                                result += $"activeAccount={activeAccount};";
                            }
                        }
                    }
                }
            }
            return result;
        }
    }
}
