﻿namespace BrakesPromotions.Common.Actions
{
    // Namespaces
    using System.Web.Mvc;
    using Umbraco.Web;

    public class BaseActionFilterAttribute : ActionFilterAttribute
    {
        public BaseActionFilterAttribute() => Order = 0;

        public BaseActionFilterAttribute(int order) => Order = order;

        protected static UmbracoHelper Umbraco => new UmbracoHelper(UmbracoContext.Current);
    }
}
