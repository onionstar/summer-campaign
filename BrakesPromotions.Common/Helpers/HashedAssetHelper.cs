﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;

namespace BrakesPromotions.Common.Helpers
{
    public static class HashedAssetHelper
    {
        private const string ConfigPath = "~/Config/webpack-assets.json";
        private static string AbsolutePath => HttpContext.Current.Server.MapPath(ConfigPath);

        private static DateTime _updatedAt;

        private static JObject _config;
        public static JObject Config
        {
            get
            {
                ReadFile();

                return _config;
            }
        }

        private static void ReadFile()
        {
            if (!System.IO.File.Exists(AbsolutePath))
                return;

            var lastEditAt = System.IO.File.GetLastWriteTimeUtc(AbsolutePath);
            if (lastEditAt <= _updatedAt) 
                return;

            var text = System.IO.File.ReadAllText(AbsolutePath);
            if (string.IsNullOrWhiteSpace(text))
                return;

            _config = JObject.Parse(text);
            _updatedAt = lastEditAt;
        }

        public static JToken GetPaths(string file)
        {
            return Config?.GetValue(file);
        }

        public static string GetJsPath(string file)
        {
            return GetPaths($"scripts/{file}")?["js"]?.ToString() ?? "";
        }

        public static string GetCssPath(string file)
        {
            return GetPaths($"css/{file}")?["css"]?.ToString() ?? "";
        }

        public static string[] GetAllScripts()
        {
            var props = Config.Properties()
                .Where(x => x.Name.StartsWith("scripts/"))
                .Select(x => x.Value["js"].ToString())
                .ToArray();

            return props;
        }

        public static string[] GetAllStyles()
        {
            var props = Config.Properties()
                .Where(x => x.Name.StartsWith("css/"))
                .Select(x => x.Value["css"].ToString())
                .ToArray();

            return props;
        }
    }
}
