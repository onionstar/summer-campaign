﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Umbraco.Core.Configuration;
using Umbraco.Core.Models;
using Umbraco.ModelsBuilder.Configuration;

namespace BrakesPromotions.Common.Helpers
{
    public class TypeHelper
    {
        private static volatile TypeHelper _instance;
        private static readonly object Lock = new object();
        private readonly Dictionary<string, Type> _mappedTypes = new Dictionary<string, Type>();
        public const string ModelsNamespace = "CountryChoice.Extensions.Models.Generated";

        public static TypeHelper Instance
        {
            get
            {
                lock (Lock)
                {
                    return _instance ?? (_instance = new TypeHelper());
                }
            }
        }

        public object GetGeneratedContentModel(IPublishedContent content)
        {
            if (content != null)
            {
                var type = GetDocumentModelType(content.DocumentTypeAlias);
                if ( type != null )
                {
                    return Activator.CreateInstance(type, content);
                }
            }

            return content;
        }

        public Type GetDocumentModelType(string documentTypeAlias)
        {
            var ns = UmbracoConfig.For.ModelsBuilder().ModelsNamespace;

            return _mappedTypes.ContainsKey(documentTypeAlias) ? 
                _mappedTypes[documentTypeAlias] : 
                GetTypesInNamespace(Assembly.GetExecutingAssembly(), ns).FirstOrDefault(x =>
                {
                    
                    var field = x.GetField("ModelTypeAlias");
                    if ( field == null || documentTypeAlias != field.GetValue(null).ToString() ) return false;
                    _mappedTypes.Add(documentTypeAlias, x);
                    return true;
                });
        }

        public object GetGeneratedContentModel(object content, bool multi)
        {
            if ( content is List<IPublishedContent> list && list.Any() )
            {
                return multi
                    ? GetGeneratedConteModelList(list)
                    : GetGeneratedContentModel(list.FirstOrDefault());
            }

            return content is IPublishedContent publishedContent ? GetGeneratedContentModel(publishedContent) : content;
        }
        
        private object GetGeneratedConteModelList(List<IPublishedContent> contents)
        {
            if ( contents != null && contents.Any() )
            {
                var type = GetDocumentModelType(contents.First().DocumentTypeAlias);
                return CastListAsType(contents, type);
            }
            return contents;
        }
        
        public object CastListAsType(List<IPublishedContent> contents, Type type)
        {
            if ( contents != null && contents.Any() )
            {
                var list = contents.Select(GetGeneratedContentModel);
                var method = GetType().GetMethod("Cast")?.MakeGenericMethod(type);
                return method?.Invoke(this, new object[] { list });
            }
            return contents;
        }

        public bool IsPartialViewModel(string documentTypeAlias)
        {
            var type = GetDocumentModelType(documentTypeAlias);
            var interfaces = type.FindInterfaces((t, criteria) =>  t.Name == (string) criteria, "IPartialViewModel");
            return interfaces.Length > 0;
        }
        
        public IEnumerable<T> Cast<T>(IEnumerable<object> objects)
        {
            return objects.Cast<T>().ToList();
        }
        
        private IEnumerable<Type> GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return assembly.GetTypes().Where(t => string.Equals(t.Namespace, nameSpace, StringComparison.Ordinal));
        }
        
        public static Type ReturnType(Type t, bool multi)
        {
            return multi ? typeof(IEnumerable<>).MakeGenericType(t) : t;
        }

        public static Type ReturnType<T>(bool multi)
        {
            return multi ? typeof(IEnumerable<T>) : typeof(T);
        }
    }
}