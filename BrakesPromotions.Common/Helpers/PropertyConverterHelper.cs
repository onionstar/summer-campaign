﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace BrakesPromotions.Common.Helpers
{
    // Namespaces
    public class PropertyConverterHelper
    {
        private static PropertyConverterHelper _instance;
        private static readonly object Padlock = new object();
        private readonly IDataTypeService _dataTypeService;
        private readonly UmbracoHelper _umbracoHelper;
        private static readonly ConcurrentDictionary<int, IDictionary<string, string>> Storages = new ConcurrentDictionary<int, IDictionary<string, string>>();

        protected PropertyConverterHelper(UmbracoContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            var appContext = ApplicationContext.Current;
            _dataTypeService = appContext.Services.DataTypeService;
            _umbracoHelper = new UmbracoHelper(context);
        }

        public static PropertyConverterHelper Instance(UmbracoContext context = null)
        {
            lock (Padlock)
            {
                return _instance ?? (_instance = new PropertyConverterHelper(context ?? UmbracoContext.Current));
            }
        }

        public object CheckPrevalue(PublishedPropertyType propertyType, string key)
        {
            var idKey = propertyType.DataTypeId;

            var prevalues = Storages.GetOrAdd(idKey, id =>
            {
                var preVals = _dataTypeService.GetPreValuesCollectionByDataTypeId(id).PreValuesAsDictionary;

                if (!preVals.Any())
                {
                    // In some odd cases, the pre-values in the db won't exist but their default pre-values contain this key so check there
                    var propertyEditor = PropertyEditorResolver.Current.GetByAlias(propertyType.PropertyEditorAlias);
                    preVals = propertyEditor.DefaultPreValues.ToDictionary(k => k.Key, v => ((PreValue)v.Value));
                }

                return preVals.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.Value ?? "");
            });

            return prevalues.ContainsKey(key) ? prevalues[key] : "";
        }

        public bool CheckPrevalueBool(PublishedPropertyType propertyType, string key)
        {
            return CheckPrevalue(propertyType, key).TryConvertTo<bool>().Result;
        }

        public string CheckPrevalueString(PublishedPropertyType propertyType, string key)
        {
            return CheckPrevalue(propertyType, key).ToString();
        }

        public int CheckPrevalueInt(PublishedPropertyType propertyType, string key)
        {
            return CheckPrevalue(propertyType, key).TryConvertTo<int>().Result;
        }

        public Udi CheckPrevalueStartNodeUdi(PublishedPropertyType propertyType)
        {
            var value = CheckPrevalue(propertyType, "startNodeId");

            if (value != null && Udi.TryParse(value.ToString(), out var udi))
            {
                return udi;
            }

            return null;
        }

        public IPublishedContent CheckPrevalueStartNodeContent(PublishedPropertyType propertyType)
        {
            var udi = CheckPrevalueStartNodeUdi(propertyType);
            return udi == null ? null : _umbracoHelper.TypedContent(udi);
        }

        public IPublishedContent CheckPrevalueStartNodeMedia(PublishedPropertyType propertyType)
        {
            var udi = CheckPrevalueStartNodeUdi(propertyType);
            return udi == null ? null : _umbracoHelper.TypedMedia(udi);
        }

        public JObject CheckPrevalueJson(PublishedPropertyType propertyType, string key)
        {
            return JObject.Parse(CheckPrevalueString(propertyType, key));
        }

        public object ConvertObject<T>(object val, bool multi) where T : class, IPublishedContent
        {
            // Check if mutli picker
            if (multi)
            {
                return ((IEnumerable<IPublishedContent>)val).Where(t => t.GetType() == typeof(T)).Cast<T>();
            }

            // return as a single
            return val as T;
        }

        internal static void ClearCaches()
        {
            Storages.Clear();
        }
    }
}