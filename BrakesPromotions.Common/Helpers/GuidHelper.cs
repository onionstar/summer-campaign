﻿namespace BrakesPromotions.Common.Helpers
{
    // Namespaces
    using System;
    using System.Collections.Generic;
    using System.Linq;
    public class GuidHelper
    {
        public static string GetString(Guid guid)
        {
            return guid.ToString();
        }

        public static string[] GetStrings(IEnumerable<Guid> guids)
        {
            return guids.Select(x => GetString(x)).ToArray();
        }

        public static Guid GetGuid(string value)
        {
            var guid = default(Guid);
            if (Guid.TryParse(value, out guid))
            {
                return guid;
            }
            else
            {
                return Guid.Empty;
            }
        }
    }
}
