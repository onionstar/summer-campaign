﻿namespace BrakesPromotions.Common.Helpers
{
    // Namespaces
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using SendGrid;
    using SendGrid.Helpers.Mail;
    using System.Web.Configuration;
    using Umbraco.Core.Logging;

    public class CommunicationsHelper
    {
        public static bool EmailDelivered { get; set; }

        public static bool SendEmail(string htmlEmail, EmailAddress from, EmailAddress to, List<EmailAddress> bcc, string subject, List<Attachment> attachments = null)
        {
            try
            {
                EmailDelivered = false;

                PrepareSendGridMail(from, to, bcc, subject, htmlEmail, attachments).Wait();
                return EmailDelivered;
            }
            catch (Exception ex)
            {
                LogHelper.Error<CommunicationsHelper>("Error sending email", ex);
                return false;
            }
        }

        private static async Task PrepareSendGridMail(EmailAddress fromEmail, EmailAddress toEmail, List<EmailAddress> bccEmail, string subject, string htmlContent, List<Attachment> attachments = null)
        {
            var apiKey = WebConfigurationManager.AppSettings["Sendgrid.ApiKey"].ToString();
            var client = new SendGridClient(apiKey);
            var msg = new SendGridMessage
            {
                Subject = subject,
                From = fromEmail,
                HtmlContent = htmlContent
            };
            msg.AddCategory("Country Choice Products");
            if (attachments != null) msg.AddAttachments(attachments);

            // Note that the to address must never be the same as the bcc address
            msg.AddTo(toEmail);
            if (bccEmail != null && bccEmail.Any())
            {
                msg.AddBccs(bccEmail);
            }

            // Send the email.
            var response = await client.SendEmailAsync(msg).ConfigureAwait(false);
            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                var thing = response.Body.ReadAsStringAsync().Result;
                var FailedAt = DateTime.Now;
                EmailDelivered = false;
            }
            else
            {
                var AcceptedAt = DateTime.Now;
                EmailDelivered = true;
            }
        }


    }
}
