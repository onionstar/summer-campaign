﻿namespace BrakesPromotions.Common.Helpers
{
    // Namespaces
    using BrakesPromotions.Common.Models.Generated;
    using Examine;
    using Examine.Providers;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Umbraco.Core.Models;
    using Umbraco.Web;
    using UmbracoExamine;
    public class NodeFinder
    {
        private static readonly UmbracoHelper UmbracoHelper = new UmbracoHelper(UmbracoContext.Current);
        private static readonly BaseSearchProvider Search = ExamineManager.Instance.DefaultSearchProvider;

        public static Options Options(int rootNode) => GetCachedNode<Options>("options", rootNode);

        public static T GetCachedNode<T>(string docType, int rootNode)
            where T : class, IPublishedContent
        {
            return GetCachedNodes<T>(docType, rootNode)?.FirstOrDefault();
        }

        public static IList<T> GetCachedNodes<T>(string docType, int rootNode)
            where T : class, IPublishedContent
        {
            var cacheName = $"{docType}{rootNode}Node";

            var nodes = (int[])System.Web.HttpRuntime.Cache[cacheName];

            if (nodes == null || !nodes.Any())
            {
                nodes = GetNodes(docType, rootNode);

                if (!nodes.Any()) return null;

                CacheNodes(cacheName, nodes);
            }

            return UmbracoHelper.TypedContent(nodes)
                .Select(x => x as T)
                .Where(x => x != null)
                .ToList();
        }

        private static int[] GetNodes(string docType, int rootNode)
        {
            try
            {
                var searchCriteria = Search.CreateSearchCriteria(IndexTypes.Content);
                var query = searchCriteria.Field("nodeTypeAlias", docType).And().Field("searchablePath", rootNode.ToString());
                return Search.Search(query.Compile()).Select(x => x.Id).ToArray();
            }
            catch
            {
                var docs = UmbracoContext.Current.ContentCache.GetByXPath($"//{docType}");
                return docs.Select(x => x.Id).ToArray();
            }
        }

        private static void CacheNodes(string cacheName, object nodes)
        {
            System.Web.HttpRuntime.Cache.Add(
                cacheName,
                nodes,
                null,
                DateTime.UtcNow.AddDays(1),
                System.Web.Caching.Cache.NoSlidingExpiration,
                System.Web.Caching.CacheItemPriority.Normal,
                null
            );
        }
    }
}
