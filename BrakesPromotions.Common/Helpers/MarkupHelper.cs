using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;

namespace BrakesPromotions.Common.Helpers
{

    // Namespaces
    public class MarkupHelper
    {
        public static IHtmlString AttributeString(Dictionary<string, string> attributes)
        {
            return new HtmlString(string.Join(" ", attributes.Select(attr => $"{attr.Key}=\"{attr.Value}\"").ToArray()));
        }

        public static IHtmlString SetTag(IHtmlString content, string oldTag, string newTag, Dictionary<string, string> attributes = null)
        {
            return SetTag(content.ToString(), oldTag, newTag, attributes);
        }

        public static IHtmlString SetTag(string content, string oldTag, string newTag, Dictionary<string, string> attributes = null)
        {
            return UpdateMarkup(content, "//" + oldTag, node =>
            {
                node.Name = newTag;
                if (attributes != null)
                {
                    foreach (var attr in attributes)
                    {
                        node.SetAttributeValue(attr.Key, attr.Value);
                    }
                }
            });
        }

        public static IHtmlString UpdateMarkup(IHtmlString content, string target, Action<HtmlNode> action)
        {
            return UpdateMarkup(content.ToString(), target, action);
        }

        public static IHtmlString UpdateMarkup(string content, string target, Action<HtmlNode> action)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(content);
            
            var root = doc.DocumentNode.FirstChild.Attributes["class"]?.Value.Contains("s-cms") ?? false
                ? doc.DocumentNode.FirstChild
                : doc.DocumentNode;
            
            var tags = root.SelectNodes(target);
            
            if (tags != null)
            {
                foreach (var tag in tags)
                {
                    action(tag);
                }
            }

            return new HtmlString(doc.DocumentNode.InnerHtml);
        }

        public static IHtmlString Icon(string icon, string className = "")
        {
            var path = icon[0] == '/'
                ? icon
                : "/images/" + icon + ".svg";

            path = AppDomain.CurrentDomain.BaseDirectory + path;

            return System.IO.File.Exists(path)
                ? UpdateMarkup(new HtmlString(System.IO.File.ReadAllText(path)), "svg", node =>
                {
                    var classNames = $"{node.GetAttributeValue("class", "")} {className}".Trim();
                    node.SetAttributeValue("class", classNames);

                    if (node.Attributes["role"] == null)
                    {
                        node.SetAttributeValue("role", "presentation");
                    }
                })
                : new HtmlString("");
        }

        public static bool EmptyHtmlCheck(IHtmlString input)
        {
            if (input == null)
            {
                return true;
            }

            var document = new HtmlDocument();
            document.LoadHtml(input.ToString());
            var textValue = HtmlEntity.DeEntitize(document.DocumentNode.InnerText);
            var isEmpty = string.IsNullOrWhiteSpace(textValue);

            return isEmpty;
        }
    }
}
