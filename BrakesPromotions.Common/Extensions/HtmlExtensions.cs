﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace BrakesPromotions.Common.Extensions
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString StyledValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            var expressionTxt = ExpressionHelper.GetExpressionText(expression);
            var fullHtmlFieldName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(expressionTxt);
            if (!htmlHelper.ViewData.ModelState.ContainsKey(fullHtmlFieldName))
                return null;

            var modelState = htmlHelper.ViewData.ModelState[fullHtmlFieldName];
            var source = modelState?.Errors;
            var error = source == null || source.Count == 0 ? null : source.FirstOrDefault(m => !string.IsNullOrEmpty(m.ErrorMessage)) ?? source[0];
            if (error == null)
                return null;

            var tagBuilder = new TagBuilder("p");
            tagBuilder.AddCssClass("Form-note Form-note--error");
            tagBuilder.SetInnerText(error.ErrorMessage);
            return new MvcHtmlString(tagBuilder.ToString(TagRenderMode.Normal));
        }

        public static string FieldValidationClassname<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            var expressionTxt = ExpressionHelper.GetExpressionText(expression);
            var fullHtmlFieldName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(expressionTxt);
            return !htmlHelper.ViewData.ModelState.ContainsKey(fullHtmlFieldName) 
                || ! htmlHelper.ViewData.ModelState[fullHtmlFieldName].Errors.Any()
                ? null : "is-invalid";
        }
    }
}
