﻿namespace BrakesPromotions.Common.Enums
{
    public class GiftEnums
    {
        public enum GiftType
        {
            Gadget,
            NectarPoints,
            BrandVoucher,
            Love2ShopVoucher,
            SchoolVoucher
        }
    }
}
