﻿namespace BrakesPromotions.Common.BLL
{
    // Namespaces
    using Helpers;
    using Models.Custom;
    using Models.Generated;
    using Models;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Umbraco.Core;
    using Umbraco.Core.Models;
    using Umbraco.Web;
    using Umbraco.Web.Security;
    using Utilities;
    using System.Web;

    public class ModelLogic : BaseClass
    {
        #region MasterModel

        public static IMasterModel CreateMasterModel()
        {
            var content = UmbracoContext.Current.PublishedContentRequest.PublishedContent;
            var model = CreateMasterModel(content);
            return model;
        }

        public static IMasterModel CreateMasterModel(IPublishedContent content)
        {

            var rootNode = CurrentPage.AncestorOrSelf(1) as Base;

            var seoContent = content.GetPropertyValue("seoMetaData") as Epiphany.SeoMetadata.SeoMetadata;

            var model = CreateMagicModel(typeof(MasterModel<>), content) as IMasterModel;

            MembershipHelper membershipHelper = new MembershipHelper(UmbracoContext.Current);

            var campaign = JsonConvert.DeserializeObject<IEnumerable<NuPickersDropDownPicker>>(rootNode.CampaignName.SavedValue.ToString()).FirstOrDefault();
            model.TitleSuffix = $" | {campaign.Label}";

            if (seoContent != null)
            {
                model.SeoTitle = !string.IsNullOrWhiteSpace(seoContent.Title) ? seoContent.Title + model.TitleSuffix : content.Name + model.TitleSuffix;
                model.SeoDescription = seoContent.Description;
                model.SeoNoIndex = seoContent.NoIndex;
            }
            else
            {
                model.SeoTitle = content.Name + model.TitleSuffix;
            }

            model.SeoAuthor = "Brakes";
            model.PageTitle = content.Name;
            model.GtmCode = NodeFinder.Options(rootNode.Id)?.GtmCode;
            if (membershipHelper.IsLoggedIn())
            {
                var Member = membershipHelper.GetCurrentMemberProfileModel();
                var ActiveKey = new Guid();
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["activeAccount"] != null)
                {
                    ActiveKey = (Guid)HttpContext.Current.Session["activeAccount"];
                }

                var cmsMember = Services.MemberService.GetByEmail(Member.Email);
                var isAuthorised = false;

                if (cmsMember != null && cmsMember.IsApproved && (cmsMember.HasProperty("authorised") && cmsMember.GetValue<bool>("authorised")))
                {
                    isAuthorised = true;
                }

                model.Campaign = new CampaignLogic().GetCampaign(GuidHelper.GetGuid(campaign.Key), ApplicationContext.Current.Services.MemberService.GetByUsername(Member.UserName), ActiveKey, isAuthorised);
            }
            else
            {
                model.Campaign = new CampaignLogic().GetCampaign(GuidHelper.GetGuid(campaign.Key));
            }

            return model;
        }

        private static object CreateMagicModel(Type genericType, IPublishedContent content)
        {
            var contentType = content.GetType();
            var modelType = genericType.MakeGenericType(contentType);
            var model = Activator.CreateInstance(modelType, content);
            return model;
        }

        #endregion
    }
}
