﻿using System;

namespace BrakesPromotions.Common.BLL
{
    using BrakesPromotions.Common.Models.Generated;
    // Namespaces
    using Manager.Controllers;
    using Manager.Services;
    using Models.Custom;
    using System.Collections.Generic;
    using System.Linq;
    using Umbraco.Core.Models;
    using Umbraco.Web;

    public class CampaignLogic
    {
        private static CampaignService campaignService = new CampaignService();
        private static GiftTierService giftTierService = new GiftTierService();
        private static GiftTypeService giftTypeService = new GiftTypeService();
        private static GiftService giftService = new GiftService();
        private static MemberService memberService = new MemberService();
        private static BrandService brandService = new BrandService();
        private static TargetService targetService = new TargetService();
        private static PromotionsService promotionsService = new PromotionsService();

        protected static UmbracoHelper _UmbracoHelper => new UmbracoHelper(UmbracoContext.Current);

        public Campaign GetCampaign(Guid CampaignGuid)
        {
            var campaignModel = campaignService.GetByGuid(CampaignGuid);
            if (campaignModel == null) return null;

            var campaign = new Campaign
            {
                Name = campaignModel.Name,
                CampaignGuid = CampaignGuid,
                IsLive = campaignModel.IsLive && DateTime.UtcNow >= campaignModel.CampaignStartDate && DateTime.UtcNow < campaignModel.CampaignEndDate,
                Tiers = new List<GiftTier>(),
                Accounts = new List<Account>()
            };

            campaign.ClaimOpen = campaign.IsLive && DateTime.UtcNow >= campaignModel.ClaimStartDate && DateTime.UtcNow < campaignModel.ClaimEndDate;
            return campaign ?? null;

        }

        public Campaign GetCampaign(Guid CampaignGuid, IMember profileMember, Guid ActiveAccount, bool IsAuthorised)
        {
            var campaignModel = campaignService.GetByGuid(CampaignGuid);
            if (campaignModel == null) return null;

            var campaign = new Campaign
            {
                Name = campaignModel.Name,
                CampaignGuid = CampaignGuid,
                IsLive = campaignModel.IsLive && DateTime.UtcNow >= campaignModel.CampaignStartDate && DateTime.UtcNow < campaignModel.CampaignEndDate,
                Tiers = GetGiftTiers(CampaignGuid),
                Accounts = new List<Account>(),
                IsAuthorised = IsAuthorised
            };

            campaign.ClaimOpen = campaign.IsLive && DateTime.UtcNow >= campaignModel.ClaimStartDate && DateTime.UtcNow < campaignModel.ClaimEndDate;
            if (!campaign.Tiers.Any()) campaign.IsLive = false;

            if (!campaign.IsLive) return campaign ?? null;

            if (!campaign.IsAuthorised) return campaign;

            var memberModels = memberService.GetByUmbracoMember(profileMember.Key, CampaignGuid);
            foreach (var member in memberModels)
            {
                var account = new Account
                {
                    AccountGuid = member.MemberGuid,
                    BusinessName = member.BusinessName,
                    ClaimCode = member.ClaimCode,
                    URN = member.Urn,
                    Brand = brandService.GetByGuid(member.BrandGuid).Name,
                    TradingState = member.TradingState,
                    TelesalesNumber = member.TelesalesNumber,
                    SpendBalance = member.SpendBalance,
                    NectarCustomer = member.NectarCustomer,
                    Active = member.MemberGuid == ActiveAccount,
                    Updated = member.Updated,
                    Tokens = member.Tokens,
                    TradeDiscount = member.TradeDiscount,
                    Targets = new List<decimal>(),
                    ClaimedGiftTierGuid = member.ClaimedGiftTierGuid,
                    ClaimedGiftGuid = member.ClaimedGiftGuid,
                    TypeGuid = member.TypeGuid,
                    UmbracoMemberGuid = member.UmbracoMemberGuid.Value,
                    SpendingGoal = new SpendingGoals(),
                    Promotions = new List<Promotion>(),
                    NectarBonusDeliveryCount = member.NectarBonusDeliveryCount,
                    NectarBonusMinimum = member.NectarBonusMinimum,
                    NectarBonusPoints = member.NectarBonusPoints,
                    NectarBonusTarget = member.NectarBonusTarget,
                    NectarPointsBalance = member.NectarPointsBalance
                };

                var Promotions = GetPromotions(account.AccountGuid);
                if (Promotions.Any())
                {
                    account.Promotions = Promotions;
                }

                campaign.Accounts.Add(account);
            }
            if (!campaign.Accounts.Any(x => x.Active)) campaign.Accounts.FirstOrDefault().Active = true;

            var Targets = targetService.GetAll(campaign.ActiveAccount.AccountGuid);
            if (Targets.Any())
            {

                foreach (var target in Targets)
                {
                    campaign.ActiveAccount.Targets.Add(target.TargetValue);
                    var tier = campaign.Tiers.FirstOrDefault(x => x.TierGuid == target.GiftTierGuid);
                    tier.TargetReached = campaign.ActiveAccount.SpendBalance >= target.TargetValue;
                    tier.TargetGap = tier.TargetReached ? 0 : target.TargetValue - campaign.ActiveAccount.SpendBalance;
                    tier.IsClaimed = tier.TierGuid == campaign.ActiveAccount.ClaimedGiftTierGuid;

                    if (tier.IsClaimed)
                    {
                        tier.Gifts.FirstOrDefault(x => x.GiftGuid == campaign.ActiveAccount.ClaimedGiftGuid).IsClaimed = true;
                    }


                }
                campaign.ActiveAccount.SpendingGoal.FinalGoalAmount = (float)campaign.ActiveAccount.Targets.Last();

                var allTargetsMet = campaign.ActiveAccount.SpendBalance >= Convert.ToDecimal(campaign.ActiveAccount.SpendingGoal.FinalGoalAmount);
                campaign.ActiveAccount.SpendingGoal.AmountToNextGoal = !allTargetsMet ? (float)(campaign.Tiers.LastOrDefault(x => x.TargetGap > 0).TargetGap) : 0;
                campaign.ActiveAccount.SpendingGoal.CurrentSpendPercentage = !allTargetsMet ? ((float)campaign.ActiveAccount.SpendBalance / campaign.ActiveAccount.SpendingGoal.FinalGoalAmount) : 100;

                campaign.ActiveAccount.SpendingGoal.Goals = new List<SpendingGoal>();
                foreach (var target in campaign.ActiveAccount.Targets)
                {
                    campaign.ActiveAccount.SpendingGoal.Goals.Add(new SpendingGoal
                    {
                        Amount = (float)target,
                        Percentage = (float)(target / campaign.ActiveAccount.Targets.Last())
                    });
                }
            }

            return campaign ?? null;

        }

        public List<GiftTier> GetGiftTiers(Guid CampaignGuid)
        {
            var Tiers = new List<GiftTier>();

            var giftTierModel = giftTierService.GetByCampaignGuid(CampaignGuid);

            if (giftTierModel != null)
            {
                foreach (var giftTier in giftTierModel)
                {
                    Tiers.Add(new GiftTier
                    {
                        Name = giftTier.Name,
                        TierGuid = giftTier.GiftTierGuid,
                        Gifts = GetGifts(giftTier.GiftTierGuid),
                        TargetReached = false,
                        IsClaimed = false
                    });
                }
            }

            return Tiers;
        }

        public List<Gift> GetGifts(Guid GiftTierGuid)
        {
            var Gifts = new List<Gift>();

            var giftModels = giftService.GetByGiftTierGuid(GiftTierGuid);

            if (giftModels != null)
            {
                foreach (var gift in giftModels)
                {
                    var giftType = giftTypeService.GetByTypeGuid(gift.TypeGuid);
                    var newGift = new Gift
                    {
                        GiftGuid = gift.GiftGuid,
                        Name = giftService.FullGiftName(gift.GiftGuid, gift.TypeGuid),
                        IsClaimed = false
                    };
                    switch (giftType.Name)
                    {
                        case "Gadget 1":
                            newGift.GiftType = Enums.GiftEnums.GiftType.Gadget;
                            break;
                        case "Gadget 2":
                            newGift.GiftType = Enums.GiftEnums.GiftType.Gadget;
                            break;
                        case "Love2Shop Vouchers":
                            newGift.GiftType = Enums.GiftEnums.GiftType.Love2ShopVoucher;
                            break;
                        case "Nectar Points":
                            newGift.GiftType = Enums.GiftEnums.GiftType.NectarPoints;
                            break;
                        case "Brakes or Woodward Vouchers":
                            newGift.GiftType = Enums.GiftEnums.GiftType.BrandVoucher;
                            break;
                        case "TTS Vouchers":
                            newGift.GiftType = Enums.GiftEnums.GiftType.SchoolVoucher;
                            break;
                    }
                    if (giftType.HasMedia && gift.MediaId != Guid.Empty)
                    {
                        IMedia content = UmbracoContext.Current.Application.Services.MediaService.GetById(gift.MediaId);
                        IPublishedContent media = content == null ? null : _UmbracoHelper.TypedMedia(content.Id);

                        if (media != null) newGift.Image = media as Image;
                    }

                    Gifts.Add(newGift);
                }
            }

            return Gifts;
        }

        public List<Promotion> GetPromotions(Guid MemberGuid)
        {
            var eList = new ElistController();
            var Products = new List<Promotion>();

            var promotedProducts = promotionsService.GetAll(MemberGuid);
            foreach (var promo in promotedProducts)
            {
                var eListItem = eList.GetProductFromId(promo.BrakesProductId.ToString());
                if (eListItem != null)
                {
                    var product = new Promotion
                    {
                        ProductListProductId = eListItem.ProductListProductId,
                        Code = eListItem.Code,
                        Prefix = eListItem.Prefix,
                        ProductId = eListItem.ProductId,
                        Name = eListItem.Name,
                        PackSize = eListItem.PackSize,
                        MinimumPackSize = eListItem.MinimumPackSize,
                        ApproxSizeCountPtn = eListItem.ApproxSizeCountPtn,
                        PriceEach = eListItem.PriceEach,
                        ListPrice = eListItem.ListPrice,
                        ProductUrl = "https://www.brake.co.uk/products/" + eListItem.CategoryUrl + "/" + eListItem.CategoryHierarchy1Url + "/" + eListItem.CategoryHierarchy2Url + "/" + eListItem.CategoryHierarchy3Url + "/" + eListItem.ItemUrlSegment,
                        PromoPrice = promo.PromoPrice,
                        ShortDescription = eListItem.ShortDescription,
                        SecondaryTitle = eListItem.SecondaryTitle
                    };
                    Products.Add(product);
                }
            }

            return Products;
        }
    }
}
