﻿namespace BrakesPromotions.Common
{
    // Namespaces
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using Umbraco.Core.IO;
    using Umbraco.Core.Logging;

    public class ExtendedViewEngine : RazorViewEngine
    {
        public ExtendedViewEngine()
        {
            var viewsPath = IOHelper.MapPath("~/Views");

            var directories = Directory.GetDirectories(viewsPath);

            var pathList = new List<string>();
            //var partialsPathList = new List<string>();

            foreach (var directory in directories.Where(x => !x.ToLower().Contains("partials")))
            {
                var folder = Path.GetFileName(directory);

                var path = string.Format("~/Views/{0}/{{0}}.cshtml", folder);

                pathList.Add(path);

                LogHelper.Info<ExtendedViewEngine>("Registering view engine path: " + folder);

                //var subdirectories = Directory.GetDirectories(directory);

                //foreach (var subDirectory in subdirectories.Where(x => x.ToLower().Contains("partials")))
                //{
                //    var partialsFolder = Path.GetFileName(subDirectory);

                //    var partialsPath = string.Format("~/Views/{0}/{{0}}.cshtml", partialsFolder);

                //    partialsPathList.Add(partialsPath);

                //    LogHelper.Info<ExtendedViewEngine>("Registering partial view engine path: " + partialsFolder);
                //}
            }

            ViewLocationFormats = ViewLocationFormats.Union(pathList).ToArray();
            //PartialViewLocationFormats = PartialViewLocationFormats.Union(partialsPathList).ToArray();
        }
    }
}
