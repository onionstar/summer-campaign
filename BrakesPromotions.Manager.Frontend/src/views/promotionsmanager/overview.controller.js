
function overviewController($scope, $location, campaignResource, formHelper, navigationService, treeService) {

    var vm = this;

    vm.loaded = false;
    vm.page = {};
    vm.newCampaign = {};
    vm.page.name = "Promotions Manager - Campaigns";
    vm.activeCampaigns = [];
    vm.campaignViewState = 'overview';
    vm.defaultButton = {
        labelKey: "campaigns_createCampaign",
        handler: function () {
            vm.setCampaignViewState('createCampaign');
        }
    };
    vm.subButtons = [];

    $scope.options = {
        legend: {
            display: true,
            position: 'bottom'
        },
        tooltips: {
            enabled: true
        }
    }

    vm.setCampaignViewState = setCampaignViewState;
    vm.createCampaign = createCampaign;
    vm.clickCampaign = clickCampaign;

    function init() {

        var activeCampaigns = campaignResource.getActiveCampaigns();

        activeCampaigns.then(function (campaigns) {
            vm.activeCampaigns = campaigns;
            $scope.loaded = true;
        });
    }

    function clickCampaign(campaign) {
        goToCampaign(campaign.CampaignGuid);
    }

    function goToCampaign(campaignGuid) {
        $location.path('promotionsmanager/promotions/campaign/' + campaignGuid);
    }

    function setCampaignViewState(state) {
        vm.page.createButtonState = "init";

        if (state === "createCampaign") {
            clearAddCampaignForm();
        }
        vm.campaignViewState = state;
    }

    function clearAddCampaignForm() {
        // clear form data
        vm.newCampaign.id = 0;
        vm.newCampaign.name = "";
        vm.newCampaign.email = "";
        vm.newCampaign.startDate = new Date();
        vm.newCampaign.endDate = new Date();
    }

    function createCampaign(addCampaignForm) {

        if (formHelper.submitForm({ formCtrl: addCampaignForm, scope: $scope, statusMessage: "Saving..." })) {
            vm.newCampaign.id = -1;
            vm.page.createButtonState = "busy";

            var newCampaign = {
                Name: addCampaignForm.name.$modelValue,
                Email: addCampaignForm.email.$modelValue,
                StartDate: addCampaignForm.startDate.$modelValue,
                EndDate: addCampaignForm.endDate.$modelValue
            };

            campaignResource.createCampaign(newCampaign)
                .then(function (saved) {
                    vm.page.createButtonState = "success";
                    vm.newCampaign = saved;
                    setCampaignViewState('createCampaignSuccess');
                    init();
                    navigationService.syncTree({ tree: "PromotionsManager", path: ["-1", $scope.contentId], forceReload: true }).then(function (syncArgs) {
                        $scope.currentNode = syncArgs.node;
                        if ($routeParams.id == $scope.currentNode.id) {
                            $route.reload();
                        }

                    });
                }, function (err) {
                    formHelper.handleError(err);
                    vm.page.createButtonState = "error";
                });
        }
    }

    // initialise the controller
    init();
}

angular.module("promotionsmanager").controller("PromotionsManager.OverviewController", overviewController);