function promotionsProductsController($scope, $http, $routeParams, campaignResource, notificationsService, navigationService, treeService) {

    var vm = this;

    vm.save = save;

    vm.loaded = false;

    vm.campaign = {};
    vm.dialogData = {};

    init();

    function init() {
        vm.campaign = {
            IsLive: false,
            Name: "",
            Email: "",
            MaxGifts: 0,
            strStartDate: "",
            strEndDate: "",
            strCreatedDate: "",
            strUpdatedDate: "",
            strClaimStart: "",
            strClaimEnd: ""
        }

        vm.loaded = true;
    }

    function save() {
        vm.loaded = false;
        var campaignPromise = campaignResource.setCampaign(vm.campaign);
        campaignPromise.then(function (campaigns) {
            vm.loaded = true;
            navigationService.hideDialog();
            navigationService.syncTree({ tree: "PromotionsManager", path: ["-1", $scope.contentId], forceReload: true }).then(function (syncArgs) {
                $scope.busy = false;
                $scope.currentNode = syncArgs.node;
                if ($routeParams.id == $scope.currentNode.id) {
                    $route.reload();
                }
            });
        });
    }
}
angular.module("promotionsmanager").controller("PromotionsManager.Backoffice.New.CampaignController", promotionsProductsController);