(function () {
    "use strict";

    function campaignController($scope, $location, $http, $routeParams, campaignResource, entityResource, formHelper, navigationService, dialogService, userService, Upload, FileSaver) {

        //$scope.fileUpload = {};

        var vm = this;

        vm.page = {};
        vm.campaign = {};
        vm.gifts = {};
        vm.currentGift = {};
        vm.accounts = {};
        vm.accountsSelection = [];
        vm.accountOptions = {};
        vm.currentAccount = {};
        vm.breadcrumbs = [];
        vm.activeView = "";
        vm.activeSubView = "";
        vm.fileSettings = {
            Name: "Password protect",
            State: false,
            Password: ""
        };

        vm.goToPage = goToPage;
        vm.save = save;
        vm.disableCampaign = disableCampaign;
        vm.enableCampaign = enableCampaign;
        vm.toggleImportCampaign = toggleImportCampaign;
        vm.toggleUpdateCampaign = toggleUpdateCampaign;
        vm.toggleExportCampaign = toggleExportCampaign;
        vm.doImportCampaign = doImportCampaign;
        vm.doUpdateCampaign = doUpdateCampaign;
        vm.doExportCampaign = doExportCampaign;
        vm.clickGiftTier = clickGiftTier;
        vm.pickImage = pickImage;
        vm.removeImage = removeImage;
        vm.toggleFilter = toggleFilter;
        vm.setAccountStatesFilter = setAccountStatesFilter;
        vm.searchAccounts = searchAccounts;
        vm.setBrandFilter = setBrandFilter;
        vm.clickAccount = clickAccount;
        vm.clickMemberLink = clickMemberLink;

        function initCampaign() {

            vm.loading = true;

            // get campaign
            var activeCampaigns = campaignResource.getCampaign($routeParams.id).then(function (campaign) {

                vm.campaign = campaign;
                vm.page.name = campaign.Name;
                vm.page.editable = true;
                setActiveState();
                setAllocatedGifts();
                makeBreadcrumbs();
                setActiveView("campaign");
                vm.activeSubView = "";
                vm.loading = false;
            });

            makeBreadcrumbs(vm.campaign);
            getCurrentUser();
        }

        function initGifts() {

            vm.loading = true;

            // get campaign
            campaignResource.getGifts($routeParams.id).then(function (gifts) {

                vm.gifts = gifts;
                vm.page.name = vm.campaign.Name + " gifts";
                vm.page.editable = false;

                setActiveView("gifts");
                vm.activeSubView = "";

                vm.loading = false;
            });
        }

        function initAccounts() {

            vm.loading = true;

            // get accounts
            vm.page.name = vm.campaign.Name + " accounts";
            vm.page.editable = false;

            setActiveView("accounts");
            vm.activeSubView = "";

            vm.accountOptions.orderBy = "ClaimCode";
            vm.accountOptions.orderDirection = "Ascending";
            vm.accountOptions.filters = {
                statusFilter: "",
                brandFilter: {}
            };

            getAccounts();

            vm.loading = false;
        }

        function getAccounts() {
            campaignResource.getAccounts($routeParams.id, vm.accountOptions).then(function (accounts) {
                vm.accounts = accounts.Items;

                vm.accountOptions.pageNumber = accounts.PageNumber;
                vm.accountOptions.pageSize = accounts.PageSize;
                vm.accountOptions.totalItems = accounts.TotalItems;
                vm.accountOptions.totalPages = accounts.TotalPages;

                getGroups();
            });

        }

        function setActiveState() {
            vm.campaign.activeState = vm.campaign.IsLive ? "success" : "danger";
            vm.campaign.activeStateLabel = vm.campaign.IsLive ? "Active" : "Disabled";
        }

        function setAllocatedGifts() {
            for (var i = 0, len = vm.campaign.AvailableGifts.length; i < len; i++) {
                if (vm.campaign.AllocatedGiftTypes.map(function (e) { return e.TypeGuid }).indexOf(vm.campaign.AvailableGifts[i].TypeGuid) != -1) {
                    vm.campaign.AvailableGifts[i].allocated = true;
                }
            }
        }

        function makeBreadcrumbs() {
            vm.breadcrumbs = [
                {
                    "name": "Promotions",
                    "path": "/promotionsmanager/promotions/overview/0"
                },
                {
                    "name": vm.campaign.Name
                }
            ];
        }

        function goToPage(ancestor) {
            $location.path(ancestor.path);
        }

        function save() {
            if (vm.activeView === "campaign") {

                vm.page.saveButtonState = "busy";

                var allocatedList = [];
                for (var i = 0, len = vm.campaign.AvailableGifts.length; i < len; i++) {
                    if (vm.campaign.AvailableGifts[i].allocated) {
                        allocatedList.push({ "TypeGuid": vm.campaign.AvailableGifts[i].TypeGuid, "CampaignGuid": vm.campaign.CampaignGuid });
                    }
                }
                vm.campaign.AllocatedGiftTypes = allocatedList;

                campaignResource.updateCampaign(vm.campaign).then(function (saved) {
                    vm.page.saveButtonState = "success";
                }, function (err) {
                    vm.page.saveButtonState = "error";
                });

            } else if (vm.activeView === "gifts" && vm.activeSubView === "giftTierDetail") {

                vm.page.saveButtonState = "busy";

                campaignResource.updateGiftTier(vm.currentGift).then(function (saved) {
                    vm.page.saveButtonState = "success";
                }, function (err) {
                    vm.page.saveButtonState = "error";
                });

            } else if (vm.activeView === "accounts" && vm.activeSubView === "accountDetail") {
                vm.page.saveButtonState = "busy";

                campaignResource.updateAccount(vm.currentAccount).then(function (saved) {
                    vm.page.saveButtonState = "success";
                }, function (err) {
                    vm.page.saveButtonState = "error";
                });

            }
        }

        function disableCampaign() {
            vm.disableCampaignButtonState = "busy";
            campaignResource.disableCampaign(vm.campaign.CampaignGuid).then(function (saved) {
                vm.disableCampaignButtonState = "success";
                vm.campaign.IsLive = saved.IsLive;
                vm.campaign.UpdatedDate = saved.UpdatedDate;
            }, function (error) {
                vm.disableCampaignButtonState = "error";
                formHelper.showNotifications(error.data);
            });
        }

        function enableCampaign() {
            vm.enableCampaignButtonState = "busy";
            campaignResource.enableCampaign(vm.campaign.CampaignGuid).then(function (saved) {
                vm.enableCampaignButtonState = "success";
                vm.campaign.IsLive = saved.IsLive;
                vm.campaign.UpdatedDate = saved.UpdatedDate;
            }, function (error) {
                vm.enableCampaignButtonState = "error";
                formHelper.showNotifications(error.data);
            });
        }

        function toggleImportCampaign() {
            vm.isImporting = !vm.isImporting;
            vm.isUpdating = false;
            vm.prepareExport = false;
            delete vm.file;
        }

        function doImportCampaign() {
            vm.isImporting = !vm.isImporting;
            vm.importCampaignButtonState = "busy";
            if (vm.file) {
                campaignResource.importCampaign(vm.file, vm.campaign.CampaignGuid, "").then(function (imported) {
                    vm.importCampaignButtonState = "success";
                    delete vm.file;
                }, function (error) {
                    vm.importCampaignButtonState = "error";
                    formHelper.showNotifications(error.data);
                });
            } else {
                vm.importCampaignButtonState = "error";
            }
            delete vm.file;
        }

        function toggleUpdateCampaign() {
            vm.isUpdating = !vm.isUpdating;
            vm.isImporting = false;
            vm.prepareExport = false;
            delete vm.file;
            vm.importPromotions = {
                Name: "Include promotions",
                State: false
            };
            vm.importTargets = {
                Name: "Include targets",
                State: false
            };
            vm.fileSettings.Password = "";
            vm.fileSettings.State = false;
        }

        function doUpdateCampaign() {
            vm.isUpdating = !vm.isUpdating;
            vm.updateCampaignButtonState = "busy";
            if (vm.file) {
                var password = "";
                if (vm.fileSettings.State) {
                    password = vm.fileSettings.Password;
                }

                campaignResource.updateImport(vm.file, vm.campaign.CampaignGuid, password, vm.importPromotions.State, vm.importTargets.State).then(function (updated) {
                    vm.updateCampaignButtonState = "success";
                    delete vm.file;
                }, function (error) {
                    vm.updateCampaignButtonState = "error";
                    formHelper.showNotifications(error.data);
                });
            } else {
                vm.updateCampaignButtonState = "error";
            }
            delete vm.file;
        }

        function toggleExportCampaign() {
            vm.prepareExport = !vm.prepareExport;
            vm.isImporting = false;
            vm.isUpdating = false;
            vm.fileSettings.Password = "";
            vm.fileSettings.State = false;
        }

        function doExportCampaign() {
            vm.prepareExport = !vm.prepareExport;
            vm.exportCampaignButtonState = "busy";

            var password = "";
            if (vm.fileSettings.State) {
                password = vm.fileSettings.Password;
            }

            campaignResource.exportCampaign(vm.campaign.CampaignGuid, password).then(function (exported) {

                var file = new Blob([exported], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
                vm.exportCampaignButtonState = "success";
                FileSaver.saveAs(file, vm.campaign.Name.replace(/ /g, '') + '.xlsx');

            });

            vm.fileSettings.Password = "";
            vm.fileSettings.State = false;
        }

        function getCurrentUser() {
            userService.getCurrentUser().then(function (userObj) {
                var user = userObj;
                vm.currentUser = user.id;
                vm.canImport = user.userType === "admin";
            });
        }

        function setActiveView(viewState) {
            vm.activeView = viewState;
            vm.activeSubView = "";

            vm.page.navigation = [
                {
                    "name": "Campaign",
                    "icon": "icon-home",
                    "action": function () {
                        initCampaign()
                    },
                    "active": vm.activeView === "campaign"
                },
                {
                    "name": "Gifts",
                    "icon": "icon-gift",
                    "action": function () {
                        initGifts()
                    },
                    "active": vm.activeView === "gifts"
                },
                {
                    "name": "Accounts",
                    "icon": "icon-users",
                    "action": function () {
                        initAccounts()
                    },
                    "active": vm.activeView === "accounts"
                }
            ];
        }

        function clickGiftTier(giftTier) {
            vm.loading = true;

            vm.activeSubView = "giftTierDetail";
            vm.page.name = giftTier.Name;
            vm.page.editable = true;
            vm.currentGift = giftTier;
            initMedia();

            vm.loading = false;

        }

        function initMedia() {
            angular.forEach(vm.currentGift.Gifts, function (value, key) {

                if (value.HasMedia) {
                    value.media = {};

                    if (value.MediaId === "00000000-0000-0000-0000-000000000000") {
                        value.media.thumbnail = '';
                        value.media.width = 0;
                        value.media.height = 0;
                    } else {
                        entityResource.getById(value.MediaId, "Media").then(function (ent) {
                            value.media.thumbnail = ent.metaData.umbracoFile.Value.src;
                            value.media.width = ent.metaData.umbracoWidth.Value;
                            value.media.height = ent.metaData.umbracoHeight.Value;
                        });
                    }
                }
            });
        }

        function pickImage(gift) {
            dialogService.mediaPicker({
                multiPicker: false, // only allow one image to be picked
                // function that is called when the dialog is closed. 
                // Selected item(s) will be passed in by the data object

                callback: function (data) {
                    gift.MediaId = data.key;
                    gift.media.thumbnail = data.thumbnail;
                    gift.media.width = data.originalWidth;
                    gift.media.height = data.originalHeight;

                }
            });
        }

        function removeImage(gift) {
            gift.MediaId = "00000000-0000-0000-0000-000000000000";
            gift.media.thumbnail = '';
            gift.media.width = 0;
            gift.media.height = 0;
        }

        function toggleFilter(type) {

            switch (type) {
                case "status":
                    vm.page.showStatusFilter = !vm.page.showStatusFilter;
                    vm.page.showBrandFilter = false;
                    break;
                case "brand":
                    vm.page.showBrandFilter = !vm.page.showBrandFilter;
                    vm.page.showStatusFilter = false;
                    break;
            }
        }

        function setAccountStatesFilter(accountState) {

            if (!vm.accountOptions.accountStates) {
                vm.accountOptions.accountStates = [];
            }

            // If the selection is "ALL" then we need to unselect everything else since this is an 'odd' filter
            if (accountState.key === "All") {
                angular.forEach(vm.accountStatesFilter, function (i) {
                    i.selected = false;
                });

                // we can't unselect All
                accountState.selected = true;

                // reset the selection passed to the server
                vm.accountOptions.accountStates = [];

            } else {

                angular.forEach(vm.accountStatesFilter, function (i) {
                    if (i.key === "All") {
                        i.selected = false;
                    }
                });
                var indexOfAll = vm.accountOptions.userStates.indexOf("All");
                if (indexOfAll >= 0) {
                    vm.accountOptions.accountStates.splice(indexOfAll, 1);
                }

            }

            if (accountState.selected) {
                vm.accountOptions.accountStates.push(accountState.key);
            } else {
                var index = vm.accountOptions.accountStates.indexOf(accountState.key);
                vm.accountOptions.accountStates.splice(index, 1);
            }

            // Get Accounts
            getAccounts();

        }

        function setBrandFilter(sortData) {

            if (vm.accountOptions.filters.brandFilter === sortData) {
                vm.accountOptions.filters.brandFilter = "";
            } else {
                vm.accountOptions.filters.brandFilter = sortData;
            }
            vm.page.showBrandFilter = !vm.page.showBrandFilter;

            // Get Accounts
            getAccounts();

        }

        function searchAccounts() {
            search();
        }

        function getGroups() {

            // get brands
            campaignResource.getBrands().then(function (brands) {
                vm.brands = brands;
            });

        }

        function clickAccount(account) {
            vm.loading = true;

            vm.activeSubView = "accountDetail";
            vm.page.name = account.ClaimCode;
            vm.page.editable = false;
            vm.currentAccount = account;

            vm.currentAccount.gdprSalesContactState = vm.currentAccount.GdprSalesContact ? "success" : "danger";
            vm.currentAccount.gdprSalesContactStateLabel = vm.currentAccount.GdprSalesContact ? "You may contact me" : "You may not contact me";

            vm.currentAccount.gdprOffersContactState = vm.currentAccount.GdprOffers ? "success" : "danger";
            vm.currentAccount.gdprOffersContactStateLabel = vm.currentAccount.GdprOffers ? "You may contact me" : "You may not contact me";

            vm.currentAccount.nectarAccountState = vm.currentAccount.NectarCustomer ? "success" : "danger";
            vm.currentAccount.nectarAccountStateLabel = vm.currentAccount.NectarCustomer ? "Nectar customer" : "Not a Nectar customer";

            vm.currentAccount.registeredState = vm.currentAccount.HasRegistered ? "success" : "danger";
            vm.currentAccount.registeredStateLabel = vm.currentAccount.HasRegistered ? "Registered" : "Unregistered";


            vm.loading = false;
        }

        function clickMemberLink(umbracoMemberGuid) {
            $location.path("member/member/edit/" + umbracoMemberGuid);
        }

        var search = _.debounce(function () {
            $scope.$apply(function () {
                getAccounts();
            });
        }, 500);

        initCampaign();
    }

    angular.module("promotionsmanager").controller("PromotionsManager.CampaignController", campaignController);
})();