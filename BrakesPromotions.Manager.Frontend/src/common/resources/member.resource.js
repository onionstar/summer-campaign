//angular.module('promotionsmanager.resources')
//    .factory('campaignMemberResource', ['$http', 'umbRequestHelper',
//        function ($http, umbRequestHelper) {
//            return {
//                getCampaign: function (campaignGuid) {
//                    var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignControllerBaseUrl'] + 'GetCampaign';
//                    return umbRequestHelper.resourcePromise(
//                        $http({
//                            url: url,
//                            method: "GET",
//                            params: { id: campaignGuid }
//                        }),
//                        'Failed to get campaign: ' + campaignGuid);
//                },
//                GetAllMembers: function (id, options) {
//                    if (options === undefined) {
//                        return "Failed to load members";
//                    }
//                    var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['membersApiController'] + 'GetPagedMembersForCampaign';
//                    var params = {
//                        Key: id,
//                        Page: options.pageNumber,
//                        ItemsPerPage: options.pageSize,
//                        SearchTerm: options.filter
//                    };
//                    return umbRequestHelper.resourcePromise(
//                        $http({
//                            url: url,
//                            method: "GET",
//                            params: params
//                        }),
//                        'Failed to load members');
//                },
//                getMember: function (memberGuid) {
//                    var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['membersApiController'] + 'GetMember';
//                    return umbRequestHelper.resourcePromise(
//                        $http({
//                            url: url,
//                            method: "GET",
//                            params: { id: memberGuid }
//                        }),
//                        'Failed to get details for member: ' + memberGuid);
//                },
//                updateMember: function (member) {
//                    var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['membersApiController'] + 'UpdateMember';
//                    return umbRequestHelper.resourcePromise(
//                        $http({
//                            url: url,
//                            method: "POST",
//                            data: member
//                        }),
//                        'Failed to update member');
//                }
//            };
//        }]);