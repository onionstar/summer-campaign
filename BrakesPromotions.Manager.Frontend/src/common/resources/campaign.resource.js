function campaignResource($q, $http, $cacheFactory, umbRequestHelper) {

    var _settingsCache = $cacheFactory('promotionsManagerSettings');

    /* helper method to get from cache or fall back to an http api call */
    function getCachedOrApi(cacheKey, apiMethod, entityName, param) {

        var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + apiMethod;
        var deferred = $q.defer();

        var dataFromCache = _settingsCache.get(cacheKey);

        if (dataFromCache) {
            deferred.resolve(dataFromCache);
        }
        else {
            var data = {
                id: param
            };
            var config = {
                params: data,
                headers: { 'Accept': 'application/json' }
            };

            var promiseFromApi = umbRequestHelper.resourcePromise(
                $http.get(url, config),
                'Failed to get all ' + entityName);

            promiseFromApi.then(function (dataFromApi) {
                _settingsCache.put(cacheKey, dataFromApi);
                deferred.resolve(dataFromApi);
            }, function (reason) {
                deferred.reject(reason);
            });
        }

        return deferred.promise;
    }

    return {
        getAllCampaigns: function () {
            return getCachedOrApi("PromotionsManagerCampaigns", "GetAllCampaigns", "all campaigns");
        },
        getActiveCampaigns: function () {
            return getCachedOrApi("PromotionsManagerCampaigns", "GetActiveCampaigns", "active campaigns");
        },
        getCampaign: function (campaignGuid) {
            var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + 'GetCampaign';
            return umbRequestHelper.resourcePromise(
                $http({
                    url: url,
                    method: "GET",
                    params: { CampaignGuid: campaignGuid },
                    headers: { 'Content-Type': 'application/json' },
                }),
                'Failed to load campaign');
        },
        getInactiveCampaigns: function () {
            return getCachedOrApi("PromotionsManagerCampaigns", "GetInActiveCampaigns", "inactive campaigns");
        },
        createCampaign: function (newCampaign) {
            var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + 'SetNewCampaign';
            var data = JSON.stringify(newCampaign);
            _settingsCache.removeAll();
            return umbRequestHelper.resourcePromise(
                $http({
                    url: url,
                    method: "POST",
                    data: data,
                    headers: { 'Content-Type': 'application/json' },
                }),
                'Failed to create campaign');
        },
        updateCampaign: function (campaign) {
            var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + 'UpdateCampaign';
            var data = JSON.stringify(campaign);
            _settingsCache.removeAll();
            return umbRequestHelper.resourcePromise(
                $http({
                    url: url,
                    method: "POST",
                    data: data,
                    headers: { 'Content-Type': 'application/json' },
                }),
                'Failed to update campaign');
        },
        disableCampaign: function (campaignGuid) {
            var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + 'DisableCampaign';
            _settingsCache.removeAll();
            return umbRequestHelper.resourcePromise(
                $http({
                    url: url,
                    method: "GET",
                    params: { CampaignGuid: campaignGuid },
                    headers: { 'Content-Type': 'application/json' },
                }),
                'Failed to disable campaign');
        },
        enableCampaign: function (campaignGuid) {
            var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + 'EnableCampaign';
            _settingsCache.removeAll();
            return umbRequestHelper.resourcePromise(
                $http({
                    url: url,
                    method: "GET",
                    params: { CampaignGuid: campaignGuid },
                    headers: { 'Content-Type': 'application/json' },
                }),
                'Failed to enable campaign');
        },
        importCampaign: function (file, campaignGuid, password) {
            var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + 'ImportCampaignAccounts';
            _settingsCache.removeAll();
            var fd = new FormData();
            fd.append('file', file);
            fd.append('campaignGuid', campaignGuid);
            fd.append('password', password);

            return umbRequestHelper.resourcePromise(
                $http({
                    url: url,
                    method: "POST",
                    headers: { 'Content-Type': undefined }, //this is important
                    transformRequest: angular.identity, //also important
                    data: fd
                }),
                'Failed to import campaign members');
        },
        updateImport: function (file, campaignGuid, password, includePromo, includeTargets) {
            var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + 'UpdateCampaignAccounts';
            _settingsCache.removeAll();
            var fd = new FormData();
            fd.append('file', file);
            fd.append('campaignGuid', campaignGuid);
            fd.append('password', password);
            fd.append('includePromo', includePromo);
            fd.append('includeTargets', includeTargets);

            return umbRequestHelper.resourcePromise(
                $http({
                    url: url,
                    method: "POST",
                    headers: { 'Content-Type': undefined, 'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }, //this is important
                    transformRequest: angular.identity, //also important
                    data: fd
                }),
                'Failed to update campaign members');
        },
        exportCampaign: function (campaignGuid, password) {
            var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + 'ExportCampaignAccounts';
            _settingsCache.removeAll();
            var fd = new FormData();
            fd.append('campaignGuid', campaignGuid);
            fd.append('password', password);

            return umbRequestHelper.resourcePromise(
                $http({
                    url: url,
                    method: "POST",
                    headers: { 'Content-Type': undefined }, //this is important
                    transformRequest: angular.identity, //also important
                    data: fd,
                    responseType: "arraybuffer"
                }),
                'Failed to export campaign members');
        },

        getGifts: function (campaignGuid) {
            var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + 'GetGiftTiers';
            return umbRequestHelper.resourcePromise(
                $http({
                    url: url,
                    method: "GET",
                    params: { CampaignGuid: campaignGuid },
                    headers: { 'Content-Type': 'application/json' },
                }),
                'Failed to load gifts');
        },
        updateGiftTier: function (giftTier) {
            var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + 'UpdateGiftTier';
            var data = JSON.stringify(giftTier);
            _settingsCache.removeAll();
            return umbRequestHelper.resourcePromise(
                $http({
                    url: url,
                    method: "POST",
                    data: data,
                    headers: { 'Content-Type': 'application/json' },
                }),
                'Failed to update gifts');
        },

        getAccounts: function (campaignGuid, options) {
            var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + 'GetAccounts';

            var defaults = {
                pageSize: 25,
                pageNumber: 1,
                filter: '',
                orderDirection: "Ascending",
                orderBy: "ClaimCode",
                filters: {
                    statusFilter: "",
                    brandFilter: ""
                }
            };

            if (options === undefined) {
                options = {};
            }

            //overwrite the defaults if there are any specified
            angular.extend(defaults, options);
            //now copy back to the options we will use
            options = defaults;

            var data = {
                campaignGuid: campaignGuid,
                pageNumber: options.pageNumber,
                pageSize: options.pageSize,
                orderBy: options.orderBy,
                orderDirection: options.orderDirection,
                filter: options.filter,
                filters: options.filters
            };

            return umbRequestHelper.resourcePromise(
                $http({
                    url: url,
                    method: "POST",
                    data: data,
                    headers: { 'Content-Type': 'application/json' },
                }),
                'Failed to load accounts');
        },
        getBrands: function () {
            return getCachedOrApi("PromotionsManagerCampaigns", "GetBrands", "all brands");
        },
        updateAccount: function (account) {
            var url = Umbraco.Sys.ServerVariables['promotionsManagerUrls']['campaignApiControllerBaseUrl'] + 'UpdateAccount';
            var data = JSON.stringify(account);
            _settingsCache.removeAll();
            return umbRequestHelper.resourcePromise(
                $http({
                    url: url,
                    method: "POST",
                    data: data,
                    headers: { 'Content-Type': 'application/json' },
                }),
                'Failed to update account');
        }
    };

}

angular.module("promotionsmanager.resources").factory("campaignResource", campaignResource);