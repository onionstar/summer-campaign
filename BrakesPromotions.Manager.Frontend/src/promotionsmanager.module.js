//bootstrap the PromotionsManager angular module
(function () {
    angular.module("promotionsmanager", [
        "umbraco.filters",
        "umbraco.directives",
        "umbraco.services",
        "checklist-model",
        "promotionsmanager.resources",
        "promotionsmanager.directives",
        "promotionsmanager.plugins",
        "promotionsmanager.services",
        'ngFileSaver'
    ]);
    angular.module("promotionsmanager.resources", []);
    angular.module("promotionsmanager.directives", []);
    angular.module("promotionsmanager.services", []);
    angular.module("promotionsmanager.plugins", ["chart.js"]);
    angular.module('fileUpload', ['ngFileUpload']);
    // Inject our dependencies
    angular.module("umbraco.packages").requires.push("promotionsmanager");
}());